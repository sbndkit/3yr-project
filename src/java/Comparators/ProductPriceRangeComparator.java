/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comparators;
import Business.Product;
import java.util.Comparator;

/**
 * The ProductPriceRangeComparator is used for comparing items by price
 * @author Sergio
 */
public class ProductPriceRangeComparator implements Comparator<Product>{
    /**
     * The compare method of the ProductPriceRangeComparator class compares the
     * prices of two product objects and then returns whether the first
     * item is greater than less than or equal to, the second object
     *
     * @param p1 The first product object that we wish to see if it is greater
     * than, less than, or equal to the second object
     * @param p2 The object that will compared to the first
     * @return This method returns 1,0 or -1 dependant on if p1 is greater than,
     * equal to or less than p2
     */ 
    @Override
    public int compare(Product p1, Product p2
    ) {

        if (p1.getPrice() < p2.getPrice()) {
            return 1;
        } else if (p1.getPrice() > p2.getPrice()) {
            return -1;
        } else {
            return 0;
        }

    }
}
