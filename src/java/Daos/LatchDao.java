/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Interfaces.LatchDaoInterface;
import com.elevenpaths.latch.LatchUser;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 * This class provides all functionalities in order to interact with the Latch
 * table in the database
 *
 * @inheritDoc This class extends the Dao class which controls the connections with the database
 * @author Sergio
 */
public class LatchDao extends Dao implements LatchDaoInterface {

    // Add in constructor that takes in the source of database connections and 
    // pass it up to the super class for storage
    public LatchDao(DataSource myDataSource) {
        super(myDataSource);
    }

    public LatchDao() {
        // Add a blank constructor to allow for default usage
        super();
    }

    /**
     * This method is used to get information for connection to the latch server
     *
     * @return LatchUser is the object which contains the details for connection
     * to latch server
     */
    public LatchUser getLatchInfo() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        LatchUser info;

        try {
            con = getConnection();

            String query = "CALL get_latch_info()";
            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            if (rs.next()) {
                String APP_ID = rs.getString("appID");
                String SECRET = rs.getString("secret");

                info = new LatchUser(APP_ID, SECRET);

                return info;
            }

        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return null;
    }

    /**
     * This method set the information in the Latch table to have a connection
     * with the latch server
     *
     * @param APP_ID is the id that identifies our application on the latch
     * server
     * @param SECRET is a token that validates the connection
     * @return <code>true</code> if the information was set successfully
     * <code>false</code>otherwise
     */
    public boolean setLatchInfo(String APP_ID, String SECRET) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = getConnection();

            String query = "CALL set_latch_info(?,?)";
            ps = con.prepareStatement(query);

            ps.setString(1, APP_ID);
            ps.setString(2, SECRET);

            ps.executeUpdate();

            return true;

        } catch (SQLException e) {
            return false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
    }
}
