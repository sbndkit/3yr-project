/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Interfaces.OrderProductInterface;
import Business.OrderProduct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *This class provides all functionalities in order to interact with the OrderProduct table in the database
 * 
 * @inheritDoc This class extends the Dao class which controls the connections with the database,
 * it also implements the OrderProductDaoInterface to ensure that all necessary methods are implemented
 * the way specified in the interface.
 * 
 * @author Sergio
 */
public class OrderProductDao extends Dao implements OrderProductInterface {

    public OrderProductDao(DataSource myDataSource) {
        super(myDataSource);
    }

    public OrderProductDao() {
        // Add a blank constructor to allow for default usage
        super();
    }

    /**
     * This method takes no parameters but returns all the elements in the orderProduct table in the database.
     * 
     * @return ArrayList of type OrderProduct, A list of all entries in the orderProduct table.
     */
    @Override
    public ArrayList<OrderProduct> getAllOrdersProduct() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<OrderProduct> orders = new ArrayList();

        try {
            con = getConnection();

            String query = "CALL get_all_orders_product()";
            ps = con.prepareStatement(query);
            
            rs = ps.executeQuery(); 
            
            while(rs.next())
            {
                // Make a OrderProduct object for the current OrderProduct
                OrderProduct o = new OrderProduct();
                
                //orderID,  ProductID, Qty
                o.setOrderId(rs.getInt("orderID"));
                o.setProductId(rs.getInt("productID"));
                o.setQty(rs.getInt("quantity"));
                orders.add(o);
            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return orders;
    }

    /**
     * This method takes a single parameter, a user id, and uses it to retrieve all
     * orderProduct entries related to that user.
     * 
     * @param userID is the id of the user we wish to get orderProducts for
     * @return ArrayList of type OrderProduct, A list of orderProducts relevant to the userID
     */
    @Override
    public ArrayList<OrderProduct> getAllOrdersProductByUserID(int userID) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<OrderProduct> orders = new ArrayList();

        try {
            con = getConnection();

            String query = "CALL get_all_orders_product_by_user_id(?)";
            ps = con.prepareStatement(query);

            ps.setInt(1, userID);
            rs = ps.executeQuery();
            while(rs.next())
            {
                
                // Store the current orderProduct object (now filled with information) in the arraylist
                OrderProduct o = new OrderProduct();
                o.setOrderId(rs.getInt("orderID"));
                o.setProductId(rs.getInt("productID"));
                o.setQty(rs.getInt("quantity"));
                orders.add(o);
            } 
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return orders;
    }

    /**
     * This method takes in 2 parameters, an order id and a product id and uses them to find a 
     * specific orderProduct entry in the orderProduct table in the database.
     * 
     * @param orderID is the id of the order we wish to find
     * @param productID is the id of the product we wish to find on the order
     * @return OrderProduct an OrderProduct object containing all the information for that order product
     */
    @Override
    public OrderProduct getOrderProductById(int orderID, int productID) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = getConnection();

            String query = "CALL get_order_product_by_id(?,?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, orderID);
            ps.setInt(2, productID);

            rs = ps.executeQuery();

            while (rs.next()) {
                OrderProduct o = new OrderProduct();

                o.setOrderId(rs.getInt("orderID"));
                o.setProductId(rs.getInt("productID"));
                o.setQty(rs.getInt("quantity"));

                return o;
            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return null;
    }

    /**
     * This method takes a single parameter, an OrderProduct object and inserts 
     * it into the orderProduct table.
     * 
     * @param nOrder is the orderProduct object that is to be inserted into 
     * the orderProduct table in the database
     * @return <code>true</code> if the OrderProduct was inserted successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean insertOrderProduct(OrderProduct nOrder) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (nOrder == null) {
            return false;
        }

        try {
            con = getConnection();

            String query = "CALL insert_order_product(?,?,?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, nOrder.getOrderId());
            ps.setInt(2, nOrder.getProductId());
            ps.setInt(3, nOrder.getQty());

            int i = ps.executeUpdate();
            
            if (i != 0)
            {
                return true;
            }

        } catch (SQLException e) {
            return false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
        return false;
    }

    /**
     * This method takes a single parameter, an OrderProduct id, 
     * that is used to find an entry in the OrderProduct table and delete it.
     * 
     * @param orderProductID is the id of the OrderProduct that is to be deleted
     * @return <code>true</code> if the OrderProduct was successfully deleted
     *         <code>false</code> otherwise
     */
    @Override
    public boolean deleteOrderProductById(int orderProductID) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        if(orderProductID <= 0)
        {
            return false;
        }
            

        try {
            con = getConnection();

            String query = "CALL delete_order_product_by_id(?)";

            ps = con.prepareStatement(query);
            ps.setInt(1, orderProductID);
            
            int i = ps.executeUpdate(); 
            
            if (i != 0)
            {
                return true;
            }
        } 
        catch (SQLException e) 
        {
            return false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
        return false;
    }

    /**
     * This method takes 2 OrderProduct parameters,one containing the new OrderProduct information
     * and another containing the new OrderProduct information, and updates the old information 
     * with the new and updates the entry in the database with this new information
     * 
     * @param newData is an OrderProduct object containing the new information 
     * to be updated to the old OrderProduct entry 
     * @param oldData is an OrderProduct object that contains the old original data for the OrderProduct entry
     * @return <code>true</code> if the OrderProduct entry was updated successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean updateOrderProduct(OrderProduct newData, OrderProduct oldData) {
        Connection con = null;
        PreparedStatement ps = null;

        boolean inserted = false;

        try {
            con = getConnection();

            String query = "CALL update_order_product(?,?,?,?,?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, newData.getOrderId());
            ps.setInt(2, newData.getProductId());
            ps.setInt(3, newData.getQty());
            ps.setInt(4, oldData.getOrderId());
            ps.setInt(5, oldData.getProductId());

            int i = ps.executeUpdate();

            if (i != 0) {
                inserted = true;
            }

        } catch (SQLException e) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
        return inserted;
    }

}
                
                //orderProductID,  ProductID, Qty
