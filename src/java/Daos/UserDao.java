/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Business.PasswordRecovery;
import Interfaces.UserDaoInterface;
import Business.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.sql.DataSource;

/**
 *This class provides all functionalities in order to interact with the User table in the database
 * 
 * @inheritDoc This class extends the Dao class which controls the connections with the database,
 * it also implements the UserDaoInterface to ensure that all necessary methods are implemented
 * the way specified in the interface.
 * 
 * @author Sergio
 */
public class UserDao extends Dao implements UserDaoInterface {

    // Add in constructor that takes in the source of database connections and 
    // pass it up to the super class for storage
    public UserDao(DataSource myDataSource) {
        super(myDataSource);
    }

    public UserDao() {
        // Add a blank constructor to allow for default usage
        super();
    }

    /**
     * This method takes no parameters but returns an ArrayList of User objects with all users information.
     * 
     * @return ArrayList of type User, A ArrayList of user objects containing 
     * information on all users in the user table.
     */
    @Override
    public ArrayList<User> FindAllUsers() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<User> users = new ArrayList();

        try {
            con = getConnection();

            String query = "CALL find_all_users()";

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                User u = new User();
                u.setUserId(rs.getInt("userid"));
                u.setfName(rs.getString("fName"));
                u.setlName(rs.getString("lName"));
                u.setEmail(rs.getString("email"));
                u.setPhone(rs.getString("phone"));
                u.setPasswordHashed(rs.getString("pass"));
                u.setIsAdmin(rs.getBoolean("isAdmin"));
                u.setWalletCredits(rs.getInt("walletCredits"));
                u.setAccountId(rs.getString("accountId"));

                users.add(u);

            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return users;     // users may be null 
    }

    /**
     * This method takes a single parameter, an id of the user that is to be found
     * 
     * @param userId is the id of the user that is to be found
     * @return User, A user object containing all that users information
     */
    @Override
    public User getUserById(int userId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User u = null;
        ArrayList<User> users = new ArrayList();

        try {
            con = this.getConnection();
            String query = "CALL get_user_by_id(?)";

            ps = con.prepareStatement(query);
            ps.setInt(1, userId);

            rs = ps.executeQuery();
            if (rs.next()) {
                u = new User();

                u.setUserId(rs.getInt("userID"));
                u.setfName(rs.getString("fName"));
                u.setlName(rs.getString("lName"));
                u.setEmail(rs.getString("email"));
                u.setPhone(rs.getString("phone"));
                u.setPasswordHashed(rs.getString("pass"));
                u.setIsAdmin(rs.getBoolean("isAdmin"));
                u.setWalletCredits(rs.getInt("walletCredits"));
                u.setAccountId(rs.getString("accountId"));

                return u;
            }
        } catch (SQLException e) {
            u = null;
        } catch (NullPointerException ex) {
            u = null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                System.err.println("A problem occurred when closing down the findUserById method:\n" + e.getMessage());
            }
        }
        return u;     // users may be null 
    }

    // this method returns a list of users depending on the first name or the last name
    /**
     * This method takes in 2 parameters, a users first and last name, and returns 
     * a list of users that have the same name.
     * 
     * @param fName is the first name of the user that is to be found
     * @param lName is the last name of the user that is to be found
     * @return ArrayList of type User. is an ArrayList of users that 
     * have the same first and last name as those entered
     */
    @Override
    public ArrayList<User> getUserByName(String fName, String lName) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User u = null;

        ArrayList<User> users = new ArrayList();
        if (fName.length() < 1 || lName.length() < 1) {
            return null;
        }
        try {
            con = this.getConnection();

            String query = "CALL get_user_by_name(?,?)";

            ps = con.prepareStatement(query);

            ps.setString(1, "%" + fName + "%");
            ps.setString(2, "%" + lName + "%");

            rs = ps.executeQuery();

            while (rs.next()) {
                u = new User();

                u.setUserId(rs.getInt("userID"));
                u.setfName(rs.getString("fName"));
                u.setlName(rs.getString("lName"));
                u.setEmail(rs.getString("email"));
                u.setPhone(rs.getString("phone"));
                u.setPasswordHashed(rs.getString("pass"));
                u.setIsAdmin(rs.getBoolean("isAdmin"));
                u.setWalletCredits(rs.getInt("walletCredits"));
                u.setAccountId(rs.getString("accountId"));

                users.add(u);
            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return users;
    }

    /**
     * This method takes 2 parameters, a users email and password, and uses them to 
     * find a user in the user table, if a user matches the user name and password
     * a user object is then created and that users information is inserted into 
     * that user object.
     * 
     * @param email is the users email which is used to access their account
     * @param userPassword is the users password which is used to access their account
     * @return User, a User object which contains all the information for that user
     */
    @Override
    public User login(String email, char[] userPassword) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User u = null;

        if (email == null) {
            return null;
        }

        try {
            con = this.getConnection();

            if (userPassword == null) {
                return null;
            }

            String pass = new User().getHash(userPassword);

            String query = "CALL login(?,?)";

            ps = con.prepareStatement(query);

            ps.setString(1, email);
            ps.setString(2, pass);

            rs = ps.executeQuery();

            if (rs.next()) {
                u = new User();

                u.setUserId(rs.getInt("userID"));
                u.setfName(rs.getString("fName"));
                u.setlName(rs.getString("lName"));
                u.setEmail(rs.getString("email"));
                u.setPhone(rs.getString("phone"));
                u.setPasswordHashed(rs.getString("pass"));
                u.setIsAdmin(rs.getBoolean("isAdmin"));
                u.setWalletCredits(rs.getInt("walletCredits"));
                String accountId = rs.getString("accountId");
                u.setAccountId(rs.getString("accountId"));

                return u;
            } else {

                new AccountDao().update(email);
            }
        } catch (SQLException e) {
            //e.printStackTrace();
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                //e.printStackTrace();
                return null;
            }
        }
        return u;     // u may be null 
    }

    /**
     * This method takes a single parameter, a User object containing the information of a 
     * user trying to register, and uses that information to make a new entry in the user table
     * 
     * @param nUser is a User object that contains all the information for a new user
     * @return <code>true</code> if the user was added successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean registerNewUser(User nUser) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (nUser == null) {
            return false;
        }

        try {
            con = this.getConnection();

            String query = "CALL register_new_user (?, ?, ?, ?, ?, ?, ?,0)";
            ps = con.prepareStatement(query);

            if (nUser.getUserId() < 0) {
                ps.setString(1, "NULL");
            } else {
                ps.setInt(1, nUser.getUserId());
            }

            ps.setString(2, nUser.getfName());
            ps.setString(3, nUser.getlName());
            ps.setString(4, nUser.getEmail());
            ps.setString(5, nUser.getPhone());
            ps.setString(6, nUser.getPassword());

            if (nUser.getIsIsAdmin()) {
                ps.setInt(7, 1);
            } else {
                ps.setInt(7, 0);
            }

            ps.execute();

            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
    }

    /**
     * This method takes a single parameter, a users id, which is used to 
     * delete a user from the user table.
     * 
     * @param userID is the id of the user that is to be deleted
     * @return <code>true</code> if the user was successfully deleted
     *         <code>false</code> otherwise
     */
    @Override
    public boolean deleteUser(int userID) {
        Connection con = null;
        PreparedStatement ps = null;
        
        if (userID <= 0) {
            return false;
        }
        
        

        try {
            con = getConnection();

            String query = "CALL delete_user(?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, userID);

            ps.execute();

            
            
            return true;
        } catch (SQLException e) {

            return false;
        } finally {
            try {

                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
    }

    /**
     * This method takes a single parameter, a User object containing all the updated information of 
     * a particular user, and uses it to update the information currently held in the user table with 
     * the new information contained in the object.
     * 
     * @param nUser is a User object which contains all the updated information for a particular user
     * @return <code>true</code> if the users details were updated successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean editUserDetails(User nUser) {
        Connection con = null;
        PreparedStatement ps = null;

        if (nUser == null || nUser.getUserId() == -1) {
            return false;
        }
        

        try {
            con = getConnection();

            String query = "CALL edit_user_details(?,?,?,?,?,?,?,?)";

            ps = con.prepareStatement(query);
            
            ps.setInt(1, nUser.getUserId());
            ps.setString(2, nUser.getfName());
            ps.setString(3, nUser.getlName());
            ps.setString(4, nUser.getEmail());
            ps.setString(5, nUser.getPhone());
            ps.setString(6, nUser.getPassword());
            ps.setBoolean(7, nUser.getIsIsAdmin());
            ps.setInt(8, nUser.getWalletCredits());

            ps.executeUpdate();

            return true;

        } catch (SQLException e) {
            ////e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                ////e.printStackTrace();
                return false;
            }
        }
    }

    /**
     * This method takes 2 parameters, coins which are the amount of credits a user has earned
     * during a transaction and user id which corresponds to a user in the user table, and are used
     * to add this users earned wallet credits to the wallet credits in their account.
     * 
     * @param coins is the amount of wallet credits a user has gained during a transaction
     * @param userId is the id of the user who has recieved the wallet credits
     * @return <code>true</code> if the users wallet credits where updated successfully
     *         <code>false</code> otherwise
     */
    public boolean updateUserWallet(int coins, int userId) {
        Connection con = null;
        PreparedStatement ps = null;

        try {
            con = getConnection();

            String query = "CALL update_user_wallet(?,?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, coins);
            ps.setInt(2, userId);

            int i = ps.executeUpdate();

            if (i != 0) {
                return true;
            }
        } catch (SQLException e) {
            ////e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                ////e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    /**
     * This method takes a single parameter, a users id, and uses it to get a user
     * from the user table and reset their credits back to 0.
     * 
     * @param userId is the id of the user who's wallet credits are to be reset
     * @return <code>true</code> if the users wallet credits were reset successfully
     *         <code>false</code> otherwise
     */
    public boolean resetUserWallet(int userId) {
        Connection con = null;
        PreparedStatement ps = null;

        try {
            con = getConnection();

            String query = "CALL reset_user_wallet(?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, userId);

            int i = ps.executeUpdate();

            if (i != 0) {
                return true;
            }
        } catch (SQLException e) {
            //e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                //e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    /**
     * This method takes a single parameter, a passwordRecovery id, which is used to select a password
     * recovery entry in the PasswordRecovery table and place the information in a password recovery object
     * 
     * @param id is the token value used to identify a users account for the password recovery, 
     * the user has been sent this token value and when the link is clicked the method checks 
     * the database if it has a matching value in it.
     * @return PasswordRecovery, a PasswordRecovery object that contains an id for the object 
     * as well as a users email address and the time of the password recovery
     */
    public PasswordRecovery passwordRecoveryById(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PasswordRecovery item = new PasswordRecovery();

        try {
            con = this.getConnection();
            String query = "CALL password_recovery_by_id(?)";

            ps = con.prepareStatement(query);
            ps.setString(1, id);

            rs = ps.executeQuery();
            if (rs.next()) {

                item.setId(rs.getString("id"));
                item.setEmail(rs.getString("email"));
                item.setTimestamp(rs.getTimestamp("time"));

                return item;
            }
        } catch (SQLException e) {
            item = null;
        } catch (NullPointerException ex) {
            item = null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                System.err.println("A problem occurred when closing down the findUserById method:\n" + e.getMessage());
            }
        }
        return null;
    }

    /**
     * This method takes a single parameter, a passwordRecovery item, and is used to populate 
     * data from the object into an entry into the database.
     * 
     * @param item is a PasswordRecovery objects which contains its id, the user email who 
     * wishes to recover their password and the time of the password recovery
     * @return <code>true</code> if the entry was recorded into the password recovery table
     *         <code>false</code> otherwise
     */
    public boolean registerNewpasswordRecovery(PasswordRecovery item) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (item == null) {
            return false;
        }

        try {
            con = this.getConnection();

            String query = "CALL register_new_password_recovery(?,?)";
            ps = con.prepareStatement(query);

            ps.setString(1, item.getEmail());
            ps.setString(2, item.getId());

            ps.execute();

            return true;
        } catch (SQLException e) {

            if (e instanceof com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException) {
                try {

                    String query = "Update PassRecovery set email=?, time=NOW(), id=? where email=?";
                    ps = con.prepareStatement(query);

                    ps.setString(1, item.getEmail());
                    ps.setString(2, item.getId());
                    ps.setString(3, item.getEmail());

                    int i = ps.executeUpdate();

                    if (i != 0) {
                        return true;
                    }
                } catch (SQLException ex1) {
                    return false;
                }
            }
            System.out.println(e.getClass());
            return false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
    }

    /**
     * This method takes 2 parameters a users email and password, and uses them to 
     * replace the users password relevant to their email.
     * 
     * @param email is the users email that wishes to reset their password
     * @param pass is the users password they wish to change to
     * @return <code>true</code> if the users password was reset successfully
     *         <code>false</code> otherwise
     */
    public boolean passwordResetByEmail(String email, String pass) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PasswordRecovery item = new PasswordRecovery();

        try {
            con = this.getConnection();
            String query = "CALL password_reset_by_email(?,?)";

            ps = con.prepareStatement(query);
            ps.setString(1, pass);
            ps.setString(2, email);

            int i = ps.executeUpdate();

            if (i != 0) {
                return true;
            }
        } catch (SQLException | NullPointerException e) {
            return false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
        return false;
    }

    /**
     * This method takes a single parameter, an email, which is used to delete 
     * the users password recovery entry in the database.
     * 
     * @param email is the users email that was used for password recovery
     * @return <code>true</code> if the passwordRecovery entry was deleted successfully
     *         <code>false</code> otherwise
     */
    public boolean deletePasswordResetByEmail(String email) {
        Connection con = null;
        PreparedStatement ps = null;

        if (email == null) {
            return false;
        }

        try {
            con = getConnection();

            String query = "CALL delete_password_reset_by_email(?)";

            ps = con.prepareStatement(query);

            ps.setString(1, email);

            ps.execute();

            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            try {

                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
    }
    
    /**
     * This method takes 2 parameters, a users emails and their Latch account id, 
     * and uses them to update the users Latch account id.
     * 
     * @param email is the users email
     * @param accountId is the id that the user wishes to change to for their latch
     * @return <code>true</code> if the Latch account id was updated successfully
     *         <code>false</code> otherwise
     */
    public boolean updateUserAccountIdForLatch(String email, String accountId) {
        Connection con = null;
        PreparedStatement ps = null;

        try {
            con = getConnection();

            String query = "CALL update_user_accountid_latch(?,?)";

            ps = con.prepareStatement(query);

            ps.setString(1, email);
            ps.setString(2, accountId);

            int i = ps.executeUpdate();

            if (i != 0) {
                return true;
            }
        } catch (SQLException e) {
            //e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                //e.printStackTrace();
                return false;
            }
        }
        return false;
    }

}
