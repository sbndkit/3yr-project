/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Business.Rating;
import Interfaces.RatingDaoInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.sql.DataSource;

/**
 * This class provides all functionalities in order to interact with the Rating
 * table in the database
 *
 * @inheritDoc This class extends the Dao class which controls the connections with the database,
 * it also implements the RatingDaoInterface to ensure that all necessary methods are implemented
 * the way specified in the interface.
 *
 * @author XPS
 */
public class RatingDao extends Dao implements RatingDaoInterface {

    // Add in constructor that takes in the source of database connections and 
    // pass it up to the super class for storage
    public RatingDao(DataSource myDataSource) {
        super(myDataSource);
    }

    public RatingDao() {
        // Add a blank constructor to allow for default usage
        super();
    }

    /**
     * This method finds all the comments users made on all products and returns
     * them
     *
     * @return ArrayList of type Rating that contains all users comments
     */
    public ArrayList<Rating> FindAllComments() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<Rating> comments = new ArrayList();

        try {
            con = getConnection();

            String query = "CALL find_all_comments()";

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                Rating c = new Rating();
                c.setUserId(rs.getInt("userId"));
                c.setProductId(rs.getInt("productId"));
                c.setComment(rs.getString("comment"));
                c.setRating(rs.getFloat("rating"));

                comments.add(c);

            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return comments;     // comments may be null 
    }

    /**
     * This method takes a single parameter, a product id, and uses it to get
     * all comments related to this product id.
     *
     * @param productId id of the product which we want comments for
     * @return ArrayList of type Rating that contains all users comments for a
     * particular product
     */
    public ArrayList<Rating> FindAllCommentsByProductId(int productId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<Rating> comments = new ArrayList();

        try {
            con = getConnection();

            String query = "CALL find_all_comments_by_product_id(?)";

            ps = con.prepareStatement(query);
            ps.setInt(1, productId);

            rs = ps.executeQuery();

            while (rs.next()) {
                Rating c = new Rating();
                c.setUserId(rs.getInt("userId"));
                c.setProductId(rs.getInt("productId"));
                c.setComment(rs.getString("comment"));
                c.setRating(rs.getInt("rating"));

                comments.add(c);

            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return comments;     // comments may be null 
    }

    /**
     * This method takes a single parameter, a user id, which is used to get
     * comments on products for that user.
     *
     * @param userId is the id of the user which we want to get comments for
     * @return ArrayList of type Rating that contains all comments for a
     * particular user
     */
    public ArrayList<Rating> FindAllCommentsByUserId(int userId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<Rating> comments = new ArrayList();

        try {
            con = getConnection();

            String query = "CALL find_all_comments_by_user_id(?)";

            ps = con.prepareStatement(query);
            ps.setInt(1, userId);

            rs = ps.executeQuery();

            while (rs.next()) {
                Rating c = new Rating();
                c.setUserId(rs.getInt("userId"));
                c.setProductId(rs.getInt("productId"));
                c.setComment(rs.getString("comment"));
                c.setRating(rs.getFloat("rating"));

                comments.add(c);

            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return comments;     // comments may be null 
    }

    /**
     * This method takes three parameters, a user id, product id and a comment
     * which are all used to create an entry in the rating table associating a
     * user and product with a users comment about that product.
     *
     * @param userId is the id of the user which the comment belongs to
     * @param productId is the id of the product the comment is about
     * @param comment is the comment itself about a particular product
     * @return <code>true</code> if the comment was successfully added
     * <code>false</code> otherwise
     */
//    public boolean addComment(int userId, int productId, String comment) {
//        Connection con = null;
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        if (comment == null) {
//            return false;
//        }
//
//        try {
//            con = this.getConnection();
//
//            String query = "CALL add_comment(?, ?, ?);";
//            ps = con.prepareStatement(query);
//
//            ps.setInt(1, userId);
//            ps.setInt(2, productId);
//            ps.setString(3, comment);
//
//            ps.execute();
//
//            return true;
//        } catch (SQLException e) {
//            return false;
//        } finally {
//            try {
//                if (rs != null) {
//                    rs.close();
//                }
//                if (ps != null) {
//                    ps.close();
//                }
//                if (con != null) {
//                    freeConnection(con);
//                }
//            } catch (SQLException e) {
//                return false;
//            }
//        }
//    }
    /**
     * This method takes a single parameter, a user id, which is used to get all
     * comments made by that user and deletes them.
     *
     * @param userID is the users id that is used to delete a users comments
     * @return <code>true</code> if all the users comments where deleted
     * successfully <code>false</code> otherwise
     */
    public boolean deleteAllUserComment(int userID) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (userID < 0) {
            return false;
        }

        try {
            con = getConnection();

            String query = "CALL delete_all_user_comment(?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, userID);

            ps.execute();

            query = "Select * FROM rating WHERE userID = ?";

            ps = con.prepareStatement(query);

            ps.setInt(1, userID);

            rs = ps.executeQuery();

            if (rs.next() == false) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            return false;
        } finally {
            try {

                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
    }

    /**
     * This method takes 2 parameters, a user id and a product id and uses them
     * to find a users comment on a particular product and delete it.
     *
     * @param userID is the id of the user which made the comment
     * @param productId is the product which the comment was made on
     * @return <code>true</code> if the comment was deleted successfully
     * <code>false</code> otherwise
     */
    public boolean deleteUserCommentOnProduct(int userID, int productId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (userID < 0) {
            return false;
        }

        try {
            con = getConnection();

            String query = "CALL delete_all_user_comment_on_product(?,?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, userID);
            ps.setInt(2, productId);

            ps.execute();

            query = "select * FROM rating WHERE userId = ? and productId = ?";

            ps = con.prepareStatement(query);

            ps.setInt(1, userID);
            ps.setInt(2, productId);

            rs = ps.executeQuery();

            if (rs.next() == false) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            return false;
        } finally {
            try {

                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
    }

    /**
     * This method takes 4 parameters, a user id, a product id, a comment and a
     * star rating and uses all these to edit a comment a user had made on a
     * particular product.
     *
     * @param userId is the id of the user which wants to rate the product edit
     * the rating they gave to a product.
     * @param productId is the id of the product that rating is to be edited
     * @param comment is the comment the user wishes to replace there original
     * comment with
     * @param stars is the star rating that the user wishes to give the product
     * @return <code>true</code> if the rating was edited successfully
     * <code>false</code> otherwise
     */
    public boolean editRating(int userId, int productId, String comment, float stars) {
        Connection con = null;
        PreparedStatement ps = null;

        if (comment == null) {
            return false;
        }

        try {
            con = getConnection();

            String query = "CALL edit_rating(?,?,?,?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, userId);
            ps.setInt(2, productId);
            ps.setString(3, comment);
            ps.setFloat(4, stars);

            ps.execute();
            int i = ps.executeUpdate();

            if (i != 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    /**
     *
     * @param userId is the user id of the user that wishes to eave the rating
     * @param productId is the id of the product that is to be rated
     * @param rating is the star rating that the user is giving the product
     * @param comment is the comment the user left on the comment
     * @return <code>true</code> if th rating was successfully added to the
     * rating table <code>false</code> otherwise
     */
    public boolean addRating(int userId, int productId, int rating, String comment) {

        if(userId<=0 || productId<=0 || rating<0)
        {
            return false;
        }
        Connection con = null;
        PreparedStatement ps = null;

        boolean inserted = false;

        try {
            con = getConnection();

            String query = "CALL add_rating(?,?,?,?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, userId);
            ps.setInt(2, productId);
            ps.setInt(3, rating);
            ps.setString(4, comment);

            ps.executeUpdate();

            inserted = true;

        } catch (SQLException e) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                System.out.println("Exception occured in the finally section of the updateRating method");
                System.out.println(e.getMessage());
            }
        }
        return inserted;
    }

    /**
     * This method takes a single parameter, a product id, and uses it to find
     * all ratings made on a product and gets the total and averages it to find
     * the actual rating for a particular product.
     *
     * @param productId is the id of the product we wish to get the rating for
     * @return Float is the rating given to a particular product by all users
     */
    public Float FindTotalRating(int productId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        float totalRating = 0;

        try {
            con = getConnection();

            String query = "SELECT get_total_rating(?)";

            ps = con.prepareStatement(query);
            ps.setInt(1, productId);

            rs = ps.executeQuery();

            while (rs.next()) {

                totalRating += rs.getFloat(1);

            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }

        return totalRating;
    }
}
