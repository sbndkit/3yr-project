/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Interfaces.OrderDaoInterface;
import Business.Order;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.sql.DataSource;

/**
 *This class provides all functionalities in order to interact with the order table in the database
 * 
 * @inheritDoc This class extends the Dao class which controls the connections with the database,
 * it also implements the OrderDaoInterface to ensure that all necessary methods are implemented
 * the way specified in the interface
 * @author Sergio
 */

public class OrderDao extends Dao implements OrderDaoInterface{
    // Add in constructor that takes in the source of database connections and 
    // pass it up to the super class for storage
    public OrderDao(DataSource myDataSource)
    {
        super(myDataSource);
    }
    
    public OrderDao()
    {
        // Add a blank constructor to allow for default usage
        super();
    }
    /**
     * This method gets all the orders from the order table in the database and returns them
     * 
     * @return ArrayList of type Order, An ArrayList of all orders that have been made to date
     */
    @Override
    public ArrayList<Order> getAllOrders()
    {
          
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ArrayList<Order> orders = new ArrayList();
        
        try{
            con = getConnection();

            String query = "CALL get_all_orders()";
            ps = con.prepareStatement(query);
            
            rs = ps.executeQuery(); 
            
            while(rs.next())
            {
                // Make a customer object for the current customer
                Order o = new Order();
                
                //productID,  ProductName, price, description, stock
                o.setOrderId(rs.getInt("orderID"));
                o.setUserId(rs.getInt("userID"));
                o.setDateOrdered(rs.getDate("dateOrdered"));
                o.setTotalPrice(rs.getFloat("totalPrice"));
                
                // Store the current customer object (now filled with information) in the arraylist
                orders.add(o);
            }
        }catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return orders;
    }
    
    /**
     * This method takes a single parameter which is the id of the user we wish to get previous orders
     * relevant to that user
     * 
     * @param id is the id of the user we wish to get orders for
     * @return ArrayList of type Order, is a list of all orders made by a particular user
     */
    @Override
    public ArrayList<Order> getAllOrdersByUserId(int id)
    {
          
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ArrayList<Order> orders = new ArrayList();
        
        try 
        {
            con = getConnection();
            
            String query = "CALL get_all_orders_by_user_id(?)";
            
            ps = con.prepareStatement(query);
            
            ps.setInt(1, id);
            
            rs = ps.executeQuery();
            
            while (rs.next()) 
            {
                // Make a customer object for the current customer
                Order o = new Order();
                
                //productID,  ProductName, price, description, stock
                o.setOrderId(rs.getInt("orderID"));
                o.setUserId(rs.getInt("userID"));
                o.setDateOrdered(rs.getDate("dateOrdered"));
                o.setTotalPrice(rs.getFloat("totalPrice"));
                
                // Store the current customer object (now filled with information) in the arraylist
                orders.add(o);
            }
        }catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return orders;
    }
    
    /**
     * This method takes a single parameter, an order id, and uses it to find
     * a particular order and return it.
     * 
     * @param id is the id of the order to be found
     * @return Order is an object containing all the information for the order was found
     */
    @Override
    public Order getOrderById(int id)
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try 
        {
            con = getConnection();
            
            String query = "CALL get_order_by_id(?)";
            
            ps = con.prepareStatement(query);
            
            ps.setInt(1, id);
            
            rs = ps.executeQuery();
            
            while (rs.next()) 
            {
                Order o = new Order();
                
                o.setOrderId(rs.getInt("orderID"));
                o.setUserId(rs.getInt("userID"));
                o.setDateOrdered(rs.getDate("dateOrdered"));
                o.setTotalPrice(rs.getFloat("totalPrice"));
                
                return o;
            }
        } 
        catch (SQLException e) 
        {
            return null;
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                return null;
            }
        }
        return null;
    }
    
    /**
     * This method takes a single parameter, a new order object, and inserts it into the order table.
     * 
     * @param nOrder is a new order to be placed in the order table
     * @return <code>true</code> if the order was inserted successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean insertOrder(Order nOrder)
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try 
        {
            con = getConnection();
            
            String query = "CALL insert_order(?,?,?,?,?)";
            
            ps = con.prepareStatement(query);
            
            if (nOrder.getOrderId() == -1)
            {
                ps.setString(1, "NULL");
            }
            else
            {
                ps.setInt(1, nOrder.getOrderId());
            }
            
            Date orderD = new Date(nOrder.getDateOrdered().getYear(), nOrder.getDateOrdered().getMonth(),nOrder.getDateOrdered().getDate());
            
            ps.setDate(2, (orderD));
            
            ps.setInt(3, nOrder.getUserId());
            ps.setInt(4, nOrder.getAddressID());
            ps.setFloat(5, nOrder.getTotalPrice());
            
            int i = ps.executeUpdate();
            
            if (i != 0)
            {
                return true;
            }
            
        } 
        catch (SQLException e) 
        {
            return false;
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                return false;
            }
        }
        return false;
    }
    
    /**
     * This method takes a single parameter, an order id, and deletes it from the order table in the database
     * 
     * @param id is the id of the order to be deleted
     * @return <code>true</code> if order was deleted successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean deleteOrderById(int id)
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        if(id < 0)
        {
            return false;
        }
        
        try 
        {
            con = getConnection();
            
            String query = "CALL delete_order_by_id(?)";
            
            ps = con.prepareStatement(query);
            
            ps.setInt(1, id);
            
            int i = ps.executeUpdate(); 
            
            if (i != 0)
            {
                return true;
            }
        } 
        catch (SQLException e) 
        {
            return false;
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                return false;
            }
        }
        return false;
    }
    
    /**
     * This method takes a single parameter, an order object, and updates the 
     * information in the object and stores it back into the order table.
     * 
     * @param nOrder is the order object which we wish to update
     * @return <code>true</code> if the order object was updated successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean updateOrder(Order nOrder)
    {
        Connection con = null;
        PreparedStatement ps = null;
        
        boolean inserted = false;
        
        try{
            con = getConnection();

            String query = "CALL update_order(?,?,?)";
            
            ps = con.prepareStatement(query);
            
            Date orderD = new Date(nOrder.getDateOrdered().getYear(), nOrder.getDateOrdered().getMonth(),nOrder.getDateOrdered().getDate());
            
            ps.setDate(1, orderD);
            ps.setInt(2, nOrder.getUserId());
            ps.setInt(3, nOrder.getOrderId());
            
            
            int i = ps.executeUpdate(); 
            
            if (i != 0)
            {
                inserted = true;
            }
              
        }catch (SQLException e){
            return false;
        }catch  (NullPointerException ex) {
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                //System.out.println("Exception occured in the finally section of the getAllCustomers() method");
                //System.out.println(e.getMessage());
                return false;
            }
        }
        return inserted;
    }
    
    /**
     * This method return the maximum ID stored into the database or -1 if the table is empty.
     * 
     * 
     * @return INTEGER is the id of the last order that was placed on the order table
     */
    @Override
    public int getLastOrderId()
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try 
        {
            con = getConnection();
            
            String query = "CALL get_last_order_id()";
            
            ps = con.prepareStatement(query);
            
            rs = ps.executeQuery();
            
            while (rs.next()) 
            {
                int max = rs.getInt(1);
                
                return max;
            }
        } 
        catch (SQLException e) 
        {
            return -1;
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                return -1;
            }
        }
        return -1;
    }
    
}
