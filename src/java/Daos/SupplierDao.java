/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Business.Supplier;
import Interfaces.SupplierDaoInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.sql.DataSource;


/**
 *This class provides all functionalities in order to interact with the Supplier table in the database
 * 
 * @inheritDoc This class extends the Dao class which controls the connections with the database,
 * it also implements the SupplierDaoInterface to ensure that all necessary methods are implemented
 * the way specified in the interface.
 * 
 * @author Sergio
 */
public class SupplierDao extends Dao implements SupplierDaoInterface {

    // Add in constructor that takes in the source of database connections and 
    // pass it up to the super class for storage
    public SupplierDao(DataSource myDataSource) {
        super(myDataSource);
    }

    public SupplierDao() {
        // Add a blank constructor to allow for default usage
        super();
    }

    /**
     * This method takes no parameters but returns an ArrayList of supplier objects with all the suppliers 
     * in the supplier table in the database.
     * 
     * @return ArrayList of type Supplier. A list of all Suppliers objects listed in the database 
     */
    @Override
    public ArrayList<Supplier> getAllSuppliers() {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<Supplier> suppliers = new ArrayList();

        try {
            con = getConnection();

            String query = "CALL get_all_suppliers()";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                // Make a customer object for the current customer
                Supplier s = new Supplier();

                s.setSupplierId(rs.getInt("supplierID"));
                s.setSupplierName(rs.getString("supplierName"));
                s.setPhone(rs.getString("phone"));
                s.setCounty(rs.getString("county"));
                s.setCountry(rs.getString("country"));
                s.setEmail(rs.getString("email"));
                s.setAddress(rs.getString("Address"));
                // Store the current customer object (now filled with information) in the arraylist
                suppliers.add(s);
            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                System.out.println("Exception occured in the finally section of the getAllCustomers() method");
                System.out.println(e.getMessage());
            }
        }
        return suppliers;
    }

    /**
     * This method takes a single parameter, a suppliers id, and uses it 
     * to find that supplier in the supplier table.
     * 
     * @param id is the id of the supplier which is to be found
     * @return Supplier is a Supplier object containing all that suppliers information
     */
    @Override
    public Supplier getSupplierById(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = getConnection();

            String query = "CALL get_supplier_by_id(?)";
            ps = con.prepareStatement(query);
            ps.setInt(1, id);

            rs = ps.executeQuery();

            if (rs.next()) {
                // Make a customer object for the current customer
                Supplier s = new Supplier();

                s.setSupplierId(rs.getInt("supplierID"));
                s.setSupplierName(rs.getString("supplierName"));
                s.setPhone(rs.getString("phone"));
                s.setEmail(rs.getString("email"));
                s.setAddress(rs.getString("Address"));
                s.setCounty(rs.getString("County"));
                s.setCountry(rs.getString("Country"));

                return s;
            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return null;
    }

    /**
     * This method takes a single parameter, a Supplier object, 
     * who's information is extracted from the object and recorded in the suppler table.
     * 
     * @param nSupplier is a Supplier object which is used to take information from 
     * and place it in a new entry in the supplier table
     * @return <code>true</code> if the supplier was inserted into the supplier table successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean insertSupplier(Supplier nSupplier) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean inserted = false;

        try {
            con = getConnection();

            String query = "CALL insert_supplier(?,?,?,?,?,?)";
            ps = con.prepareStatement(query);

            ps.setString(1, nSupplier.getSupplierName());
            ps.setString(2, nSupplier.getAddress());
            ps.setString(3, nSupplier.getCounty());
            ps.setString(4, nSupplier.getCountry());
            ps.setString(5, nSupplier.getEmail());
            ps.setString(6, nSupplier.getPhone());

            ps.execute();

            inserted = true;

        } catch (SQLException e) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                System.out.println("Exception occured in the finally section of the getAllCustomers() method");
                System.out.println(e.getMessage());
            }
        }
        return inserted;
    }

    /**
     * This method takes a single parameter, a supplier id, which is used to find a supplier 
     * in the supplier table and delete the entry.
     * 
     * @param id is the id of the supplier that is to be deleted
     * @return <code>true</code> if the supplier was deleted successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean deleteSupplierById(int id) {
        Connection con = null;
        PreparedStatement ps = null;

        if (id < 0) {
            return false;
        }

        try {
            con = getConnection();

            String query = "CALL delete_supplier_by_id(?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, id);

            ps.executeUpdate();

            
            return true;
            
        } catch (SQLException e) {
            return false;
        } finally {
            try {

                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
    }

    /**
     * This method takes a single parameter, a supplier object, 
     * which is taken in and the information is extracted from to 
     * update a particular entry in the supplier table
     * 
     * @param nSupplier is a supplier object containing all the new information 
     * to be updated to a particular entry in the supplier table.
     * @return <code>true</code> if the supplier was updated successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean updateSupplier(Supplier nSupplier) {
        Connection con = null;
        PreparedStatement ps = null;

        if (nSupplier == null) {
            return false;
        }

        try {
            con = getConnection();

            String query = "CALL update_supplier(?,?,?,?,?,?,?)";

            ps = con.prepareStatement(query);

            ps.setInt(1, nSupplier.getSupplierId());
            ps.setString(2, nSupplier.getSupplierName());
            ps.setString(3, nSupplier.getAddress());
            ps.setString(4, nSupplier.getCounty());
            ps.setString(5, nSupplier.getCountry());
            ps.setString(6, nSupplier.getEmail());
            ps.setString(7, nSupplier.getPhone());
            

            ps.executeUpdate();

            return true;

        } catch (SQLException e) {
            //e.printStackTrace();
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                //e.printStackTrace();
                return false;
            }
        }
    }

}
