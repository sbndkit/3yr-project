/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Business.Address;
import Interfaces.AddressDaoInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.sql.DataSource;


/**
 * 
 * @author Sergio
 * 
 * This class provide all the necessary functionalities in order to interact 
 * with the table Address.
 * 
 * @inheritDoc This class extends the Dao class which controls the connections with the database,
 * it also implements the AddressDaoInterface to ensure that all necessary methods are implemented
 * the way specified in the interface
 */
public class AddressDao extends Dao implements AddressDaoInterface{
    
    // Add in constructor that takes in the source of database connections and 
    // pass it up to the super class for storage
    public AddressDao(DataSource myDataSource) {
        super(myDataSource);
    }

    public AddressDao() {
        // Add a blank constructor to allow for default usage
        super();
    }
    
    

    /**
     *
     * Method getAllUserAddress takes in the userID (int) and returns an
 ArrayList<Address>
     * object that contains all the addresses related with this user ID.
     *
     * @param userID
     * @return ArrayList<Address>
     */
    @Override
    public ArrayList<Address> getAllUserAddress(int userID) {
        // objects for stablishing the connection
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        // ArrayList<address> declaration for storing all the user addresses
        ArrayList<Address> addresses = new ArrayList();

        try {
            // requesting a connection
            con = getConnection();

            // creating the query
            String query = "CALL get_all_address_by_user_id(?)";
            ps = con.prepareStatement(query);

            // preparing the query
            ps.setInt(1, userID);

            // executing the query
            rs = ps.executeQuery();

            // This while loop stores all the addresses retreaved from the database into the ArrayList
            while (rs.next()) {
                // Make a customer object for the current customer
                Address a = new Address();

                //productID,  ProductName, price, description, stock
                a.setAddress(rs.getString("address"));
                a.setAddressID(rs.getInt("addressID"));
                a.setCountry(rs.getString("country"));
                a.setCounty(rs.getString("county"));
                a.setUserID(rs.getInt("userID"));

                // Store the current customer object (now filled with information) in the arraylist
                addresses.add(a);
            }
            // Catching any possible exception
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        // returning the addresses ArrayList
        return addresses;
    }

    /**
     *  Method insertAddres takes a new Address object (newAddress) and 
     * stores this information into the table Address. 
     *
     * 
     * @param newAddress is the new address to be added to the address table
     * @return boolean <code>true</code> if the address was inserted successfully
     *                 <code>false</code>otherwise
     */
    @Override
    public boolean insertAddres(Address newAddress) {
        // Objects for stablishing the connection
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        // Checking that the Address object received is not null
        if (newAddress == null) {
            return false;
        }

        try {
            // Getting the connection
            con = getConnection();

            // Preparing the query
            String query = "CALL register_address(?,?,?,?,?)";

            // Preparing the statement for the insertion
            ps = con.prepareStatement(query);
            
            int addressId = newAddress.getAddressID();
            if(addressId != -1)
            {
                ps.setInt(1, addressId);
            }else{
                ps.setString(1, "null");
            }
            ps.setInt(2, newAddress.getUserID());
            ps.setString(3, newAddress.getAddress());
            ps.setString(4, newAddress.getCounty());
            ps.setString(5, newAddress.getCountry());

            // Executing the query and storing the response
            // if i>0 the information was inserted
            // if i==0 no information inserted into the database
            int i = ps.executeUpdate();

            // Returning true if the response is > than 0
           
                return true;
            

            // catching any possible exception
        } catch (SQLException e) {
            return false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }

    }
    
    /**
     * Method deleteAddressById takes an Address object ID and 
     * delete this information from the table Address. 
     *
     * 
     * @param addressId is the id of the address assigned to a particular user
     * @return boolean value <code>true</code> if the address was deleted successfully
     *                       <code>false</code> otherwise
     */
    @Override
    public boolean deleteAddressById(int addressId) {
        // creating objects for the connection to the database
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        // Checking that the address id received is valid (addressId >= 0)
        if(addressId <= 0)
        {
            return false;
        }
        
        try 
        {
            // Getting a new connection
            con = getConnection();
            
            //Preparing the SQL satement
            String query = "CALL delete_address_by_address_id(?)";
            
            ps = con.prepareStatement(query);
            
            ps.setInt(1, addressId);
            
            // executing the call to the SQL sted procedure and storing the 
            //response into an int
            int i = ps.executeUpdate(); 
            
            // The operation is succesful if the value received is > than 0
            if (i != 0)
            {
                return true;
            }
        } 
        //Catching possible exceptions
        catch (SQLException e) 
        {
            return false;
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                return false;
            }
        }
        // Returnig false if there is no information deleted and no exceptions throunw 
        return false;
    }
    
    /**
     * Method deleteAllAddressesById takes an User ID and 
     * delete all the addresses associated to this user from the Address table.
     *  
     * 
     * 
     * @param userID is the id of the user we wish to delete all addresses for
     * @return boolean value <code>true</code> if all addresses were deleted successfully
     *                       <code>false</code> otherwise
     */
    @Override
    public boolean deleteAllAddressesById(int userID) {
        // creating objects for the connection to the database
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        // Checking that the address id received is valid (userID >= 0)
        if(userID <= 0)
        {
            return false;
        }
        
        try 
        {
            // Getting a new connection
            con = getConnection();
            
            //Preparing the SQL satement
            String query = "CALL delete_all_user_address(?)";
            
            ps = con.prepareStatement(query);
            
            ps.setInt(1, userID);
            
            // executing the call to the SQL sted procedure and storing the 
            //response into an int
            int i = ps.executeUpdate(); 
            
            // The operation is succesful if the value received is > than 0
            if (i != 0)
            {
                return true;
            }
        } 
        //Catching possible exceptions
        catch (SQLException e) 
        {
            return false;
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                return false;
            }
        }
        // Returnig false if there is no information deleted and no exceptions throunw 
        return false;
    }
    
    /**
     * Method updateAddress takes an Address object ID and 
     * update the information stored into the table Address. 
     * 
     * 
     * @param newAddress
     * @return boolean value <code>true</code> if the address was deleted successfully
     *                       <code>false</code> otherwise
     */
    @Override
    public boolean updateAddress(Address newAddress) {
        // necessary objects for the connection
        Connection con = null;
        PreparedStatement ps = null;
        boolean inserted = false;
        
        try{
            // Geting a connection
            con = getConnection();
            
            // Preparing the SQL stament
            String query = "CALL edit_user_address(?,?,?,?)";
            
            ps = con.prepareStatement(query);
            
            ps.setInt(1, newAddress.getAddressID());
            ps.setString(2, newAddress.getAddress());
            ps.setString(3, newAddress.getCounty());
            ps.setString(4, newAddress.getCountry());
            
            // Executing the statement via call to a stored procedure
            int i = ps.executeUpdate(); 
            
                if(i!=0)
                {
                inserted = true;
                }
                
            
              
        // Catching possible exceptions
        }catch (SQLException e){
            return false;
        }catch  (NullPointerException ex) {
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
        // Returning false if the information wasn't update.
        return inserted;
    }
    
    
    /**
     * Method returns the last id inserted for this userID
     * 
     * @param userID is the id of the user that we wish to get the last used address for
     * @return Integer informing us if the selection was successful or not
     */
    public int selectLastIdAdded(int userID) {
        // necessary objects for the connection
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try{
            // Geting a connection
            con = getConnection();
            
            // Preparing the SQL stament
            String query = "CALL get_last_address(?)";
            
            ps = con.prepareStatement(query);
            
            ps.setInt(1, userID);
            
            // Executing the statement via call to a stored procedure
            rs = ps.executeQuery(); 
            
            // Checking that the ResultSet is not empty
            if(rs.next())
            {
                return rs.getInt(1);
            }
            
              
        // Catching possible exceptions
        }catch (SQLException e){
            return -1;
        }catch  (NullPointerException ex) {
            return -1;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return -1;
            }
        }
        // Returning false if the information wasn't update.
        return -1;
    }
    
    

}
