package Daos;

import java.sql.SQLException;
/**
 * This class is used to define a DaoException constructor 
 * and output a message should this exception occur.
 * 
 * @author XPS
 * @inheritDoc This class extends the SQLException class which handles the errors thrown by SQL statements
 */
public class DaoException extends SQLException  
{
    /**
     * Default constructor for DaoException
     */
    public DaoException() {
    }

    /**
     * Parameterised constructor for DaoException used to alert the user if an error
     * occurred while trying to gain a connection to the database
     * 
     * @param aMessage is the message we wish to give to once the exception is thrown
     */
    public DaoException(String aMessage) 
    {
        super(aMessage);
    }
}