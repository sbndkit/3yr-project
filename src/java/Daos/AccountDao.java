/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Business.Account;
import Interfaces.AccountDaoInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 * The AccountDao class is used to get a connection with the database
 * and call procedures in each method
 * 
 * @inheritDoc This class extends the Dao class which controls the connections with the database,
 * it also implements the AccountDaoInterface to ensure that all necessary methods are implemented
 * the way specified in the interface
 * @author Sergio
 */
public class AccountDao extends Dao implements AccountDaoInterface{
    public AccountDao(DataSource myDataSource)
    {
        super(myDataSource);
    }
    
    public AccountDao()
    {
        // Add a blank constructor to allow for default usage
        super();
    }
    
    /**
     * update(String email) method recieves a single parameter to associate an email with user account
     * Modifies account data for user and returns a true or false value to signify 
     * if the update was successful or not
     *  
     * 
     * 
     * @param email is the email registered to a particular customer.
     * @return <code>true</code> if the account was updated successfully.
     *         <code>false</code> otherwise.
     */
    public boolean update(String email)
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "CALL update_account(?)";
        
        try {
            con = this.getConnection();
            
            ps = con.prepareStatement(query);
            
            ps.setString(1,email);
            
            int isIn = ps.executeUpdate();
            
            if(isIn == 0)
            {
                return false;
            }
            return true;
            
        } catch (SQLException ex) {
            return false;
        } 
        finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
    }
    
    /**
     * getStatus(String email) method recieves a single parameter to associate an email with user account
     * returns user account associated with the email if one was found.
     * 
     * 
     * @param email is the email registered to a particular customer.
     * @return a user account object if one was found,
     *           null if no user account was found
     */
    public Account getStatus(String email)
    {
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = this.getConnection();
            
            if (email == null) {
                return null;
            }
            
            String query = "CALL get_status(?)";

            ps = con.prepareStatement(query);

            ps.setString(1, email);
            
            rs = ps.executeQuery();
            if(rs.next()){
                Account a = new Account(); 
                
                a.setAccountStatus(rs.getString("accountStatus"));
                a.setAttempts(rs.getInt("Attempts"));
                a.setTimestamp(rs.getTimestamp("Time"));
                a.setEmail(rs.getString("email"));
                
                return a;
                
            }else{
                return null;
            }
        } catch (SQLException ex) {
            return null;
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        
        

    }
    
    
    /**
     * reset(String email) takes a single parameter, an email, associated with a users account
     * resets the users account so that the users failed login attempts are cleared
     * 
     * 
     * @param email is the email registered to a particular customer.
     * @return <code>true</code> if the account was successfully reset
     *         <code>false</code> otherwise
     */
    public boolean reset(String email){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "CALL reset(?)";
        
        if(email==null)
        {
            return false;
        }
        
        try {
            con = this.getConnection();
            
            ps = con.prepareStatement(query);
            
            ps.setString(1,email);
            
            ps.execute();
            
            return true;
            
        } catch (SQLException ex) {
            return false;
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return false;
            }
        }
    }
    
    
    /**
     * getTimeDifference(String email) recieves a single parameter, an email, associated with a users account,
     * uses this email to get associated account and calculates the difference in time since the last failed
     * login time and the current login time.
     * 
     * 
     * @param email is the email registered to a particular customer.
     * @return the time difference between the last time the user failed 
     * to login and the current login time or -1 to signify that the account was not found
     */
    public long getTimeDifference(String email){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "CALL get_time_difference(?)";
        
        try {
            con = this.getConnection();
            
            ps = con.prepareStatement(query);
            
            ps.setString(1,email);
            
            
            rs = ps.executeQuery();
            
            if(rs.next()){
                return rs.getLong(1);
            }
            return -1;
            
        } catch (SQLException ex) {
            return -1;
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return -1;
            }
        }
    }
}
