/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Business.HashMap;
import Interfaces.ProductDaoInterface;
import Business.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.sql.DataSource;

/**
 *This class provides all functionalities in order to interact with the Product table in the database
 * 
 * @inheritDoc This class extends the Dao class which controls the connections with the database,
 * it also implements the ProductDaoInterface to ensure that all necessary methods are implemented
 * the way specified in the interface.
 * 
 * @author Sergio
 */
public class ProductDao extends Dao implements ProductDaoInterface {

    // Add in constructor that takes in the source of database connections and 
    // pass it up to the super class for storage
    public ProductDao(DataSource myDataSource)
    {
        super(myDataSource);
    }
    
    public ProductDao()
    {
        // Add a blank constructor to allow for default usage
        super();
    }
    // This method returns all the mobiles in the database.
    
    /**
     * This method returns all mobile products in the database into a 
     * hashMap with the key being the brand of the product
     * 
     * @return HashMap of type String for key and type Product for value
     */
    @Override
    public HashMap<String, Product> getAllProducts() {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        HashMap<String, Product> products;

        try {
            con = getConnection();

            Statement rows = con.createStatement();

            rs = rows.executeQuery("CALL count_brands()");
            int i = 1;
            if (rs.next()) {
                i = rs.getInt(1);
            }

            products = new HashMap(i);

            rs = null;

            String query = "CALL get_all_products()";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                // Make a customer object for the current customer
                Product p = new Product();

                //productID,  ProductName, price, description, stock
                p.setProductID(rs.getInt("productID"));
                p.setProductName(rs.getString("ProductName"));
                p.setPrice(rs.getDouble("price"));
                p.setDesc(rs.getString("description"));
                p.setStock(rs.getInt("stock"));
                p.setPictureLink(rs.getString("pictureLink"));
                p.setBrand(rs.getString("brand"));

                // Store the current customer object (now filled with information) in the arraylist
                products.put(p.getBrand(), p);
            }

        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                System.out.println("Exception occured in the finally section of the getAllCustomers() method");
                System.out.println(e.getMessage());
            }
        }
        return products;
    }
    
    /**
     * This method takes a single parameter, an order id, and returns an arrayList of 
     * all the products contained on that order.
     * 
     * @param orderID is the id of the order in which we want to get the products for
     * @return ArrayList of type Product, A List of products that is on a particular order
     */
    public ArrayList<Product> getAllProductsByOrderId(int orderID){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ArrayList<Product> products = new ArrayList();
        
        try {
            con = getConnection();
            
            String query = "CALL get_all_products_by_order_id(?)";
            ps = con.prepareStatement(query);
            ps.setInt(1, orderID);
            
            rs = ps.executeQuery();

            while (rs.next()) {
                // Make a customer object for the current customer
                Product p = new Product();

                //productID,  ProductName, price, description, stock
                p.setProductID(rs.getInt("productID"));
                p.setProductName(rs.getString("ProductName"));
                p.setPrice(rs.getDouble("price"));
                p.setDesc(rs.getString("description"));
                p.setStock(rs.getInt("stock"));
                p.setPictureLink(rs.getString("pictureLink"));
                p.setBrand(rs.getString("brand"));

                // Store the current customer object (now filled with information) in the arraylist
                products.add(p);
            }

        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                System.out.println("Exception occured in the finally section of the getAllCustomers() method");
                System.out.println(e.getMessage());
            }
        }
        return products;
    }

    /**
     * This method takes in a single parameter, a product id, and uses it to 
     * find a product in the product table and return it.
     * 
     * @param ID is the id of the product which is to be found
     * @return Product is a Product object containing all the information 
     * for the product that is being searched for.
     */
    @Override
    public Product getProduct(int ID) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = getConnection();

            String query = "CALL get_product(?)";
            ps = con.prepareStatement(query);
            ps.setInt(1, ID);
            rs = ps.executeQuery();

            if (rs.next()) {
                // Make a customer object for the current customer
                Product p = new Product();

                //productID,  ProductName, price, description, stock
                p.setProductID(rs.getInt("productID"));
                p.setProductName(rs.getString("ProductName"));
                p.setPrice(rs.getDouble("price"));
                p.setDesc(rs.getString("description"));
                p.setStock(rs.getInt("stock"));
                p.setPictureLink(rs.getString("pictureLink"));
                p.setBrand(rs.getString("brand"));

                return p;
            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
        return null;
    }

    /**
     * This method takes in a single parameter, the name of a product, the method then searches for the 
     * product in the product table and returns it if found.
     * 
     * @param name is the name of a particular product
     * @return HashMap of type String for key and type Product for value
     */
    @Override
    public HashMap<String, Product> getProductLikeName(String name) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        HashMap<String, Product> products;

        try {
            con = getConnection();

            Statement rows = con.createStatement();

            rs = rows.executeQuery("CALL count_brands()");
            int i = 1;
            if (rs.next()) {
                i = rs.getInt(1);
            }

            products = new HashMap(i);

            rs = null;

            String query = "CALL get_product_like_name(?)";
            ps = con.prepareStatement(query);
            ps.setString(1, "%" + name + "%");
            rs = ps.executeQuery();


            while (rs.next()) {
                // Make a customer object for the current customer
                Product p = new Product();

                //productID,  ProductName, price, description, stock
                p.setProductID(rs.getInt("productID"));
                p.setProductName(rs.getString("ProductName"));
                p.setPrice(rs.getDouble("price"));
                p.setDesc(rs.getString("description"));
                p.setStock(rs.getInt("stock"));
                p.setPictureLink(rs.getString("pictureLink"));
                p.setBrand(rs.getString("brand"));

                products.put(p.getBrand(), p);
            }
            return products;
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                return null;
            }
        }
    }

    /**
     * This method takes a single parameter, a Product object, and creates a new entry in the product table 
     * and transfers the data from the object to the database entry.
     * 
     * @param nProduct is a Product object which contains all the information for 
     * a new product to be registered to the product table.
     * @return <code>true</code> if the product was successfully added to the product table
     *         <code>false</code> otherwise
     */
    @Override
    public boolean registerProduct(Product nProduct) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean inserted = false;

        try {
            con = getConnection();

            String query = "CALL register_product(?,?,?,?,?,?);";
            ps = con.prepareStatement(query);

            ps.setString(1, nProduct.getProductName());
            ps.setDouble(2, nProduct.getPrice());
            ps.setString(3, nProduct.getDesc());
            ps.setInt(4, nProduct.getStock());
            ps.setString(5, nProduct.getBrand());
            ps.setString(6, nProduct.getPictureLink());

            ps.executeUpdate();

            inserted = true;

        } catch (SQLException e) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                System.out.println("Exception occured in the finally section of the getAllCustomers() method");
                System.out.println(e.getMessage());
            }
        }
        return inserted;
    }

    /**
     * This method takes a single parameter, a product id, and deletes the 
     * product related to this id from the Product table.
     * 
     * @param id is the id of the product to be deleted
     * @return <code>true</code> if the product was deleted successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean deleteProduct(int id) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean inserted = false;

        try {
            con = getConnection();

            String query = "CALL delete_product_by_id(?)";
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            int i = ps.executeUpdate();

            if (i != 0) {
                inserted = true;
            }

        } catch (SQLException e) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                System.out.println("Exception occured in the finally section of the getAllCustomers() method");
                System.out.println(e.getMessage());
            }
        }
        return inserted;
    }

    /**
     * This method takes a single parameter, a product name, and returns the product associated with that name
     * 
     * @param productName is the name of the product that is to be deleted
     * @return <code>true</code> if the product was deleted successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean deleteProduct(String productName) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean inserted = false;

        try {
            con = getConnection();

            String query = "CALL delete_product_by_name(?)";
            ps = con.prepareStatement(query);

            ps.setString(1, productName);

            int i = ps.executeUpdate();

            if (i != 0) {
                inserted = true;
            }

        } catch (SQLException e) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                System.out.println("Exception occured in the finally section of the getAllCustomers() method");
                System.out.println(e.getMessage());
            }
        }
        return inserted;
    }

     /**
     * This method takes a single parameter, a product object, and uses it to update 
     * the information of the entry in the Product table with the same id
     * 
     * @param nProduct is the product object that has the new information to update the entry with
     * @return <code>true</code> if the product was updated successfully
     *         <code>false</code> otherwise
     */
    @Override
    public boolean updateProduct(Product nProduct) {

        Connection con = null;
        PreparedStatement ps = null;

        boolean inserted = false;

        try {
            con = getConnection();

            String query = "CALL update_product(?,?,?,?,?,?,?)";

            ps = con.prepareStatement(query);

            ps.setString(1, nProduct.getProductName());
            ps.setDouble(2, nProduct.getPrice());
            ps.setString(3, nProduct.getDesc());
            ps.setInt(4, nProduct.getStock());
            ps.setString(5, nProduct.getBrand());
            ps.setString(6, nProduct.getPictureLink());
            ps.setInt(7, nProduct.getProductID());

            int i = ps.executeUpdate();

            if (i != 0) {
                inserted = true;
            }

        } catch (SQLException e) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                System.out.println("Exception occured in the finally section of the getAllCustomers() method");
                System.out.println(e.getMessage());
            }
        }
        return inserted;
    }

    

}
