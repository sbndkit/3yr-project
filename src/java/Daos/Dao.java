/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Sergio
 * 
 * This class provides all necessary functionalities to get and release
 * database connections using connection pooling to do so.
 */
public class Dao
{
    private DataSource datasource;
    
    public Dao(DataSource myDataSource)
    {
        this.datasource = myDataSource;
    }
     // Constructor for use by pooled connections
    /**
     * Constructor for use by pooled connections
     */
    public Dao()
    {
        Connection con = null;
        String DATASOURCE_CONTEXT = "jdbc/PhoneStoreProject";
        try {
            Context initialContext = new InitialContext();
            javax.sql.DataSource ds = (javax.sql.DataSource)initialContext.lookup("java:comp/env/" + DATASOURCE_CONTEXT);
            if(ds != null){
                datasource = ds;
            }
            else{
		System.out.println(("Failed to lookup datasource."));
            }
        }catch (NamingException ex ){
            System.out.println("Cannot get connection: " + ex);
        }
    }
    // Original version of getConnection
//    public Connection getConnection()
//    {
//
//        String driver = "com.mysql.jdbc.Driver";
//        String url = "jdbc:mysql://localhost:3306/MobileShop";
//        String username = "root";
//        String password = "";
//        Connection con = null;
//        try {
//            Class.forName(driver);
//            con = DriverManager.getConnection(url, username, password);
//        } catch (ClassNotFoundException ex1) {
//            System.out.println("Failed to find driver class " + ex1.getMessage());
//            System.exit(1);
//        } catch (SQLException ex2) {
//            System.out.println("Connection failed " + ex2.getMessage());
//            System.exit(2);
//        }
//        return con;
//    }
    
    /**
     * This method is used to get a connection to the database using connection pooling.
     * 
     * @return Connection returns a connection to the database
     * @throws DaoException if something goes wrong with getting a connection
     */
    public Connection getConnection() throws DaoException
    {
        Connection conn = null;
        try{
            if (datasource != null) {
                conn = datasource.getConnection();
            }else {
                System.out.println(("Failed to lookup datasource."));
            }
        }catch (SQLException ex2){
            System.out.println("Connection failed " + ex2.getMessage());
            System.exit(2); // Abnormal termination
        }
        return conn;
    }

    /**
     * This method is used to free a particular connection to the database
     * 
     * @param con is the connection we want to disconnect
     */
    public void freeConnection(Connection con)
    {
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException e) {
            System.out.println("Failed to free connection: " + e.getMessage());
            System.exit(1);
        }
    }

}
