package Factory;


import Commands.Rating.*;
import Commands.Command;
import Commands.Communication.PassResetCommand;
import Commands.Communication.PasswordRecoveryCommand;
import Commands.Communication.ResetPasswordCommand;
import Commands.Latch.LatchPairCommand;
import Commands.Latch.LatchUnpairCommand;
import Commands.Narrator.NarratorOffCommand;
import Commands.Narrator.NarratorOnCommand;
import Commands.Order.*;
import Commands.OrderProduct.*;
import Commands.Product.*;
import Commands.Supplier.*;
import Commands.User.*;

public class CommandFactory {

    public Command createCommand(String action) {
        Command command = null;

        // Selecting the action
        switch (action) {
            // User login
            case "login":
                command = new LoginCommand();
                break;
            // Registering a new user
            case "registernewuser":
                command = new RegisterNewUserCommand();
                break;
            // Getting the list of users form the database
            case "getallusers":
                command = new GetAllUsersCommand();
                break;
            // Getting specific user by ID;
            case "getuserbyid":
                command = new GetUserByIdCommand();
                break;
            // Getting specific user by name and/or surname
            case "getuserbyname":
                command = new GetUserByNameCommand();
                break;
            // User login
            case "deleteuser":
                command = new DeleteUserCommand();
                break;
            // Registering a new user
            case "updateuserdetails":
                command = new UpdateUserDetailsCommand();
                break;
            // Getting the list of users form the database
            case "listofusers":
                command = new UpdateUserDetailsCommand();
                break;
            // Getting specific user by ID;
            case "getallsuppliers":
                command = new GetAllSuppliersCommand();
                break;
            // Getting specific user by name and/or surname
            case "getsupplierbyid":
                command = new GetSupplierByIDCommand();
                break;
            // User login
            case "registernewsupplier":
                command = new RegisterNewSupplierCommand();
                break;
            // Registering a new user
            case "register":
                command = new RegisterNewUserCommand();
                break;
            // Getting the list of users form the database
            case "updatesupplier":
                command = new UpdateSupplierCommand();
                break;
            // Getting specific user by ID;
            case "deleteproductbyid":
                command = new DeleteProductByIdCommand();
                break;
            // Getting specific user by name and/or surname
            case "deleteproductbyname":
                command = new DeleteProductByNameCommand();
                break;
            // User login
            case "getallproducts":
                command = new GetAllProductsCommand();
                break;
            // Registering a new user
            case "getproductbyid":
                command = new GetProductByIdCommand();
                break;
            // Getting the list of users form the database
            case "getproductbyname":
                command = new GetProductByNameCommand();
                break;
            // Getting specific user by ID;
            case "registernewproduct":
                command = new RegisterNewProductCommand();
                break;
            // Getting specific user by name and/or surname
            case "updateproduct":
                command = new UpdateProductCommand();
                break;
            // Getting specific user by ID;
            case "deleteorderproductbyid":
                command = new DeleteOrderProductByIdCommand();
                break;
            // Getting specific user by name and/or surname
            case "getallorderproducts":
                command = new GetAllOrderProductsCommand();
                break;
            // User login
            case "getorderproductbyid":
                command = new GetOrderProductByIDCommand();
                break;
            // Registering a new user
            case "registerneworderproduct":
                command = new RegisterNewOrderProductCommand();
                break;
            // Getting the list of users form the database
            case "updateorderproduct":
                command = new UpdateOrderProductCommand();
                break;
            // Getting specific user by ID;
            case "deleteorderbyid":
                command = new DeleteOrderByIdCommand();
                break;
            // Getting specific user by name and/or surname
            case "getallorders":
                command = new GetOrdersByUserCommand();
                break;
            // Getting specific user by ID;
            case "getorderbyid":
                command = new GetOrderByIDCommand();
                break;
            case "getalluserstodelete":
                command = new GetAllUsersToDeleteCommand();
                break;
            case "logout":
                command = new LogoutCommand();
                break;
            case "getallsuppliersfordelete":
                command = new GetAllSuppliersForDeleteCommand();
                break;
            case "deletesupplierbyid":
                command = new DeleteSupplierByIdCommand();
                break;
            case "getallproductstodelete":
                command = new GetAllProductsToDeleteCommand();
                break;
            case "getallordersforedit":
                command = new GetAllOrdersCommand();
                break;
            case "deleteproductfromcart":
                command = new DeleteProductFromCartCommand();
                break;
            case "updateqtty":
                command = new UpdateCartQttyCommand();
                break;
            case "applyfilters":
                command = new FilterProductsCommand();
                break;
            case "generateneworder":
                command = new GenerateNewOrderCommand();
                break;
            case "addreview":
                command = new AddRatingCommand();
                break;
            case "viewuserscomments":
                command = new ViewUserRatingCommand();
                break;
            case "updateuserrating":
                command = new UpdateUserRatingCommand();
                break;
            case "deleteuserrating":
                command = new DeleteRatingCommand();
                break;
            case "getorderdetails":
                command = new GetOrderDetailsCommand();
                break;
            case "passwordrecovery":
                command = new PasswordRecoveryCommand();
                break;
            case "passreset":
                command = new PassResetCommand();
                break;
            case "resetpassword":
                command = new ResetPasswordCommand();
                break;
            case "pairlatch":
                command = new LatchPairCommand();
                break;
            case "unpairlatch":
                command = new LatchUnpairCommand();
                break;
            case "narratoron":
                command = new NarratorOnCommand();
                break;
            case "narratoroff":
                command = new NarratorOffCommand();
                break;
            default:
                command = null;
                break;
        }

        //Return the instantiated Command object to the calling code...
        return command;// may be null
    }
}