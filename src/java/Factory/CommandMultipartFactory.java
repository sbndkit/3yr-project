/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Factory;

import Commands.Command;
import Commands.Multipart.MultipartAddProductCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sergio
 */
public class CommandMultipartFactory {

    public Command createCommand(HttpServletRequest request, HttpServletResponse response) {

        Command command = null;
        String action = "registernewproduct";
        // Selecting the action
        switch (action) {
            // User login
            case "registernewproduct":
                command = new MultipartAddProductCommand();
                break;
            default:
                command = null;
                break;
        }

        //Return the instantiated Command object to the calling code...
        return command;
    }
}
