/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Objects;

/**
 * The Rating class is used by our application as a method of storing customer
 * input about our products in the form of a rating and comment about that
 * product
 *
 * @author XPS
 */
public class Rating {

    int productId;
    int userId;
    float rating;
    String comment;

    public Rating() {
    }

    /**
     * The Rating parameterized constructor is used for creating new ratings in
     * our application. It takes in several parameters that have been deemed
     * necessary by us for a Rating object
     *
     * @param productId This is stored as an int and is the unique id of the product
     * objected being rated eg.(1)
     * @param userId This is stored as an int and is the unique id of the user that
     * rated the project eg.(10)
     * @param rating This is stored as an int and is the rating they gave the
     * product eg.(2)
     * @param comment This is stored as an String and is the comment then left
     * about the product eg.(I loved this phone)
     */
    public Rating(int productId, int userId, int rating, String comment) {
        this.productId = productId;
        this.userId = userId;
        this.rating = rating;
        this.comment = comment;
    }

    /**
     * Method to retrieve the productId variable which is stored as an Int
     *
     * @return productId being the id of the current product object
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Allows a user to change the product's id stored in the rating object
     *
     * @param productId the new value(Int) to stored as the productId
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * Method to retrieve the userId variable which is stored as an Int
     *
     * @return userId being the id of the user in the current rating object
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Allows a user to change the user's id stored in the rating object
     *
     * @param userId the new value(Int) to stored as the userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Method to retrieve the rating variable which is stored as an Int
     *
     * @return rating being the rating of the product in the rating object
     */
    public float getRating() {
        return rating;
    }

    /**
     * Allows a user to change the rating value stored rating object
     *
     * @param rating the new value(Int) to stored as the rating
     */
    public void setRating(float rating) {
        this.rating = rating;
    }

    /**
     * Method to retrieve the comment variable which is stored as an String
     *
     * @return comment being the user's comments about the product in the rating
     * object
     */
    public String getComment() {
        return comment;
    }

    /**
     * Allows a user to change the comment value stored rating object
     *
     * @param comment the new value(String) to stored as the comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * The hashCode method takes the variables in the Rating class and converts
     * them to integer value while adding other factors to the final result.
     *
     * @return The hashCode method returns a hashed value of the current Rating
     * object and returning it as an Int value.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.productId;
        hash = 53 * hash + this.userId;
        hash = 53 * hash + Float.floatToIntBits(this.rating);
        hash = 53 * hash + Objects.hashCode(this.comment);
        return hash;
    }

    /**
     * The equals method returns true or false based on whether the current
     * Rating object is the same as the Rating object being passed to the method
     *
     * @return True or False based on whether the two objects have the same
     * values
     * @param obj this represents another Rating object
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rating other = (Rating) obj;
        if (this.productId != other.productId) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        if (this.rating != other.rating) {
            return false;
        }
        if (!Objects.equals(this.comment, other.comment)) {
            return false;
        }
        return true;
    }

    /**
     * The toString method is used to supply a string with all relevant
     * information to the object
     *
     * @return A string variable with all relevant information
     */
    @Override
    public String toString() {
        return "Rating{" + "productId=" + productId + ", userId=" + userId + ", rating=" + rating + ", comment=" + comment + '}';
    }

}
