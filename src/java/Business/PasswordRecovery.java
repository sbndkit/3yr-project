/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * The passwordRecovery class is a feature of our project that allows a user to
 * set a new password if they forget theirs, we do this by sending the user a
 * token with which we are able to identify the user wishing to change their
 * password.
 *
 * @author Sergio
 */
public class PasswordRecovery {

    private String email;
    private String id;
    private Timestamp date;

    /**
     * The parameterized PasswordRecovery constructor is used to create
     * PasswordRecovery objects
     *
     * @param email the Email that the token was sent to
     * @param id the id of the user wishing to change their password
     * @param date the date of the last failed login attempt
     */
    public PasswordRecovery(String email, String id, long date) {
        this.setEmail(email);
        this.setTimestamp(date);
        this.setId(id);
    }

    public PasswordRecovery() {
    }

    /**
     * Method to retrieve the email variable in a PasswordRecovery object
     *
     * @return Returns the email variable where email is the email of the user
     * wishing to change their password
     */
    public String getEmail() {
        return email;
    }

    /**
     * Allows a user to change the email value in the passwordRecovery object
     *
     * @param email This is the new email value value to be stored.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Method to retrieve the id stored in the PasswordRecovery object
     *
     * @return Returns the id variable where id = to the users id wishing to
     * change their password
     */
    public String getId() {
        return id;
    }

    /**
     * Allows a user ti change the id value in the password recovery object
     *
     * @param id This is the new id value to be stored
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Method is used to retrieve the date of the last failed login attempt
     *
     * @return Returns the date variable of the PasswordRecovery object
     */
    public Timestamp getTimestamp() {
        return date;
    }

    /**
     * Allows a user ti change the date value in the PasswordRecovery object
     *
     * @param date This is the new date value to be stored
     */
    public void setTimestamp(long date) {
        this.date = new Timestamp(date);
    }

    public void setTimestamp(Timestamp date) {
        this.date = new Timestamp(date.getTime());
    }

    /**
     * The hashCode method takes the three variables in the PasswordRecovery
     * class and converts them to integer value while adding other factors to
     * the final result.
     *
     * @return The hashCode method returns a hashed value of the current
     * PasswordRecovery object object and returning it as an Int value.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.email);
        hash = 71 * hash + Objects.hashCode(this.id);
        hash = 71 * hash + Objects.hashCode(this.date);
        return hash;
    }

    /**
     * The equals method returns true or false based on whether the current
     * PasswordRecovery object is the same as the PasswordRecovery object being
     * passed to the method
     *
     * @return True or False based on whether the two objects have the same
     * values
     * @param obj this represents another PasswordRecovery object
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PasswordRecovery other = (PasswordRecovery) obj;
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    /**
     * The toString method is used to supply a string with all relevant
     * information to the object
     *
     * @return A string variable with all relevant information
     */
    @Override
    public String toString() {
        return "PasswordRecovery{" + "email=" + email + ", id=" + id + ", date=" + date + '}';
    }

}
