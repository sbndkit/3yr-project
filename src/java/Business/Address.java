/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Objects;

/**
 *
 * @author Sergio
 */
public class Address {

    private int addressID;
    private int userID;
    private String address;
    private String county;
    private String country;

    /**
     * Address class store the information related to a one user address.
     *
     * Class parameters:
     *
     * @param nAddressID int fields that stores the user address
     * @param nUserID int fields that stores the user ID
     * @param nAddress String fields that stores the user address
     * @param nCounty String fields that stores the user county
     * @param nCountry String fields that stores the user country
     */
    public Address() {
    }

    /**
     * Parametrised constructor. This constructor accept five parameters 
     * 
     * @param nAddressID
     * @param nUserID
     * @param nAddress
     * @param nCounty
     * @param nCountry 
     */
    public Address(int nAddressID, int nUserID, String nAddress, String nCounty, String nCountry) {
        this.setAddress(nAddress);
        this.setAddressID(nAddressID);
        this.setCountry(nCountry);
        this.setCounty(nCounty);
        this.setUserID(nUserID);
    }

    /**
     * This method returns the current addressID field value
     * 
     * @return int value
     */
    public int getAddressID() {
        return addressID;
    }

    /**
     * This method sets the addressID field value
     * 
     * @param addressID 
     */
    public void setAddressID(int addressID) {
        this.addressID = addressID;
    }

    /**
     * This method returns the current userID field value
     * 
     * @return int value
     */
    public int getUserID() {
        return userID;
    }
    
    /**
     * This method sets the userID field value
     * 
     * @param userID 
     */
    public void setUserID(int userID) {
        this.userID = userID;
    }

    /**
     * This method returns the current address field value
     * @return String value
     */
    public String getAddress() {
        return address;
    }

    /**
     * This method sets the address field value
     * 
     * @param address 
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * This method returns the current county field value
     * @return String value
     */
    public String getCounty() {
        return county;
    }

    /**
     * This method sets the county field value
     * 
     * @param county 
     */
    public void setCounty(String county) {
        this.county = county;
    }

    /**
     * This method returns the current country field value
     * @return String value
     */
    public String getCountry() {
        return country;
    }

    /**
     * This method sets the country field value
     * 
     * @param country 
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * This method override the inherited method from the Object class to 
     * provide a new method for Hashing
     * 
     * @return int value
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.addressID;
        hash = 97 * hash + this.userID;
        hash = 97 * hash + Objects.hashCode(this.address);
        hash = 97 * hash + Objects.hashCode(this.county);
        hash = 97 * hash + Objects.hashCode(this.country);
        return hash;
    }

    /**
     * This method override the inherited method from the Object class to 
     * provide a new method to check if two Address objects are equals
     * 
     * @return boolean value
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Address other = (Address) obj;
        if (this.userID != other.userID) {
            return false;
        }
        if (this.addressID != other.addressID) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.county, other.county)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        return true;
    }

    /**
     * This method override the inherited method from the Object class to 
     * provide a new method for displaying the information stored in the class fields
     * 
     * @return String value
     */
    @Override
    public String toString() {
        return "Address{" + "addressID=" + addressID + ", userID=" + userID + ", address=" + address + ", county=" + county + ", country=" + country + '}';
    }
    
     /**
     * This method display the information stored in the object using a specific format that is user friendly.
     * 
     * @return String value
     */
    public String display(){
        return this.address + ", " + this.county + ", " + this.country;
    }

}
