/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Comparators.ProductBrandComparator;
import java.util.Comparator;
import java.util.Objects;
import java.util.TreeSet;

/**
 * The HashMap class is the primary method of storing in the PhoneStore
 * application. The choice to make a custom HashMap class was made based on the
 * need for the least amount of data collision
 *
 * @author Sergio
 * @param KeyType keytype is the means by which the HashMap will be devided it
 * is also a generic variable
 * @param DataType Datatype is the information stored in each of the HashMaps it
 * is also a generic variable buckets
 */
public class HashMap<KeyType, DataType> {

    private KeyType[] arrayOfKeys;
    private TreeSet<DataType>[] arrayOfTreeSets;
    private int capacity = 26;
    private int count = 0;

    /**
     * The HashMap() is a basic constructor that creates a HashMap of TreeSets
     */
    public HashMap() {
        this.arrayOfKeys = (KeyType[]) new Object[this.capacity];
        this.arrayOfTreeSets = new TreeSet[this.capacity];
        for (int i = 0; i < this.capacity; i++) {
            this.arrayOfTreeSets[i] = new TreeSet<DataType>();
        }
    }

    /**
     * The HashMap(int i) is a parameterized constructor method that creates a
     * HashMap object with it's size based on the parameter passed to it
     *
     * @param i Specifies the size of the HashMap to be created
     */
    public HashMap(int i) {
        this.capacity = i;

        this.arrayOfKeys = (KeyType[]) new Object[this.capacity];
        this.arrayOfTreeSets = new TreeSet[this.capacity];
        for (int x = 0; x < this.capacity; x++) {
            this.arrayOfTreeSets[x] = new TreeSet(new ProductBrandComparator());
        }
    }

    /**
     * The HashMap(int i,Comparator comparator) is a parameterized constructor
     * method that creates a HashMap object with it's size based on the
     * parameter passed to it and we use a comparator in order to sort the
     * TreeSets stored in the HashMap object
     *
     * @param i decides the size of the HashMap
     * @param comparator The parameter comparator sorts said TreeSets according
     * to the order imposed by the comparator
     */
    public HashMap(int i, Comparator comparator) {
        this.capacity = i;

        this.arrayOfKeys = (KeyType[]) new Object[this.capacity];
        this.arrayOfTreeSets = new TreeSet[this.capacity];
        for (int x = 0; x < this.capacity; x++) {
            this.arrayOfTreeSets[x] = new TreeSet(comparator);
        }
    }

    /**
     * Method to hash the key value for inserting into the HashMap
     *
     * @param key is the key value of the bucket
     * @return returns the hashed key-value which in turn will point to a bucket
     * with the HashMap
     */
    public int hash(KeyType key) {
        return Objects.hash(key) % this.capacity;
    }

    /**
     * Method for getting the Keys used in the HashMap object
     *
     * @return This method returns the array of keys of the HashMap object
     */
    public KeyType[] getArrayOfKeys() {
        return arrayOfKeys;
    }

    /**
     * Method for getting the TreeSets stored in the HashMap object
     *
     * @return This method returns the Array of TreeSets
     */
    public TreeSet<DataType>[] getArrayOfTreeSets() {
        return arrayOfTreeSets;
    }

    /**
     * Method that gets the capacity of the HashMap where capacity is the
     * maximum amount of entries the HashMap can hold
     *
     * @return The size of the HashMap and it's TreeSets
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * This method is used to amount of spaces in the HashMap that are actually
     * used
     *
     * @return count variable where count is equal to the amount of occupied
     * spaces in the HashMap object
     */
    public int getCount() {
        return count;
    }

    /**
     * The put method is used to insert objects into the HashMap
     *
     * @param key This is the key-value of the object and will decide as to
     * which bucket the object is placed
     * @param data This is the actual object to be stored in the bucket
     */
    public void put(KeyType key, DataType data) {
        if (key != null && data != null) {

            int hash = hash(key);
            hash = Math.abs(hash);

            boolean exit = false;
            int increase = 1;

            while (!exit) {

                if (this.arrayOfKeys[hash] == null) {
                    this.arrayOfKeys[hash] = key;

                    this.arrayOfTreeSets[hash].add(data);
                    exit = true;
                } else if (this.arrayOfKeys[hash].equals(key)) {
                    this.arrayOfTreeSets[hash].add(data);
                    exit = true;
                } else {
                    Product p = (Product) data;
                    hash = Objects.hashCode((hash + increase) + "-" + increase);
                    hash = Math.abs(hash) % this.capacity;
                    exit = false;
                    increase++;
                }
            }
            this.count++;

        }
    }

    /**
     * The clear method wipes the HashMap by creating a new arrayofkeys and
     * arrayofTreeSets both being empty and equal to null
     */
    public void clear() {

        for (int i = 0; i < this.capacity; i++) {
            this.arrayOfKeys[i] = null;
            this.arrayOfTreeSets[i] = new TreeSet();
        }
    }

    /**
     * The remove method deletes a single product object from its bucket
     *
     * @param item is the product object that the user wishes to delete from the
     * HashMap object
     */
    public void remove(Product item) {

        if (item != null) {

            String tempKey = item.getBrand();

            int hash = tempKey.hashCode();
            hash = Math.abs(hash);

            boolean exit = false;
            int increase = 1;

            while (!exit) {

                if (this.arrayOfKeys[hash] != null && this.arrayOfTreeSets[hash] != null) {

                    if (this.arrayOfKeys[hash].equals(arrayOfKeys)) {
                        this.arrayOfTreeSets[hash].remove(item);

                        if (this.arrayOfTreeSets[hash].isEmpty()) {
                            this.arrayOfKeys[hash] = null;
                        }
                    } else {
                        hash = Objects.hashCode((hash + increase) + "-" + increase);
                        hash = Math.abs(hash);
                        exit = false;
                        increase++;
                    }

                }
            }
            this.count--;

        }

    }

}
