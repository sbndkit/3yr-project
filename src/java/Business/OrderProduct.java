/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 * The orderProduct class behaves as the cart in the application
 *
 * @author BrianMcM
 */
public class OrderProduct {

    int orderId;
    int productId;
    int Qty;

    /**
     * The basic constructor for orderProduct
     */
    public OrderProduct() {
        orderId = -1;
        productId = -1;
        Qty = 0;
    }

    /**
     * The parameterized OrderProduct constructor is used to
     *
     * @param orderId The unique id of the order the product is to be associated with
     * @param productId the unique id of the product being associated with the order
     * @param Qty the quantity of the phone being ordered
     */
    public OrderProduct(int orderId, int productId, int Qty) {
        this.orderId = orderId;
        this.productId = productId;
        this.Qty = Qty;
    }

    /**
     * Method to get the Order id variable of the orderProduct object
     *
     * @return the order id stored with the OrderProduct object
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * Method to allow a user to alter the Order id stored in the orderproduct
     * object
     *
     * @param orderId the new Order id to be stored in the object
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * Method to get the product id variable of the orderProduct object
     *
     * @return the product id stored with the orderproduct object
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Method to allow a user to alter the product id stored in the orderproduct
     * object
     *
     * @param productId the new product id to be stored in the object
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * Method to get the Qty variable of the orderProduct object
     *
     * @return the quantity stored with the orderproduct object
     */
    public int getQty() {
        return Qty;
    }

    /**
     * Method to allow a user to alter the quantity stored in the orderproduct
     * object
     *
     * @param Qty the new quantity to be stored in the object
     */
    public void setQty(int Qty) {
        this.Qty = Qty;
    }

    /**
     * @return The hashCode method returns a hashed value of the current
     * Orderproduct object and returning it as an Int value.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.orderId;
        hash = 59 * hash + this.productId;
        hash = 59 * hash + this.Qty;
        return hash;
    }

    /**
     * @return The equals method returns true or false based on whether the
     * current Order object is the same as the OrderProduct object being passed
     * to the method
     * @param obj this represents another Order object
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrderProduct other = (OrderProduct) obj;
        if (this.orderId != other.orderId) {
            return false;
        }
        if (this.productId != other.productId) {
            return false;
        }
        if (this.Qty != other.Qty) {
            return false;
        }
        return true;
    }

    /**
     * @return The toString method returns a string composed of Titles and
     * variables of the OrderProduct Class
     */
    @Override
    public String toString() {
        return "Order_Product{" + "orderId=" + orderId + ", productId=" + productId + ", Qty=" + Qty + '}';
    }

}
