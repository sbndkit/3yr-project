/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;
import java.util.Objects;

/**
 * The Order Class is used by our application to facilitate a users ability to
 * purchase a product
 *
 * @author BrianMcM
 */
public class Order {

    private int orderId = -1;
    private int userId = -1;
    private float totalPrice = -1;
    private Date dateOrdered = null;
    private int addressID = -1;

    public Order() {
    }

    /**
     * The parameterized Order constructor is the standard method by which we
     * create an Order object
     *
     * @param orderId The unique id number of the order
     * @param userId The unique id of the user placing the order
     * @param dateOrdered The date the order was placed
     * @param nTotalPrice The total price of the Order being placed
     * @param nAddressID The ID of the address the user has selected as their
     * delivery address
     */
    public Order(int orderId, int userId, Date dateOrdered, float nTotalPrice, int nAddressID) {
        this.setOrderId(orderId);
        this.setDateOrdered(dateOrdered);
        this.setUserId(userId);
        this.setTotalPrice(totalPrice);
        this.setAddressID(nAddressID);
    }

    /**
     * Method to retrieve the ID of the user that placed the order
     *
     * @return Returns the Id of the user who placed the order
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Method that allows you to change the ID and therefore the user who placed
     * the order
     *
     * @param userId The new id you wish to place into the userId variable
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Method to retrieve the ID of the order object
     *
     * @return Returns the ID of the order id
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * Method that allows you to change the ID of the order
     *
     * @param orderId The new ID you wish to assign to the order
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * Method to retrieve the dateOrdered variable of the Order object
     *
     * @return Returns the date the order was placed
     */
    public Date getDateOrdered() {
        return dateOrdered;
    }

    /**
     * Method that allows the user to change the date the order was placed
     *
     * @param dateOrdered The new Date the order will be placed on.
     */
    public void setDateOrdered(Date dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

    /**
     * Method to return the totalPrice variable of a order object
     *
     * @return Returns the total price of the order being placed
     */
    public float getTotalPrice() {
        return totalPrice;
    }

    /**
     * Method that allows the user to alter the totalPrice variable
     *
     * @param totalPrice The new value we wish to change the totalPrice to.
     */
    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * Method to retrieve the addressId stored in the order object
     *
     * @return Returns the addressId variable
     */
    public int getAddressID() {
        return addressID;
    }

    /**
     * Method that allows a user alter the addressId
     *
     * @param addressID this is the new addressId to be associated with the
     * order
     */
    public void setAddressID(int addressID) {
        this.addressID = addressID;
    }

    /**
     * @return The hashCode method returns a hashed value of the current Order
     * object and returning it as an Int value.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.orderId;
        hash = 43 * hash + this.userId;
        hash = 43 * hash + Float.floatToIntBits(this.totalPrice);
        hash = 43 * hash + Objects.hashCode(this.dateOrdered);
        hash = 43 * hash + this.addressID;
        return hash;
    }

    /**
     * @return The equals method returns true or false based on whether the
     * current Order object is the same as the Order object being passed to the
     * method
     * @param obj this represents another Order object
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Order other = (Order) obj;
        if (this.orderId != other.orderId) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        if (Float.floatToIntBits(this.totalPrice) != Float.floatToIntBits(other.totalPrice)) {
            return false;
        }
        if (this.addressID != other.addressID) {
            return false;
        }
        if (!Objects.equals(this.dateOrdered, other.dateOrdered)) {
            return false;
        }
        return true;
    }

    /**
     * @return The toString method returns a string composed of Titles and
     * variables of the Order Class
     */
    @Override
    public String toString() {
        return "Order{" + "orderId=" + orderId + ", userId=" + userId + ", totalPrice=" + totalPrice + ", dateOrdered=" + dateOrdered + ", addressID=" + addressID + '}';
    }
}
