/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Objects;

/**
 * The supplier object represents a Supplier to the business in the application
 * and as such the object stores all the relevant information for the suppler
 *
 * @author BrianMcM
 */
public class Supplier {

    int supplierId;
    String supplierName;
    String email;
    String address;
    String county;
    String country;
    String phone;

    public Supplier() {
    }

    /**
     * The Supplier parameterized constructor is used for creating new Supplier
     * objects in our application. It takes in several parameters that have been
     * deemed necessary by us for a Supplier object
     *
     * @param supplerId This is stored as an int and is the unique id of the
     * Supplier eg.(1)
     * @param supplierName This is stored as a String and is the name of the
     * Supplier(Name/Company) eg.(PhoneSuppliers.Ltd)
     * @param email This is stored as a String and is the email address of the
     * supplier eg.(PhoneSupplier@Hotmail.com)
     * @param address This is stored as a String and is the address of the
     * supplier eg.(Unit 3, Drogheda Retail Park, Drogheda)
     * @param county This is stored as a String and is the county in which the
     * Supplier is located eg.(Louth)
     * @param country This is stored as a String and is the country in which the
     * Supplier is located eg.(Ireland)
     * @param phone This is stored as an int and is the contact number for the
     * in Supplier eg.(041-98896325)
     */
    public Supplier(int supplerId, String supplierName, String email, String address, String county, String country, String phone) {
        this.supplierId = supplerId;
        this.supplierName = supplierName;
        this.email = email;
        this.address = address;
        this.county = county;
        this.country = country;
        this.phone = phone;
    }

    /**
     * Method to get the unique supplierId variable which is stored as an int
     *
     * @return the supplier id stored in the supplier object
     */
    public int getSupplierId() {
        return supplierId;
    }

    /**
     * Allows a user to change the supplier's id stored in the Supplier object
     *
     * @param supplerId the new value(Int) to stored as the supplierId
     */
    public void setSupplierId(int supplerId) {
        this.supplierId = supplerId;
    }

    /**
     * Method to get the supplierName variable which is stored as an String
     *
     * @return the supplier's name stored in the supplier object
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * Allows a user to change the supplier's name stored in the Supplier object
     *
     * @param supplierName the new value(String) to stored as the supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * Method to get the email variable which is stored as an String
     *
     * @return the supplier's email stored in the supplier object
     */
    public String getEmail() {
        return email;
    }

    /**
     * Allows a user to change the supplier's email stored in the Supplier
     * object
     *
     * @param email the new value(String) to stored as the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Method to get the address variable which is stored as an String
     *
     * @return the supplier's address stored in the supplier object
     */
    public String getAddress() {
        return address;
    }

    /**
     * Allows a user to change the supplier's address stored in the Supplier
     * object
     *
     * @param address the new value(String) to stored as the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Method to get the phone variable which is stored as an String
     *
     * @return the supplier's phone number stored in the supplier object
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Allows a user to change the supplier's phone number stored in the
     * Supplier object
     *
     * @param phone the new value(String) to stored as the phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    
    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    /**
     * The hashCode method takes the variables in the Supplier class and
     * converts them to integer value while adding other factors to the final
     * result.
     *
     * @return The hashCode method returns a hashed value of the current
     * Supplier object and returning it as an Int value.
     */
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + this.supplierId;
        hash = 19 * hash + Objects.hashCode(this.supplierName);
        hash = 19 * hash + Objects.hashCode(this.email);
        hash = 19 * hash + Objects.hashCode(this.address);
        hash = 19 * hash + Objects.hashCode(this.phone);
        return hash;
    }
/**
     * The equals method returns true or false based on whether the current
     * Supplier object is the same as the Suppier object being passed to the method
     *
     * @return True or False based on whether the two objects have the same
     * values
     * @param obj this represents another Supplier object
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Supplier other = (Supplier) obj;
        if (this.supplierId != other.supplierId) {
            return false;
        }
        if (!Objects.equals(this.supplierName, other.supplierName)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.phone, other.phone)) {
            return false;
        }
        return true;
    }
    /**
     * The toString method is used to supply a string with all relevant
     * information to the object
     *
     * @return A string variable with all relevant information
     */
    @Override
    public String toString() {
        return "Supplier{" + "supplerId=" + supplierId + ", supplierName=" + supplierName + ", email=" + email + ", address=" + address + ", phone=" + phone + '}';
    }

}
