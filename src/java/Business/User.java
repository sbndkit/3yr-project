/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is the class used for all users(Admin/User) in the application
 *
 * @author BrianMcM
 */
public class User {

    private int userId;
    private String fName;
    private String lName;
    private String email;
    private String phone;
    private String password;
    private String accountId;
    private boolean isAdmin;
    private int walletCredits;

    /**
     * Base constructor which creates a blank user object with just an accountId
     */
    public User() {
        this.accountId = "";
    }

    /**
     * The User parameterized constructor is used for creating a user object in
     * our application. It takes in several parameters that have been deemed
     * necessary by us for a User object
     *
     * @param password This is received as a char array but is stored as a
     * string and is the password the user entered.
     * @param fName This is stored as a String and is the User's first name
     * eg.(Niall)
     * @param lName This is stored as a String and is the User's last name
     * eg.(Mulready)
     * @param email This is stored as a String and is the User's email
     * eg.(test2@test.com)
     * @param phone This is stored as a String and is the User's contact number
     * eg.(0833123585)
     * @param walletCredits This is stored as an int and is the User's credits
     * eg.(35)
     */
    public User(char[] password, String fName, String lName, String email, String phone, int walletCredits) {

        this.setPassword(password);
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.phone = phone;
        this.walletCredits = walletCredits;
        this.accountId = "";
    }

    /**
     * Retrieves the User's password
     *
     * @return password being the password used by the user to sign into our
     * application
     */
    public String getPassword() {
        return password;
    }

    /**
     * Allows a user to change the User's password stored in the User object
     *
     * @param password the new value(String) to be stored in the password
     * variable
     */
    public void setPasswordHashed(String password) {
        this.password = password;
    }

    /**
     * Allows a user to change the User's password stored in the User object
     * this method is called in the constructor as it calls on the getHash
     * method to hash the char array received as the password and then converts
     * it to a string
     *
     * @param password the new value(char array) to be stored in the password
     * variable
     */
    public void setPassword(char[] password) {
        this.password = getHash(password);
    }

    /**
     * Method to retrieve the userId variable which is stored as an Int
     *
     * @return userId being the id of the user in the current User object
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Allows a user to change the user's id stored in the User object
     *
     * @param userId the new value(Int) to stored as the userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Method to retrieve the fName variable which is stored as a String
     *
     * @return fName being the first name of the user in the current User object
     */
    public String getfName() {
        return fName;
    }

    /**
     * Allows a user to change the user's first name stored in the User object
     *
     * @param fName the new value(String) to stored as the fName
     */
    public void setfName(String fName) {
        this.fName = fName;
    }

    /**
     * Method to retrieve the lName variable which is stored as a String
     *
     * @return lName being the last name of the user in the current User object
     */
    public String getlName() {
        return lName;
    }

    /**
     * Allows a user to change the user's last name stored in the User object
     *
     * @param lName the new value(String) to stored as the lName
     */
    public void setlName(String lName) {
        this.lName = lName;
    }

    /**
     * Method to retrieve the email variable which is stored as a String
     *
     * @return email being the email address of the user in the current User
     * object
     */
    public String getEmail() {
        return email;
    }

    /**
     * Allows a user to change the user's email address stored in the User
     * object
     *
     * @param email the new value(String) to stored as the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Method to retrieve the phone variable which is stored as a String
     *
     * @return phone being the phone number of the user in the current User
     * object
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Allows a user to change the user's phone number stored in the User object
     *
     * @param phone the new value(String) to stored as the phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Method to retrieve the isAdmin variable which is stored as a boolean
     *
     * @return isAdmin being the true or false result deciding if the current
     * user is an Admin or not
     */
    public boolean getIsIsAdmin() {
        return isAdmin;
    }

    /**
     * Allows a user to change the isAdmin value stored in the User object
     *
     * @param isAdmin the new value(boolean) to be stored as the isAdmin value
     */
    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * Method to retrieve the walletCredits variable which is stored as a int
     *
     * @return walletCredits being the integer value(whole number) that they
     * have associated
     */
    public int getWalletCredits() {
        return walletCredits;
    }

    /**
     * Allows a user to change the amount stored in walletCredits
     *
     * @param walletCredits the new value(integer) to be stored as the
     * walletCredits total amount value
     */
    public void setWalletCredits(int walletCredits) {
        this.walletCredits = walletCredits;
    }

    /**
     * Method to retrieve the accountId variable which is stored as a String
     *
     * @return accountId being the String value that is stored in the User
     * object
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Allows a user to change the amount stored in walletCredits
     *
     * @param accountId the new value(String) to be stored as the accountId
     * variable
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * The toHex performs similarly to a salt method in that it adds extra
     * security to our ability to safely store a users password by adding an
     * extra layer of encryption to the passwords.
     *
     * @param digest This is the hashed password being passed to this method
     * from the toHash method after it has been converted to a byte array.
     * @return Returns a fully encrypted password which is represented as a
     * String with which we then store on the database in that users entry.
     */
    private String toHex(byte[] digest) {
        String hash = "";
        for (byte aux : digest) {
            int b = aux & 0xff;
            if (Integer.toHexString(b).length() == 1) {
                hash += "0";
            }
            hash += Integer.toHexString(b);
        }
        return hash;
    }

    /**
     * charToByteArray is our method for preparing a hashed password which is a
     * char array to be sent to toHex which only accepts byte arrays
     *
     * @param pass This is the already hashed password the user has entered
     * @return Returns the users hashed password but as a Byte array instead of
     * a Char array and thus preparing it to be sent to the toHex method
     */
    public byte[] charToByteArray(char[] pass) {

        byte[] b = new byte[pass.length];

        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) pass[i];
        }

        return b;
    }

    /**
     * The getHash method is our applications primary method of encrypting a
     * user's password. This is done by using the Secure Hash Algorithm or
     * SHA-256, 256 being the size 256-bit
     *
     * @param pass The User's password we wish to encrypt is passed to the
     * method as a Char array.
     * @return Once the password has been taken in it is converted from a char
     * array to a byte array it is then encrypted by our main hashing algorithm
     * when finally it is passed to the toHex method and then returned as a
     * String
     */
    public String getHash(char[] pass) {

        byte[] digest = null;
        byte[] buffer = charToByteArray(pass);

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.reset();
            messageDigest.update(buffer);
            digest = messageDigest.digest();

            return toHex(digest);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }

        return toHex(digest);
    }

    /**
     * The hashCode method takes the variables in the User class and converts
     * them to integer value while adding other factors to the final result.
     *
     * @return The hashCode method returns a hashed value of the current User
     * object and returning it as an Int value.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.userId;
        hash = 97 * hash + Objects.hashCode(this.fName);
        hash = 97 * hash + Objects.hashCode(this.lName);
        hash = 97 * hash + Objects.hashCode(this.email);
        hash = 97 * hash + Objects.hashCode(this.phone);
        hash = 97 * hash + Objects.hashCode(this.password);
        hash = 97 * hash + Objects.hashCode(this.accountId);
        hash = 97 * hash + (this.isAdmin ? 1 : 0);
        hash = 97 * hash + this.walletCredits;
        return hash;
    }

    /**
     * The equals method returns true or false based on whether the current User
     * object is the same as the User object being passed to the method
     *
     * @return True or False based on whether the two objects have the same
     * values
     * @param obj this represents another User object
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.isAdmin != other.isAdmin) {
            return false;
        }
        if (this.walletCredits != other.walletCredits) {
            return false;
        }
        if (!Objects.equals(this.fName, other.fName)) {
            return false;
        }
        if (!Objects.equals(this.lName, other.lName)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.phone, other.phone)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.accountId, other.accountId)) {
            return false;
        }
        return true;
    }

    /**
     * The toString method is used to supply a string with all relevant
     * information to the object
     *
     * @return A string variable with all relevant information
     */
    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", fName=" + fName + ", lName=" + lName + ", email=" + email + ", phone=" + phone + ", password=" + password + ", accountId=" + accountId + ", isAdmin=" + isAdmin + ", walletCredits=" + walletCredits + '}';
    }

}
