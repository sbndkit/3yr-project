/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.sql.Timestamp;
import java.util.Objects;

/**
 *
 * @author BrianMcM
 */
public class Account {

    private String email = null;
    private int attempts = 0;
    private Timestamp date = null;
    private String accountStatus = null;

    public Account() {
    }

    /**
     * The Account(String email, int attempts, Timestamp date, String
     * accountStatus) is a parameterized constructor method creates a standard
     * account object based on the parameters passe to it.
     *
     * @param email Email/Username of the user the account is being created for
     * @param attempts Number of attempts that have been made to login
     * @param date The date and time the account was locked
     * @param accountStatus Shows whether the account is locked or unlocked
     */
    public Account(String email, int attempts, Timestamp date, String accountStatus) {
        this.email = email;
        this.attempts = attempts;
        this.date = date;
        this.accountStatus = accountStatus;
    }

    /**
     * @return The getEmail method returns the email variable of the Account
     * object as a string.
     */
    public String getEmail() {
        return email;
    }

    /**
     * The setEmail method allows the altering of the email variable in an
     * Account object
     *
     * @param email The new Email/Username of the account
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * The getAttempts method returns the attempts variable's current value as
     * an integer.
     *
     * @return attempts variable of Account object
     */
    public int getAttempts() {
        return attempts;
    }

    /**
     * The setAttempts method allows the altering of the attempts variable in an
     * Account object NOTE: attempts is a int variable.
     *
     * @param attempts Number of attempts that have been made to login
     */
    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    /**
     * The getTimestamp method returns the date variable of the Account object.
     *
     * @return Returns the date and time the account was locked
     */
    public Timestamp getTimestamp() {
        return date;
    }

    /**
     * The setTimestamp method allows the altering of the date variable in an
     * Account object NOTE: date is a Timestamp variable.
     *
     * @param date The date and time the account was locked
     */
    public void setTimestamp(Timestamp date) {
        this.date = date;
    }

    /**
     * The getAccountStatus method returns the AccountStatus variable of the
     * Account object.
     *
     * @return Returns the the variable accountStatus which says whether or not
     * the account is locked
     */
    public String getAccountStatus() {
        return accountStatus;
    }

    /**
     * The setAccountStatus method allows the altering of the AccountStatus
     * variable in an Account object NOTE: date is a Timestamp variable.
     *
     * @param accountStatus Shows whether the account is locked or unlocked
     */
    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    /**
     * The hashCode method returns a hashed value of the current Account object
     * and returning it as an Int value.
     *
     * @return Returns the hash value of the Account object object
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.email);
        hash = 41 * hash + this.attempts;
        hash = 41 * hash + Objects.hashCode(this.date);
        hash = 41 * hash + Objects.hashCode(this.accountStatus);
        return hash;
    }

    /**
     * @return The equals method returns true or false based on whether the
     * current Account object is the same as the Account object being passed to
     * the method
     * @param obj this represents another Account object
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Account other = (Account) obj;
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (this.attempts != other.attempts) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.accountStatus, other.accountStatus)) {
            return false;
        }
        return true;
    }

    /**
     * @return The toString method returns a string composed of Titles and
     * variables of the Account Class
     */
    @Override
    public String toString() {
        return "Account{" + "email=" + email + ", attempts=" + attempts + ", date=" + date + ", accountStatus=" + accountStatus + '}';
    }

}
