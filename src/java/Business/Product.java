/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Objects;

/**
 * The product object represents singular product of our application and as such
 * the object stores all the relevant information for the product
 *
 * @author Megatronus
 */
public class Product {

    private int productID = -1;
    private String productName = "not set";
    private Double price = -1.0;
    private String desc = "not set";
    private int stock = 0;
    private String pictureLink = "not set";
    private String brand = "not set";

    public Product() {
    }

    /**
     * The Product parameterized constructor is used for creating new products
     * in our application. It takes in several parameters that have been deemed
     * necessary by us for a product object
     *
     * @param productID This is stored as an Int and is the unique id of the new
     * product eg.(1)
     * @param productName This is stored as a String and is the chosen name of
     * the product eg.(Galaxy S6)
     * @param brand This is stored as a String and is the chosen brand of the
     * product eg.(Samsung)
     * @param price This is stored as a double and is the chosen price of the
     * product eg.(235.55)
     * @param desc This is stored as a String and is the chosen description of
     * the product eg.(full description of the phone and it's specifications)
     * @param stock This is stored as a Int and is the chosen stock of the
     * product eg.(3)
     * @param pictureLink This is stored as a String and is the chosen picture
     * of the product eg(samsungGS6.jpg)
     */
    public Product(int productID, String productName, String brand, Double price, String desc, int stock, String pictureLink) {
        this.setProductID(productID);
        this.setProductName(productName);
        this.setBrand(brand);
        this.setPrice(price);
        this.setDesc(desc);
        this.setStock(stock);
        this.setPictureLink(pictureLink);
    }

    /**
     * Method to retrieve the productId variable which is stored as an Int
     *
     * @return productId being the id of the current product object
     */
    public int getProductID() {
        return productID;
    }

    /**
     * Allows a user to change the product id stored in the product object
     *
     * @param productID the new value(Int) to stored as the productId
     */
    public void setProductID(int productID) {
        this.productID = productID;
    }

    /**
     * Method to retrieve the productName variable which is stored as an String
     *
     * @return productName being the name of the current product object
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Allows a user to change the product name stored in the product object
     *
     * @param productName the new value(String) to stored as the productName
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Method to retrieve the price variable which is stored as an double
     *
     * @return price being the current price-value of the product object
     */
    public Double getPrice() {
        return price;
    }

    /**
     * Allows a user to change the product price stored in the product object
     *
     * @param price the new value(double) to stored as the price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * Method to retrieve the desc variable which is stored as an String
     *
     * @return desc being the current description of the product object
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Allows a user to change the product description stored in the product
     * object
     *
     * @param desc the new value(String) to be stored as the desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * Method to retrieve the stock variable which is stored as an int
     *
     * @return stock being the current stock-value of the product object
     */
    public int getStock() {
        return stock;
    }

    /**
     * Allows a user to change the product's stock-value stored in the product
     * object
     *
     * @param stock the new value(int) to be stored as the stock
     */
    public void setStock(int stock) {
        this.stock = stock;
    }

    /**
     * Method to retrieve the pictureLink variable which is stored as a String
     *
     * @return pictureLink being the current name of the picture of the product.
     */
    public String getPictureLink() {
        return pictureLink;
    }

    /**
     * Allows a user to change the product's associated picture link name stored
     * in the product object
     *
     * @param pictureLink the new value(String) to be stored as the pictureLink
     */
    public void setPictureLink(String pictureLink) {
        this.pictureLink = pictureLink;
    }

    /**
     * Method to retrieve the brand variable which is stored as a String
     *
     * @return brand being the current brand the product.
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Allows a user to change the product's associated picture link name stored
     * in the product object
     *
     * @param brand the new value(String) to be stored as the brand of the
     * product object
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * The hashCode method takes the variables in the Product class and converts
     * them to integer value while adding other factors to the final result.
     *
     * @return The hashCode method returns a hashed value of the current Product
     * object and returning it as an Int value.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.productID;
        hash = 59 * hash + Objects.hashCode(this.productName);
        hash = 59 * hash + Objects.hashCode(this.price);
        hash = 59 * hash + Objects.hashCode(this.desc);
        hash = 59 * hash + this.stock;
        hash = 59 * hash + Objects.hashCode(this.pictureLink);
        hash = 59 * hash + Objects.hashCode(this.brand);
        return hash;
    }

    /**
     * The equals method returns true or false based on whether the current
     * Product object is the same as the Product object being passed to the
     * method
     *
     * @return True or False based on whether the two objects have the same
     * values
     * @param obj this represents another Product object
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        if (this.productID != other.productID) {
            return false;
        }
        if (this.stock != other.stock) {
            return false;
        }
        if (!Objects.equals(this.productName, other.productName)) {
            return false;
        }
        if (!Objects.equals(this.desc, other.desc)) {
            return false;
        }
        if (!Objects.equals(this.pictureLink, other.pictureLink)) {
            return false;
        }
        if (!Objects.equals(this.brand, other.brand)) {
            return false;
        }
        if (!Objects.equals(this.price, other.price)) {
            return false;
        }
        return true;
    }

    /**
     * The toString method is used to supply a string with all relevant
     * information to the object
     *
     * @return A string variable with all relevant information
     */
    @Override
    public String toString() {
        return "Product{" + "productID=" + productID + ", productName=" + productName + ", price=" + price + ", desc=" + desc + ", stock=" + stock + ", pictureLink=" + pictureLink + ", brand=" + brand + '}';
    }

}
