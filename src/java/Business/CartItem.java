/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Objects;

/**
 * The CartItem class is used for managing the process through which a user is
 * able to purchase product objects in the application
 *
 * @author Sergio
 */
public class CartItem implements Comparable<CartItem> {

    private Product product = null;
    private int Qty = -1;

    public CartItem() {
    }

    /**
     * The CartItem(Product product, int nQty) is a parameterized constructor
     * method creates a standard CartItem object based on the parameters passed
     * to it.
     *
     * @param product The product(Phone) being added to the cart
     * @param nQty The quantity of that phone being added to the care NOTE:
     * cannot exceed Stock
     */

    public CartItem(Product product, int nQty) {
        this.setProduct(product);
        this.setQty(nQty);
    }

    /**
     * @return The getProduct method returns the Product variable of the
     * CartItem object as a Product object.
     */
    public Product getProduct() {
        return product;
    }

    /**
     * The setProduct method allows the manipulation of the product object in
     * the cartItem object.
     *
     * @param product This allows you to change the product in the cartItem
     * object
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return The getQty method returns the Qty variable of the CartItem object
     * as an int.
     */
    public int getQty() {
        return Qty;
    }

    /**
     * The setQty method allows the manipulation of the Qty variable in the
     * cartItem object.
     *
     * @param Qty The new quantity of the product object
     */
    public void setQty(int Qty) {
        this.Qty = Qty;
    }

    /**
     * @return The hashCode method returns a hashed value of the current Account
     * object and returning it as an Int value.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.product);
        hash = 43 * hash + this.Qty;
        return hash;
    }

    /**
     * @return The equals method returns true or false based on whether the
     * current Account object is the same as the Account object being passed to
     * the method
     * @param obj this represents another cartItem object
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CartItem other = (CartItem) obj;
        if (this.Qty != other.Qty) {
            return false;
        }
        if (!Objects.equals(this.product, other.product)) {
            return false;
        }
        return true;
    }

    /**
     * @return The toString method returns a string composed of Titles and
     * variables of the Account Class
     */
    @Override
    public String toString() {
        return "CartItem{" + "product=" + product + ", Qty=" + Qty + '}';
    }

    /**
     * @return The compareTo method returns whether or not the current products
     * productId is greater than the products productId of the product of the
     * cartItem object being passed to it.
     * @param o O refers to a CartItem object
     */

    @Override
    public int compareTo(CartItem o) {
        return this.product.getProductID() - o.getProduct().getProductID();
    }

}
