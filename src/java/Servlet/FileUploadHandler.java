/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Commands.Command;
import Factory.CommandMultipartFactory;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns={"/upload"}) 
public class FileUploadHandler extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            // TODO Auto-generated method stub
         processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        processRequest(request, response);
    }
    
    // Servlet to process the requests
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
    {
        String forwardToJsp = "";
        
        if (request != null)
        {
            // Create an appropriate Command and execute it
            CommandMultipartFactory factory = new CommandMultipartFactory();
            Command command = factory.createCommand(request, response);
            // execute generated Command
            forwardToJsp = command.execute(request, response);
        }
        
        // Forwarding approach
        
        //Get the request dispatcher object and forward the request to the appropriate JSP page...
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/" + forwardToJsp);
        try {
          
                dispatcher.forward(request, response);
            
            // B) Redirecting approach:
            // 
        } catch (ServletException ex) {
            Logger.getLogger(ProcessRequest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProcessRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
