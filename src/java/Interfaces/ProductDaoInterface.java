/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Business.HashMap;
import Business.Product;
import java.util.ArrayList;

/**
 *This class provides an abstract view of the methods to be implemented into the ProductDao class
 * 
 * @author Sergio
 */
public interface ProductDaoInterface {
    
    /**
     * This method returns all mobile products in the database into a 
     * hashMap with the key being the brand of the product
     * 
     * @return HashMap of type String for key and type Product for value
     */
    public HashMap<String,Product> getAllProducts();
    
    
    /**
     * This method takes a single parameter, an order id, and returns an arrayList of 
     * all the products contained on that order.
     * 
     * @param orderID is the id of the order in which we want to get the products for
     * @return ArrayList of type Product, A List of products that is on a particular order
     */
    public ArrayList<Product> getAllProductsByOrderId(int orderID);
    
    
    /**
     * This method takes in a single parameter, a product id, and uses it to 
     * find a product in the product table and return it.
     * 
     * @param ID is the id of the product which is to be found
     * @return Product is a Product object containing all the information 
     * for the product that is being searched for.
     */
    public Product getProduct(int ID);
    
    
    /**
     * This method takes in a single parameter, the name of a product, the method then searches for the 
     * product in the product table and returns it if found.
     * 
     * @param name is the name of a particular product
     * @return HashMap of type String for key and type Product for value
     */
    public HashMap<String, Product>getProductLikeName(String name);
    
    
    /**
     * This method takes a single parameter, a Product object, and creates a new entry in the product table 
     * and transfers the data from the object to the database entry.
     * 
     * @param nProduct is a Product object which contains all the information for 
     * a new product to be registered to the product table.
     * @return <code>true</code> if the product was successfully added to the product table
     *         <code>false</code> otherwise
     */
    public boolean registerProduct(Product nProduct);
    
    
    /**
     * This method takes a single parameter, a product id, and deletes the 
     * product related to this id from the Product table.
     * 
     * @param id is the id of the product to be deleted
     * @return <code>true</code> if the product was deleted successfully
     *         <code>false</code> otherwise
     */
    public boolean deleteProduct(int id);
    
    
    /**
     * This method takes a single parameter, a product name, and returns the product associated with that name
     * 
     * @param productName is the name of the product that is to be deleted
     * @return <code>true</code> if the product was deleted successfully
     *         <code>false</code> otherwise
     */
    public boolean deleteProduct(String productName);
    
    
    /**
     * This method takes a single parameter, a product object, and uses it to update 
     * the information of the entry in the Product table with the same id
     * 
     * @param nProduct is the product object that has the new information to update the entry with
     * @return <code>true</code> if the product was updated successfully
     *         <code>false</code> otherwise
     */
    public boolean updateProduct(Product nProduct);
}
