/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Business.PasswordRecovery;
import Business.User;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *This class provides an abstract view of the methods to be implemented into the UserDao class
 * 
 * @author Sergio
 */
public interface UserDaoInterface {
    
    /**
     * This method takes no parameters but returns an ArrayList of User objects with all users information.
     * 
     * @return ArrayList of type User, A ArrayList of user objects containing 
     * information on all users in the user table.
     */
    public ArrayList<User> FindAllUsers() throws SQLException;
    
    
    /**
     * This method takes a single parameter, an id of the user that is to be found
     * 
     * @param userId is the id of the user that is to be found
     * @return User, A user object containing all that users information
     */
     public User getUserById(int userId);
     
     
     // this method returns a list of users depending on the first name or the last name
    /**
     * This method takes in 2 parameters, a users first and last name, and returns 
     * a list of users that have the same name.
     * 
     * @param fName is the first name of the user that is to be found
     * @param lName is the last name of the user that is to be found
     * @return ArrayList of type User. is an ArrayList of users that 
     * have the same first and last name as those entered
     */
     public ArrayList<User> getUserByName(String fName, String lName);
       
      /**
     * This method takes 2 parameters, a users email and password, and uses them to 
     * find a user in the user table, if a user matches the user name and password
     * a user object is then created and that users information is inserted into 
     * that user object.
     * 
     * @param email is the users email which is used to access their account
     * @param userPassword is the users password which is used to access their account
     * @return User, a User object which contains all the information for that user
     */
     public User login(String email, char[] userPassword);
     
     
     /**
     * This method takes a single parameter, a User object containing the information of a 
     * user trying to register, and uses that information to make a new entry in the user table
     * 
     * @param nUser is a User object that contains all the information for a new user
     * @return <code>true</code> if the user was added successfully
     *         <code>false</code> otherwise
     */
    public boolean registerNewUser(User nUser);
    
    
     /**
     * This method takes a single parameter, a users id, which is used to 
     * delete a user from the user table.
     * 
     * @param userID is the id of the user that is to be deleted
     * @return <code>true</code> if the user was successfully deleted
     *         <code>false</code> otherwise
     */
    public boolean deleteUser(int userID);
    
    
    /**
     * This method takes a single parameter, a User object containing all the updated information of 
     * a particular user, and uses it to update the information currently held in the user table with 
     * the new information contained in the object.
     * 
     * @param nUser is a User object which contains all the updated information for a particular user
     * @return <code>true</code> if the users details were updated successfully
     *         <code>false</code> otherwise
     */
    public boolean editUserDetails(User nUser);
    
    
    /**
     * This method takes 2 parameters, coins which are the amount of credits a user has earned
     * during a transaction and user id which corresponds to a user in the user table, and are used
     * to add this users earned wallet credits to the wallet credits in their account.
     * 
     * @param coins is the amount of wallet credits a user has gained during a transaction
     * @param userId is the id of the user who has recieved the wallet credits
     * @return <code>true</code> if the users wallet credits where updated successfully
     *         <code>false</code> otherwise
     */
    public boolean updateUserWallet(int coins, int userId);
    
    
    /**
     * This method takes a single parameter, a users id, and uses it to get a user
     * from the user table and reset their credits back to 0.
     * 
     * @param userId is the id of the user who's wallet credits are to be reset
     * @return <code>true</code> if the users wallet credits were reset successfully
     *         <code>false</code> otherwise
     */
    public boolean resetUserWallet(int userId);
    
    
    /**
     * This method takes a single parameter, a passwordRecovery id, which is used to select a password
     * recovery entry in the PasswordRecovery table and place the information in a password recovery object
     * 
     * @param id is the token value used to identify a users account for the password recovery, 
     * the user has been sent this token value and when the link is clicked the method checks 
     * the database if it has a matching value in it.
     * @return PasswordRecovery, a PasswordRecovery object that contains an id for the object 
     * as well as a users email address and the time of the password recovery
     */
    public PasswordRecovery passwordRecoveryById(String id);
    
    
    /**
     * This method takes a single parameter, a passwordRecovery item, and is used to populate 
     * data from the object into an entry into the database.
     * 
     * @param item is a PasswordRecovery objects which contains its id, the user email who 
     * wishes to recover their password and the time of the password recovery
     * @return <code>true</code> if the entry was recorded into the password recovery table
     *         <code>false</code> otherwise
     */
    public boolean registerNewpasswordRecovery(PasswordRecovery item);
    
    
    /**
     * This method takes 2 parameters a users email and password, and uses them to 
     * replace the users password relevant to their email.
     * 
     * @param email is the users email that wishes to reset their password
     * @param pass is the users password they wish to change to
     * @return <code>true</code> if the users password was reset successfully
     *         <code>false</code> otherwise
     */
    public boolean passwordResetByEmail(String email, String pass);
    
    
    /**
     * This method takes a single parameter, an email, which is used to delete 
     * the users password recovery entry in the database.
     * 
     * @param email is the users email that was used for password recovery
     * @return <code>true</code> if the passwordRecovery entry was deleted successfully
     *         <code>false</code> otherwise
     */
    public boolean deletePasswordResetByEmail(String email);
    
    
    /**
     * This method takes 2 parameters, a users emails and their Latch account id, 
     * and uses them to update the users Latch account id.
     * 
     * @param email is the users email
     * @param accountId is the id that the user wishes to change to for their latch
     * @return <code>true</code> if the Latch account id was updated successfully
     *         <code>false</code> otherwise
     */
    public boolean updateUserAccountIdForLatch(String email, String accountId);
    
    
    
    
    
}
