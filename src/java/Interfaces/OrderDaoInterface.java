/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Business.Order;
import java.util.ArrayList;

/**
 *This class provides an abstract view of the methods to be implemented into the OrderDao class
 * 
 * @author Sergio
 */
public interface OrderDaoInterface {
    
    /**
     * This method gets all the orders from the order table in the database and returns them
     * 
     * @return ArrayList of type Order, An ArrayList of all orders that have been made to date
     */
    public ArrayList<Order> getAllOrders();
    
    
    /**
     * This method takes a single parameter which is the id of the user we wish to get previous orders
     * relevant to that user
     * 
     * @param id is the id of the user we wish to get orders for
     * @return ArrayList of type Order, is a list of all orders made by a particular user
     */
    public ArrayList<Order> getAllOrdersByUserId(int id);
    
    
    /**
     * This method takes a single parameter, an order id, and uses it to find
     * a particular order and return it.
     * 
     * @param id is the id of the order to be found
     * @return Order is an object containing all the information for the order was found
     */
    public Order getOrderById(int id);
    
    
    /**
     * This method takes a single parameter, a new order object, and inserts it into the order table.
     * 
     * @param nOrder is a new order to be placed in the order table
     * @return <code>true</code> if the order was inserted successfully
     *         <code>false</code> otherwise
     */
    public boolean insertOrder(Order nOrder);
    
    
    /**
     * This method takes a single parameter, an order id, and deletes it from the order table in the database
     * 
     * @param id is the id of the order to be deleted
     * @return <code>true</code> if order was deleted successfully
     *         <code>false</code> otherwise
     */
    public boolean deleteOrderById(int id);
    
    
    /**
     * This method takes a single parameter, an order object, and updates the 
     * information in the object and stores it back into the order table.
     * 
     * @param nOrder is the order object which we wish to update
     * @return <code>true</code> if the order object was updated successfully
     *         <code>false</code> otherwise
     */
    public boolean updateOrder(Order nOrder);
    
    
    /**
     * This method return the maximum ID stored into the database or -1 if the table is empty.
     * 
     * 
     * @return INTEGER is the id of the last order that was placed on the order table
     */
    public int getLastOrderId();
}
