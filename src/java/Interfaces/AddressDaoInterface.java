/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Business.Address;
import java.util.ArrayList;

/**
 *This class provides an abstract view of the methods to be implemented into the AddressDao class
 * 
 * @author Sergio
 */
public interface AddressDaoInterface {
    /**
     * Method getAllUserAddress takes a users id and returns all the addresses associated with that user
     * 
     * @param userID is the id of the user which we want the addresses for
     * @return ArrayList of type Address containing all addresses for the user
     */
    public ArrayList<Address> getAllUserAddress(int userID);

    /**
     *  Method insertAddres takes a new Address object (newAddress) and 
     * stores this information into the table Address. It also returns a boolean
     * value that confirms if the information was inserted (true) or not (false)
     * 
     * @param newAddress
     * @return boolean value
     */
    public boolean insertAddres(Address newAddress);
    
    /**
     * Method deleteAddressById takes an Address object ID and 
     * delete this information from the table Address. It also returns a boolean
     * value that confirms if the information was deleted (true) or not (false)
     * 
     * @param addressId
     * @return boolean value
     */
    public boolean deleteAddressById(int addressId);
    
    /**
     * Method deleteAllAddressesById takes an User ID and 
     * delete all the addresses associated to this user from the Address table.
     * It also returns a boolean value that confirms if the information was 
     * deleted (true) or not (false)
     * 
     * @param userID
     * @return boolean value
     */
    public boolean deleteAllAddressesById(int userID);
    
    /**
     * Method updateAddress takes an Address object ID and 
     * update the information stored into the table Address. It also returns a boolean
     * value that confirms if the information was updated (true) or not (false)
     * 
     * @param newAddress
     * @return boolean value
     */
    public boolean updateAddress(Address newAddress);
    
    
    /**
     * Method returns the last id inserted for this userID
     * 
     * @param userID is the id of the user that we wish to get the last used address for
     * @return Integer informing us if the selection was successful or not
     */
    public int selectLastIdAdded(int userID);
}
