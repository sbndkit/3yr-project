/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Business.Supplier;
import java.util.ArrayList;

/**
 *This class provides an abstract view of the methods to be implemented into the SupplierDao class
 * 
 * @author Sergio
 */
public interface SupplierDaoInterface {
    
    /**
     * This method takes no parameters but returns an ArrayList of supplier objects with all the suppliers 
     * in the supplier table in the database.
     * 
     * @return ArrayList of type Supplier. A list of all Suppliers objects listed in the database 
     */
    public ArrayList<Supplier> getAllSuppliers();
    
    
    /**
     * This method takes a single parameter, a suppliers id, and uses it 
     * to find that supplier in the supplier table.
     * 
     * @param id is the id of the supplier which is to be found
     * @return Supplier is a Supplier object containing all that suppliers information
     */
    public Supplier getSupplierById(int id);
    
    
    /**
     * This method takes a single parameter, a Supplier object, 
     * who's information is extracted from the object and recorded in the suppler table.
     * 
     * @param nSupplier is a Supplier object which is used to take information from 
     * and place it in a new entry in the supplier table
     * @return <code>true</code> if the supplier was inserted into the supplier table successfully
     *         <code>false</code> otherwise
     */
    public boolean insertSupplier(Supplier nSupplier);
    
    
    /**
     * This method takes a single parameter, a supplier id, which is used to find a supplier 
     * in the supplier table and delete the entry.
     * 
     * @param id is the id of the supplier that is to be deleted
     * @return <code>true</code> if the supplier was deleted successfully
     *         <code>false</code> otherwise
     */
    public boolean deleteSupplierById(int id);
    
    
    /**
     * This method takes a single parameter, a supplier object, 
     * which is taken in and the information is extracted from to 
     * update a particular entry in the supplier table
     * 
     * @param nSupplier is a supplier object containing all the new information 
     * to be updated to a particular entry in the supplier table.
     * @return <code>true</code> if the supplier was updated successfully
     *         <code>false</code> otherwise
     */
    public boolean updateSupplier(Supplier nSupplier);
}
