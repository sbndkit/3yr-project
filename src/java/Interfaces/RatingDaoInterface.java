/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Business.Rating;
import java.util.ArrayList;

/**
 *This class provides an abstract view of the methods to be implemented into the RatingDao class
 * 
 * @author XPS
 */
public interface RatingDaoInterface {
    
    /**
     * This method finds all the comments users made on all products and returns them
     * 
     * @return ArrayList of type Rating that contains all users comments
     */
    public ArrayList<Rating> FindAllComments();
    
    
    /**
     * This method takes a single parameter, a product id, and uses it to get all 
     * comments related to this product id.
     * 
     * @param productId id of the product which we want comments for 
     * @return ArrayList of type Rating that contains all users comments for a particular product
     */
    public ArrayList<Rating> FindAllCommentsByProductId(int productId);
    
    
    /**
     * This method takes a single parameter, a user id, which is used
     * to get comments on products for that user.
     * 
     * @param userId is the id of the user which we want to get comments for
     * @return ArrayList of type Rating that contains all comments for a particular user
     */
    public ArrayList<Rating> FindAllCommentsByUserId(int userId);
    
    
    /**
     * This method takes three parameters, a user id, product id and a comment which are all used to create an entry in the rating table 
     * associating a user and product with a users comment about that product.
     * 
     * @param userId is the id of the user which the comment belongs to
     * @param productId is the id of the product the comment is about
     * @param comment is the comment itself about a particular product
     * @return <code>true</code> if the comment was successfully added
     *         <code>false</code> otherwise
     */
    //public boolean addComment(int userId, int productId, String comment);
    
    
    /**
     * This method takes a single parameter, a user id, which is used to get all 
     * comments made by that user and deletes them.
     * 
     * @param userID is the users id that is used to delete a users comments
     * @return <code>true</code> if all the users comments where deleted successfully
     *         <code>false</code> otherwise
     */
    public boolean deleteAllUserComment(int userID);
    
    
    /**
     * This method takes 2 parameters, a user id and a product id and uses them to 
     * find a users comment on a particular product and delete it.
     * 
     * @param userID is the id of the user which made the comment
     * @param productId is the product which the comment was made on
     * @return <code>true</code> if the comment was deleted successfully
     *         <code>false</code> otherwise
     */
    public boolean deleteUserCommentOnProduct(int userID, int productId);
    
    
    /**
     * This method takes 4 parameters, a user id, a product id, a comment and a star rating and uses 
     * all these to edit a comment a user had made on a particular product.
     * 
     * @param userId is the id of the user which wants to rate the product edit the 
     * rating they gave to a product.
     * @param productId is the id of the product that rating is to be edited
     * @param comment is the comment the user wishes to replace there original comment with
     * @param stars is the star rating that the user wishes to give the product
     * @return <code>true</code> if the rating was edited successfully
     *         <code>false</code> otherwise
     */
    public boolean editRating(int userId, int productId, String comment, float stars);
    
    
    /**
     * 
     * @param userId is the user id of the user that wishes to eave the rating
     * @param productId is the id of the product that is to be rated
     * @param rating is the star rating that the user is giving the product
     * @param comment is the comment the user left on the comment
     * @return <code>true</code> if th rating was successfully added to the rating table
     *         <code>false</code> otherwise
     */
    public boolean addRating(int userId, int productId, int rating, String comment );
    
    
    /**
     * This method takes a single parameter, a product id, and uses it to find all ratings made on a product 
     * and gets the total and averages it to find the actual rating for a particular product.
     * 
     * @param productId is the id of the product we wish to get the rating for
     * @return Float is the rating given to a particular product by all users
     */
    public Float FindTotalRating(int productId);
}
