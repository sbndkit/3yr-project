/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Business.Account;

/**
 *This class provides an abstract view of the methods to be implemented into the AccountDao class
 * 
 * @author Hana
 */

public interface AccountDaoInterface {
    
    /**
     * update(String email) method recieves a single parameter to associate an email with user account
     * Modifies account data for user and returns a true or false value to signify 
     * if the update was successful or not
     *  
     * 
     * 
     * @param email is the email registered to a particular customer.
     * @return <code>true</code> if the account was updated successfully.
     *         <code>false</code> otherwise.
     */
    public boolean update(String email);
    
    
    /**
     * getStatus(String email) method recieves a single parameter to associate an email with user account
     * returns user account associated with the email if one was found.
     * 
     * 
     * @param email is the email registered to a particular customer.
     * @return a user account object if one was found,
     *           null if no user account was found
     */
    public Account getStatus(String email);
    
    
    /**
     * reset(String email) takes a single parameter, an email, associated with a users account
     * resets the users account so that the users failed login attempts are cleared
     * 
     * 
     * @param email is the email registered to a particular customer.
     * @return <code>true</code> if the account was successfully reset
     *         <code>false</code> otherwise
     */
    public boolean reset(String email);
    
    
    /**
     * getTimeDifference(String email) recieves a single parameter, an email, associated with a users account,
     * uses this email to get associated account and calculates the difference in time since the last failed
     * login time and the current login time.
     * 
     * 
     * @param email is the email registered to a particular customer.
     * @return the time difference between the last time the user failed 
     * to login and the current login time or -1 to signify that the account was not found
     */
    public long getTimeDifference(String email);
    
}
