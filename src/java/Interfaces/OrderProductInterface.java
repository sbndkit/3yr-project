/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Business.OrderProduct;
import java.util.ArrayList;

/**
 *This class provides an abstract view of the methods to be implemented into the OrderProductDao class
 * 
 * @author Sergio
 */
public interface OrderProductInterface {
    
    /**
     * This method takes no parameters but returns all the elements in the orderProduct table in the database.
     * 
     * @return ArrayList of type OrderProduct, A list of all entries in the orderProduct table.
     */
    public ArrayList<OrderProduct> getAllOrdersProduct();
    
    
    /**
     * This method takes a single parameter, a user id, and uses it to retrieve all
     * orderProduct entries related to that user.
     * 
     * @param userID is the id of the user we wish to get orderProducts for
     * @return ArrayList of type OrderProduct, A list of orderProducts relevant to the userID
     */
    public ArrayList<OrderProduct> getAllOrdersProductByUserID(int userID);
    
    
    /**
     * This method takes in 2 parameters, an order id and a product id and uses them to find a 
     * specific orderProduct entry in the orderProduct table in the database.
     * 
     * @param orderID is the id of the order we wish to find
     * @param productID is the id of the product we wish to find on the order
     * @return OrderProduct an OrderProduct object containing all the information for that order product
     */
    public OrderProduct getOrderProductById(int orderID, int productID);
    
    
    /**
     * This method takes a single parameter, an OrderProduct object and inserts 
     * it into the orderProduct table.
     * 
     * @param nOrder is the orderProduct object that is to be inserted into 
     * the orderProduct table in the database
     * @return <code>true</code> if the OrderProduct was inserted successfully
     *         <code>false</code> otherwise
     */
    public boolean insertOrderProduct(OrderProduct nOrder);
    
    
    /**
     * This method takes a single parameter, an OrderProduct id, 
     * that is used to find an entry in the OrderProduct table and delete it.
     * 
     * @param orderProductID is the id of the OrderProduct that is to be deleted
     * @return <code>true</code> if the OrderProduct was successfully deleted
     *         <code>false</code> otherwise
     */
    public boolean deleteOrderProductById(int orderProductID);
    
    
    /**
     * This method takes 2 OrderProduct parameters,one containing the new OrderProduct information
     * and another containing the new OrderProduct information, and updates the old information 
     * with the new and updates the entry in the database with this new information
     * 
     * @param newData is an OrderProduct object containing the new information 
     * to be updated to the old OrderProduct entry 
     * @param oldData is an OrderProduct object that contains the old original data for the OrderProduct entry
     * @return <code>true</code> if the OrderProduct entry was updated successfully
     *         <code>false</code> otherwise
     */
    public boolean updateOrderProduct(OrderProduct newData, OrderProduct oldData);
}
