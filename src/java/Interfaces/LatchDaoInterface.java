/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import com.elevenpaths.latch.LatchUser;

/**
 *This class provides an abstract view of the methods to be implemented into the LatchDao class
 * 
 * @author XPS
 */
public interface LatchDaoInterface {
    
    /**
     * This method is used to get information for connection to the latch server
     * 
     * @return LatchUser is the object which contains the details for connection to latch server
     */
    public LatchUser getLatchInfo();
    
    
    /**
     * This method set the information in the Latch table to have a connection with the latch server
     * 
     * @param APP_ID is the id that identifies our application on the latch server
     * @param SECRET is a token that validates the connection
     * @return <code>true</code> if the information was set successfully
     *         <code>false</code>otherwise
     */
    public boolean setLatchInfo(String APP_ID, String SECRET);
    
}
