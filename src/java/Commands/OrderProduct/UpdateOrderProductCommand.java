/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.OrderProduct;

import Commands.OrderProduct.*;
import Business.OrderProduct;
import Commands.Command;
import Daos.OrderProductDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The UpdateOrderProductCommand allows a user to update their order
 *
 * @author Sergio
 */
public class UpdateOrderProductCommand implements Command {

    /**
     * The execute method of the UpdateOrderProductCommand allows users to
     * replace their orderProducts objects with new orderProduct objects.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page and
     * session attribute containing the  new orderProduct 
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        // Getting the current session
        HttpSession session = request.getSession();
        // creating a new instance to access the database
        OrderProductDao instance = new OrderProductDao();
        // getting the parameters to search from the request
        OrderProduct newObj = (OrderProduct) request.getAttribute("OrderProductToUpdated");
        OrderProduct oldObj = (OrderProduct) request.getAttribute("OrderProductToUpdated");
        // Consulting the database
        boolean updated = instance.updateOrderProduct(newObj, oldObj);

        if (updated) {
            request.setAttribute("orderProductUpdated", newObj);
        } else {
            request.setAttribute("orderProductUpdated", null);
        }
        // site where the information is processed
        forwardToJsp = "/index.jsp";

        return forwardToJsp;
    }
}
