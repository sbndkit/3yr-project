/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.OrderProduct;

import Commands.OrderProduct.*;
import Business.OrderProduct;
import Commands.Command;
import Daos.OrderProductDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The GetOrderProductByIDCommand returns a single orderProduct object
 *
 * @author Sergio
 */
public class GetOrderProductByIDCommand implements Command {

    /**
     * The execute method of the GetOrderProductByIDCommand is used to return a
     * single orderProduct object based on if the object's id is the same as the
     * id selected by the user
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page and
     * session attribute containing a single orderProduct object.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "";
        // getting the current session
        HttpSession session = request.getSession();
        // new object to acces the database
        OrderProductDao instance = new OrderProductDao();
        // Getting the user stored into the session
        OrderProduct s = (OrderProduct) session.getAttribute("orderProduct");
        // searching for an user based on the ID
        OrderProduct searchById = instance.getOrderProductById(s.getOrderId(), s.getProductId());
        // If it's not null setting boolean variable as true and setting the userfound (searchById) as an attribute
        if (searchById != null) {

            //session.setAttribute("loggedSessionId", clientSessionId);
            session.setAttribute("found", true);
            session.setAttribute("searchById", searchById);
        } // If null, setting the boolean variable as false and the user as null
        else {
            session.setAttribute("found", false);
            session.setAttribute("searchById", null);
        }
        forwardToJsp = "/loginSuccess.jsp";
        // returning the page to forward
        return forwardToJsp;
    }
}
