/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.OrderProduct;

import Business.CartItem;
import Commands.Command;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The UpdateCartQttyCommand allows the update of quantities in the cart
 *
 * @author Sergio
 */
public class UpdateCartQttyCommand implements Command {

    /**
     * The execute method of the UpdateCartQttyCommand allows a user to change
     * the quantity of a product they wish to order
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page and
     * session attribute containing the orderProduct object that was passed to
     * it but with a new quantity.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        HttpSession session = request.getSession();

        ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

        int qtty = Integer.parseInt(request.getParameter("qtty"));
        int itemId = Integer.parseInt(request.getParameter("itemId"));

        int index = -1;

        for (CartItem item : currentCart) {
            if (item.getProduct().getProductID() == itemId) {
                index = currentCart.indexOf(item);
            }
        }
        currentCart.get(index).setQty(qtty);

        session.setAttribute("currentCart", currentCart);

        forwardToJsp = "cart.jsp";

        return forwardToJsp;
    }

}
