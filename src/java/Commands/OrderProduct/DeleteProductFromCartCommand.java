/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.OrderProduct;

import Business.CartItem;
import Commands.Command;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The DeleteProductFromCartCommand allows a user to removes items from the cart
 *
 * @author Sergio
 */
public class DeleteProductFromCartCommand implements Command {

    /**
     * The execute method of the DeleteProductFromCartCommand allows a user to
     * delete singular items from the cart. The applications is able to delete a
     * singular item as it is passed the productId of the product as a parameter
     * in the request
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        HttpSession session = request.getSession();

        ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

        int toDelete = Integer.parseInt(request.getParameter("cartItemToDelete"));
        int index = -1;

        for (CartItem item : currentCart) {
            if (item.getProduct().getProductID() == toDelete) {
                index = currentCart.indexOf(item);
            }
        }

        currentCart.remove(index);

        session.setAttribute("currentCart", currentCart);

        forwardToJsp = "cart.jsp";

        return forwardToJsp;
    }

}
