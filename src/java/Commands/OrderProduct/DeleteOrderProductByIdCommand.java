/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.OrderProduct;

import Commands.Command;
import Daos.OrderProductDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The DeleteOrderProductByIdCommand deletes an orderProduct object from the
 * database
 *
 * @author Sergio
 */
public class DeleteOrderProductByIdCommand implements Command {

    /**
     * The execute method of the DeleteOrderProductByIdCommand allows an admin
     * to delete an orderProduct object based on the order id selected
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        // Getting the current session
        HttpSession session = request.getSession();
        // creating a new instance to access the database
        OrderProductDao instance = new OrderProductDao();

        int orderID = -1;
        // getting the parameters to search from the request
        try {
            orderID = Integer.parseInt(request.getParameter("orderID"));

            // deleting the object from the database
            boolean deleted = instance.deleteOrderProductById(orderID);
            // setting a new arrayList with the orders returned
            session.setAttribute("deleted", deleted);
            // site where the information is processed

            forwardToJsp = "/index.jsp";
        } catch (NumberFormatException e) {
            forwardToJsp = "/Error.html";
        }

        return forwardToJsp;
    }
}
