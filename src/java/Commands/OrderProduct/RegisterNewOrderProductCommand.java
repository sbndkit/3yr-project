/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.OrderProduct;

import Business.Address;
import Business.CartItem;
import Business.Product;
import Business.User;
import Commands.Command;
import Daos.AddressDao;
import Daos.ProductDao;
import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Environment;
import java.util.ArrayList;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The RegisterNewOrderProductCommand registers new orderProduct objects to the
 * system
 *
 * @author Sergio
 */
public class RegisterNewOrderProductCommand implements Command {

    /**
     * The execute method of the RegisterNewOrderProductCommand allows the user
     * to place a new order on the database but in order to do this we must also
     * create orderProduct objects to show what products were ordered and the
     * quantity of the product that was ordered
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page and
     * session attribute containing an ArrayList of cartItem objects.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "";

        HttpSession session = request.getSession();

        ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

        if (currentCart == null) {
            currentCart = new ArrayList();

        }

        ProductDao instance = new ProductDao();

        CartItem newItem = new CartItem();
        int qtty = Integer.parseInt(request.getParameter("quantity"));

        Product item = instance.getProduct(Integer.parseInt(request.getParameter("id")));

        newItem.setQty(qtty);
        newItem.setProduct(item);

        Collections.sort(currentCart);

        int binSearch = Collections.binarySearch(currentCart, newItem);

        if (binSearch < 0) {
            currentCart.add(newItem);
        } else {
            currentCart.get(binSearch).setQty(currentCart.get(binSearch).getQty() + qtty);
        }

        User user = (User) session.getAttribute("user");
        if (user != null) {
            AddressDao ADInstance = new AddressDao();
            ArrayList<Address> addressList = ADInstance.getAllUserAddress(user.getUserId());
            session.setAttribute("addressList", addressList);
        } else {
            session.setAttribute("addressList", null);
        }
        session.setAttribute("currentCart", currentCart);

        forwardToJsp = "cart.jsp";
        

       /// Generate a cliente Token \\\ 
       
       String clientToken = (String)session.getAttribute("clientToken");
       
       if(clientToken == null){
           clientToken = gateway.clientToken().generate();
           session.setAttribute("clientToken",clientToken);
           session.setAttribute("gateway", gateway);
       }   
       
       
        return forwardToJsp;
    }
    
    BraintreeGateway gateway = new BraintreeGateway(
            Environment.SANDBOX,
            "83cr4qfg7cpdm32v",
            "hfmqcdgkrxxb4b6w",
            "9a58840d9a2f377d3ce31557dbbedc87"
    );
}
