/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Narrator;

import Commands.Command;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The NarratorOffCommand is used to set the Narrator attribute to Off this in
 * turn will stop our webpages from generating the JavaScript that creates the
 * narrator and it's methods
 *
 * @author D00128036
 */
public class NarratorOffCommand implements Command {

    /**
     * The execute method in the NarratorOffCommand is the method that sets the
     * narrator attribute to Off
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String isOn = "Off";
        HttpSession session = request.getSession();
        session.setAttribute("narrator", isOn);
        return "about.jsp";
    }
}
