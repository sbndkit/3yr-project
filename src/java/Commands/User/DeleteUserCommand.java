
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.User;

/**
 * The DeleteUserCommand allows Admins to delete users
 *
 * @author Sergio
 */
import Commands.Command;
import Daos.UserDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class DeleteUserCommand implements Command {
    /**
     * The execute method of the DeleteUserCommand allows admins of the system
     * to delete users from the application and database
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether the execute method was successful or not.
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        // Getting the current session
        HttpSession session = request.getSession();
        // creating a new instance to access the database
        UserDao instance = new UserDao();
        
        int userID = -1;
        // getting the parameters to search from the request
        try {
            userID = Integer.parseInt(request.getParameter("userID"));
        } catch (NumberFormatException e)  {
            e.printStackTrace();
        }
        // deleting the object from the database
        boolean deleted = instance.deleteUser(userID);
        // setting a new arrayList with the users returned
        session.setAttribute("deleted", deleted);
        
        /*         ArrayList<User> lst = instance.FindAllUsers();
        
        
        // Setting the list as an attibute in the session
        request.setAttribute("getallusers", lst);*/
        // Setting the page where the servlet will forward the request
        forwardToJsp = "success.jsp";

        return forwardToJsp;
    }
}
