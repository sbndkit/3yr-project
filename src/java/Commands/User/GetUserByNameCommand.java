
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.User;

/**
 * The GetUserByNameCommand returns a singular user object
 *
 * @author Sergio
 */
import Business.User;
import Commands.Command;
import Daos.UserDao;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GetUserByNameCommand implements Command {

    /**
     * The execute method of the GetUserByNameCommand is used to return a user
     * object stored as an attribute. The user object is selected on the basis
     * that its name matches the name selected by the user.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether the execute method was successful or not.
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        // Getting the current session
        HttpSession session = request.getSession();
        // creating a new instance to access the database
        UserDao instance = new UserDao();
        // getting the parameters to search from the request
        String fName = request.getParameter("fName");
        String lName = request.getParameter("lName");

        if (fName != null && lName != null) {
            // Consulting the database
            ArrayList<User> userByNames = instance.getUserByName(fName, lName);
            // setting a new arrayList with the users returned
            session.setAttribute("usersByNames", userByNames);
            // site where the information is processed
        }
        forwardToJsp = "/index.jsp";

        return forwardToJsp;
    }
}
