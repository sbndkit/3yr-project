
package Commands.User;

/**
 *
 * @author Sergio
 */
import Commands.Command;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutCommand implements Command 
{
    /**
     * The execute method of the LoginCommand allows users of the system to log
     * into their accounts provided the user enters a valid username and
     * password. However if the user enters a incorrect password but a valid
     * email a counter is created with an account object. If the user fails to
     * enter the correct password for 5 consecutive attempts the account is then
     * locked for five minutes
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether the execute method was successful or not.
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) 
    {
        String forwardToJsp;

        //If login successful, store the session id for this client...
        HttpSession session = request.getSession();
        String clientSessionId = session.getId();
        
        if(session.getAttribute("loggedSessionId") != null)
        {
            try {
                session.invalidate();
                request.logout();
                session = null;
                request = null;
            } catch (ServletException ex) {
                Logger.getLogger(LogoutCommand.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        forwardToJsp = "/logout.jsp";				

        return forwardToJsp;
    }
}
