/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.User;

import Business.User;
import Commands.Command;
import Daos.UserDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



/**
 * The UpdateUserDetailsCommand allows the modification of a users details
 * 
 * @author Sergio
 */
public class UpdateUserDetailsCommand implements Command{

    /**
     * The execute method of the UpdateUserDetailsCommand allows a user/admin to
     * modify their/a profile in the system database.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        // Getting the current session
        HttpSession session = request.getSession();
        // creating a new instance to access the database
        UserDao instance = new UserDao();
        // getting the parameters to search from the request
        User toUpdate = new User();
        boolean updated = false;
        User user = (User) session.getAttribute("user");
                
         if(request.getParameter("userID") != null && request.getParameter("fName") != null && request.getParameter("lName") != null && request.getParameter("email") != null &&
                request.getParameter("phone") != null && request.getParameter("password") != null && request.getParameter("walletCredits") != null)
        {
        toUpdate.setUserId(Integer.parseInt(request.getParameter("userID")));
        toUpdate.setfName(request.getParameter("fName"));
        toUpdate.setlName(request.getParameter("lName"));
        toUpdate.setEmail(request.getParameter("email"));
        toUpdate.setPhone(request.getParameter("phone"));
        toUpdate.setIsAdmin(Boolean.parseBoolean(request.getParameter("admin")));
        toUpdate.setPasswordHashed(request.getParameter("password"));
        toUpdate.setWalletCredits(Integer.parseInt(request.getParameter("walletCredits")));
        // Consulting the database
        updated = instance.editUserDetails(toUpdate);
        }

        if (updated) {
            session.setAttribute("userUpdated", toUpdate);
        } else {
             request.setAttribute("userUpdated", null);
        }
        
        if(user.getIsIsAdmin() == true)
        {
            
        // site where the information is processed
        forwardToJsp = "/adminController.jsp";
        
        if(user.getUserId()==toUpdate.getUserId())
            {
                session.setAttribute("user", toUpdate);
                forwardToJsp = "/processRequest?action=getuserbyid&user="+toUpdate.getUserId();
            }
        }
        else
        {
            session.setAttribute("user", toUpdate);
            forwardToJsp = "/index.jsp";
        }

        return forwardToJsp;
    }
}
