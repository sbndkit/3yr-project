/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.User;

import Business.User;
import Commands.Command;
import Daos.UserDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Allows users to register onto the system/database if they are not already a
 * part of our system/database
 *
 * @author Sergio
 */
public class RegisterNewUserCommand implements Command {

    /**
     * The execute method of the RegisterNewUserCommand allows users to register
     * in our system/database.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page.
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "error.jsp";

        //Getting the user to be registered
        User user = new User();
        if(request.getParameter("fName") != "" && request.getParameter("lName") != "" && request.getParameter("email") != "" &&
                request.getParameter("phone") != "" && request.getParameter("password") != "")
            {
                user.setfName(request.getParameter("fName"));
                user.setlName(request.getParameter("lName"));
                user.setEmail(request.getParameter("email"));
                user.setPhone(request.getParameter("phone"));
                user.setPassword(request.getParameter("password").toCharArray());
                String admin = request.getParameter("adminChoice");
                if (admin != null && admin.equals("1"))
                {
                    user.setIsAdmin(true);
                }else{
                    user.setIsAdmin(false);
                }
            }
        
        // Checking for null value
        if (user != null)
        {
            UserDao instance = new UserDao();
            if (instance.registerNewUser(user))
            {
                HttpSession session = request.getSession();
                User me = (User) session.getAttribute("user");
                if (me != null) {
                    if (me.getIsIsAdmin()) {
                        forwardToJsp = "/success.jsp";
                    } else {
                        forwardToJsp = "/index.jsq";
                    }
                } else {
                    forwardToJsp = "/index.jsp";
                }
            }
            else
            {
                forwardToJsp = "/error.jsp";
            }
        }
        
        return forwardToJsp;
    }
    
}
