/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.User;

/**
 * The loginCommand allows users to login into the application
 *
 * @author Sergio
 */
import Business.Account;
import Business.User;
import Commands.Command;
import Daos.AccountDao;
import Daos.LatchDao;
import Service.UserService;
import com.elevenpaths.latch.Latch;
import com.elevenpaths.latch.LatchResponse;
import com.elevenpaths.latch.LatchUser;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginCommand implements Command {

    /**
     * The execute method of the LoginCommand allows users of the system to log
     * into their accounts provided the user enters a valid username and
     * password. However if the user enters a incorrect password but a valid
     * email a counter is created with an account object. If the user fails to
     * enter the correct password for 5 consecutive attempts the account is then
     * locked for five minutes
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether the execute method was successful or not.
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "";
        HttpSession session = request.getSession();

        //The user wants to log in...
        String username = request.getParameter("username");
        char[] password = null;
        boolean pass = true;
        String passReceived = request.getParameter("password");
        
        if (passReceived != null && passReceived != "") {
            // converting the password into a Char[] array
            password = (passReceived.toCharArray());
            // checking if the password is null
            if (password != null) {
                // verifying if the characters are valid
                for (char item : password) {
                    if (Character.isDigit(item)) {
                        continue;
                    } else if (Character.isLetter(item)) {
                        continue;
                    } else {
                        // if some of the characters are not valid then setting the var pass as false to give an error login
                        pass = false;
                    }
                }
            }
        } else{
            // if the passReceived is null or empty then the login fails
            pass = false;
        }
        if (!pass) {
            // If login doesn't pass we redirect to the error page with the "redirecting" flag set as yes so the form for the login will be displayed
            request.setAttribute("redirecting", "yes");
            forwardToJsp = "./error.jsp";
        } else {

            //Use the UserServive class to login...
            UserService userService = new UserService();
            // instance to check the account status
            AccountDao instance = new AccountDao();
            // getting the current account status
            Account account = instance.getStatus(username);
            // Variable to set the login lag
            int lag = 0;
            long timePassed = 0;
            // checking if the time in miliseconds since the last login'
            if (account != null) {
                
                timePassed = instance.getTimeDifference(username);

                // delay depending of the account status
                switch (account.getAccountStatus()) {
                    case "stg1":
                        lag = 300000;
                        break;
                    case "stg2":
                        lag = 900000;
                        break;
                    case "locked":
                        lag = 3600000;
                        break;
                    default:
                        lag = 0;
                        break;
                }
            }
            if (lag < timePassed || account == null) {

                User userLogginIn = userService.login(username, password);

                if (userLogginIn != null) {

                    // setting variable for integration
                    boolean isLatchOn = true;
                    // retreaving information from from the latch database
                    LatchUser info = new LatchDao().getLatchInfo();
                    // checking if the user has a latch token set
                    if (!userLogginIn.getAccountId().isEmpty()) {
                        // Creating a Latch object
                        Latch latch = new Latch(info.getAppId(), info.getSecretKey());
                        // retreaving the latch status from the server
                        LatchResponse lResponse = latch.status(userLogginIn.getAccountId());
                        // Accesing to the gson to get the latch status
                        // Here we need to add checkings for null
                        String status = lResponse.getData().get("operations").getAsJsonObject().get(info.getAppId()).getAsJsonObject().get("status").getAsString();
                        // If the status is off, the service is not available
                        if (status.equalsIgnoreCase("off")) {
                            // Set the variable to false
                            isLatchOn = false;
                        }
                    }
                    // if the latch is on (not locked) we login normally
                    if (isLatchOn) {
                        //If login successful, store the session id for this client...
                        
                        String clientSessionId = session.getId();
                        session.setAttribute("loggedSessionId", clientSessionId);
                        session.setAttribute("user", userLogginIn);
                        //checks if the remember me checkbox is checked 
                        if (request.getParameter("rememberMe") == "checked") {//if checked then stores the username as an attribute 
                            session.setAttribute("remeberMeCookie", username);
                        }

                        instance.reset(username);

                        request.setAttribute("redirecting", "no");

                        forwardToJsp = request.getParameter("forwardToJsp");

                        if (!userLogginIn.getAccountId().isEmpty()) {
                            session.setAttribute("view", "unpair");
                        } else {
                            session.setAttribute("view", "pair");
                        }

                    } //otherwise the service is not available (status is off)
                    else {
                        request.setAttribute("redirecting", "yes");
                        forwardToJsp = "/error.jsp";
                    }
                } else {
                    request.setAttribute("redirecting", "yes");
                    forwardToJsp = "/error.jsp";
                }
            }else{
                request.setAttribute("redirecting", "locked");
                session.setAttribute("user", null);
                forwardToJsp = "/error.jsp";
            }
        }

        return forwardToJsp;
    }
}
