/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Supplier;

import Business.Supplier;
import Commands.Command;
import Daos.SupplierDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The RegisterNewSupplierCommand allows the User to register suppliers
 *
 * @author Sergio
 */
public class RegisterNewSupplierCommand implements Command {

    /**
     * The execute of the RegisterNewSupplierCommand is used in the Admin
     * functionality. It allows the admin to add new suppliers to the database
     * and application
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether the execute method was successful or not.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "";

        //Getting the supplier to be registered
        Supplier supplier = new Supplier();
        
        supplier.setSupplierName(request.getParameter("supplierName"));
        supplier.setPhone(request.getParameter("supplierPhone"));
        supplier.setEmail(request.getParameter("supplierEmail"));
        supplier.setAddress(request.getParameter("supplierAddress"));
        supplier.setCounty(request.getParameter("supplierCounty"));
        supplier.setCountry(request.getParameter("supplierCountry"));
        
        // Checking for null value
        if (supplier != null)
        {
            SupplierDao instance = new SupplierDao();
            if (instance.insertSupplier(supplier))
            {
                forwardToJsp = "success.jsp";
            }
            else
            {
                forwardToJsp = "adminController.jsp";
            }
        }
        
        return forwardToJsp ;
    }
}
