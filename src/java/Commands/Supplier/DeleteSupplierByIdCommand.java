/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Supplier;

import Business.Supplier;
import Commands.Command;
import Daos.SupplierDao;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The DeleteSupplierByIdCommand allows Admins of the application to delete
 * suppliers
 *
 * @author Sergio
 */
public class DeleteSupplierByIdCommand implements Command {

    /**
     * The execute method of the DeleteSupplierByIdCommand allows a user to
     * delete a supplier from the database based on their id number.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether the execute method was successful or not.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        // Getting the current session
        HttpSession session = request.getSession();
        // creating a new instance to access the database
        SupplierDao instance = new SupplierDao();
        
        int supplierID = -1;
        // getting the parameters to search from the request
        try {
            supplierID = Integer.parseInt(request.getParameter("supplierID"));
        } catch (NumberFormatException e)  {
            e.printStackTrace();
        }
        // deleting the object from the database
        boolean deleted = instance.deleteSupplierById(supplierID);
        
        if (deleted)
            {
                ArrayList<Supplier> lst = instance.getAllSuppliers();
                session.setAttribute("supplierLst", lst);
                forwardToJsp = "/success.jsp";
                
            }
            else
            {
                forwardToJsp = "/error.jsp";
            }
        
        

        return forwardToJsp;
    }
}
