/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Supplier;

import Business.Supplier;
import Commands.Command;
import Daos.SupplierDao;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The GetAllSuppliersCommand returns a list of all suppliers
 *
 * @author Sergio
 */
public class GetAllSuppliersCommand implements Command {
    /**
     * The execute method of the SupplierByIdCommand allows a user to
     * delete a supplier from the database based on their id number.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether the execute method was successful or not.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        // Seting the jsp page to forward to
        String forwardToJsp = "";
        // Creating the instance to reteave all the data from the database
        SupplierDao instance = new SupplierDao();
        // Setting an ArrayList with all the information retreaved
        ArrayList<Supplier> lst = instance.getAllSuppliers();
        // Checking if the list is null
        if (lst != null) {
            // Getting the session
            HttpSession session = request.getSession();
            // Setting the list as an attibute in the session
            session.setAttribute("supplierLst", lst);
            // Setting the page where the servlet will forward the request
            forwardToJsp = "suppliers.jsp";
        } else {
            // // Setting the page where the servlet will forward the request
            forwardToJsp = "index.jsp";
        }

        return forwardToJsp;
    }
}
