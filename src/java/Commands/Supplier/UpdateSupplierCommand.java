/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Supplier;

import Business.Supplier;
import Commands.Command;
import Daos.SupplierDao;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The UpdateSupplierCommand allows the editing of suppliers
 *
 * @author Sergio
 */
public class UpdateSupplierCommand implements Command {

    /**
     * The execute of the UpdateSupplierCommand is used in the Admin
     * functionality. It allows the admin to manipulate and change the information of suppliers in the database
     * and application with the exception of their supplier id numbers
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether the execute method was successful or not.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        // Getting the current session
        HttpSession session = request.getSession();
        // creating a new instance to access the database
        SupplierDao instance = new SupplierDao();
        // getting the parameters to search from the request
        Supplier toUpdate = new Supplier();
        boolean updated = false;
        
        if(request.getParameter("supplierId") != null && request.getParameter("userName") != null && request.getParameter("address") != null 
                && request.getParameter("email") != null && request.getParameter("country") != null && request.getParameter("county") != null && 
                request.getParameter("phone") != null)
        {
        toUpdate.setSupplierId(Integer.parseInt(request.getParameter("supplierId")));
        
        toUpdate.setSupplierName(request.getParameter("userName"));
        toUpdate.setAddress(request.getParameter("address"));
        toUpdate.setEmail(request.getParameter("email"));
        toUpdate.setPhone(request.getParameter("phone"));
        toUpdate.setCountry(request.getParameter("country"));
        toUpdate.setCounty(request.getParameter("county"));
        // Consulting the database
        updated = instance.updateSupplier(toUpdate);
        }
        
        

        if (updated) {
            request.setAttribute("supplierUpdated", toUpdate);
            ArrayList<Supplier> lst = instance.getAllSuppliers();
            session.setAttribute("supplierLst", lst);
        } else {
             request.setAttribute("supplierUpdated", null);
        }
        // site where the information is processed
        forwardToJsp = "/success.jsp";

        return forwardToJsp;
    }
}
