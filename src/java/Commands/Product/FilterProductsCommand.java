/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Product;

import Business.HashMap;
import Business.Product;
import Commands.Command;
import Daos.ProductDao;
import java.util.Arrays;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The filter command allows a user to alter the product list shown to them
 *
 * @author XPS
 */
public class FilterProductsCommand implements Command {

    /**
     * The execute method of the FilterProductsCommand allows a user to alter
     * the list of products shown to the user in three ways. Thery are to list
     * phones of a brand, list products in a particular price-range, List
     * products of a particular brand and in a particular price range
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        // Seting the jsp page to forward to
        String forwardToJsp = "";
        // Getting the session
        HttpSession session = request.getSession();

        ProductDao instance = new ProductDao();
        // Setting an ArrayList with all the information retreaved
        HashMap<String, Product> lst = instance.getAllProducts();

        Object[] keys = lst.getArrayOfKeys();

        HashMap<Integer, Product> filtered = new HashMap<Integer, Product>(4);

        boolean allFalse = true;

        boolean[] ranges = new boolean[4];
        Arrays.fill(ranges, false);

        for (int i = 0; i < 4; i++) {

            if (request.getParameter("range-" + i) != null) {
                int index = (Objects.hash(i) % ranges.length);
                ranges[index] = true;
                allFalse = false;
            }
        }

        if (allFalse) {
            ranges = null;
        }

        request.setAttribute("rangesFilter", ranges);

        boolean addAll = true;

        for (int x = 0; x < keys.length; x++) {
            String brand = (String) keys[x];
            
            if (request.getParameter(brand) != null) {
                addAll = false;
            }
        }

        for (int i = 0; i < keys.length; i++) {

            String brand = (String) keys[i];

            if (request.getParameter(brand) != null || addAll) {

                allFalse = false;
                for (Product item : lst.getArrayOfTreeSets()[i]) {
                    double price = item.getPrice();
                    if (price > 500) {
                        //System.out.println("500+ " + 3 % 4 + ranges[3]);
                        filtered.put(3, item);
                    } else if (price >= 200) {
                        //System.out.println("200-499+ " + 2 % 4 + ranges[2]);
                        filtered.put(2, item);
                    } else if (price >= 100) {
                        //System.out.println("100-199+ " + 1 % 4 + ranges[1]);
                        filtered.put(1, item);
                    } else if (price < 100) {
                        //System.out.println("0-99+ " + 0 % 4 + ranges[0]);
                        filtered.put(0, item);
                    }
                }
            }
        }

        session.setAttribute("products", filtered);

        forwardToJsp = "/phoneStore.jsp";

        return forwardToJsp;
    }

}
