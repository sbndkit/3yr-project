/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Product;

import Business.Product;
import Commands.Command;
import Daos.ProductDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The GetProductByIdCommand is used to select and individual product
 *
 * @author Sergio
 */
public class GetProductByIdCommand implements Command {

    /**
     * The execute method of the GetProductByIdCommand allows the user to view
     * an individual product. It does this by retrieving the product from the
     * database based on it's Id being the same as the one selected by the user
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "";
        // getting the current session
        HttpSession session = request.getSession();
        // new object to acces the database
        ProductDao instance = new ProductDao();
        int id = -1;
        if (request.getParameter("id") != null) {
            // Getting the user stored into the session
            id = Integer.parseInt(request.getParameter("id"));
        }
        // searching for an user based on the ID
        Product searchById = instance.getProduct(id);
        // If it's not null setting boolean variable as true and setting the userfound (searchById) as an attribute
        if (searchById != null) {

            //session.setAttribute("loggedSessionId", clientSessionId);
            request.setAttribute("found", true);
            request.setAttribute("searchByName", searchById);
        } // If null, setting the boolean variable as false and the user as null
        else {
            request.setAttribute("found", false);
            request.setAttribute("searchByName", null);
        }
        forwardToJsp = "/phoneItem.jsp";
        // returning the page to forward
        return forwardToJsp;
    }
}
