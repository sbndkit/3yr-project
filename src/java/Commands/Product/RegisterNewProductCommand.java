/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Product;



import Business.Product;
import Commands.Command;
import Daos.ProductDao;
import com.oreilly.servlet.MultipartRequest;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Sergio
 */
public class RegisterNewProductCommand implements Command{
    
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "";

        //Getting the product to be registered
        Product product = new Product();
        MultipartRequest mRequest;
        try {
            mRequest = new MultipartRequest(request,request.getServletContext().getRealPath("") + "../../web/images/phones",50);
        } catch (IOException ex) {
            Logger.getLogger(RegisterNewProductCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        product.setProductName(request.getParameter("productName"));
        product.setBrand(request.getParameter("brand"));
        product.setDesc(request.getParameter("desc"));
        product.setPrice(Double.parseDouble(request.getParameter("productPrice")));
        product.setStock(Integer.parseInt(request.getParameter("productStock")));
        
        //takes entire filename including .jpg
        String pictureLink = (String)request.getParameter("pictureName");
        if(pictureLink.substring(0,pictureLink.indexOf(".")) != null)
        {
        //sends on the actual name of the products picture
        product.setPictureLink(pictureLink.substring(0,pictureLink.indexOf(".")));
        }
        ProductDao instance = new ProductDao();
        
        if(product != null)
        {
            instance.registerProduct(product);
        }
        forwardToJsp="adminController.jsp";
        return forwardToJsp;
    }
}
