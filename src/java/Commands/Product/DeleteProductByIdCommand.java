/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Product;

import Commands.Command;
import Daos.ProductDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The DeleteProductByIdCommand allows an admin to delete products from the
 * database
 *
 * @author Sergio
 */
public class DeleteProductByIdCommand implements Command {

    /**
     * The DeleteProductByIdCommand is the command that allows an Admin to
     * delete an individual product from the database by taking in the product
     * object's id the user selection to be deleted and searching the database
     * and deleting the product with that unique id.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        // Getting the current session
        HttpSession session = request.getSession();
        // creating a new instance to access the database
        ProductDao instance = new ProductDao();

        int productID = -1;
        // getting the parameters to search from the request
        try {

            productID = Integer.parseInt(request.getParameter("ID"));

            // deleting the object from the database
            instance.deleteProduct(productID);

            // site where the information is processed
            forwardToJsp = "success.jsp";
        } catch (NumberFormatException e) {
            forwardToJsp = "error.jsp";
            //e.printStackTrace();
        }

        return forwardToJsp;
    }
}
