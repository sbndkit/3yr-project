/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Product;

import Business.HashMap;
import Business.Product;
import Commands.Command;
import Daos.ProductDao;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The GetProductByNameCommand retrieves between one or several products
 *
 * @author Sergio
 */
public class GetProductByNameCommand implements Command {

    /**
     * The execute method of GetProductByNameCommand can be used by the user to
     * retrieve either a single product or a list of products depending on their
     * name. For example if a user were to submit the String Samsung then the
     * user will retrieve a list of all products with Samsung in their name
     * however if they were to specify the product to greater detail by entering
     * in Samsung Galaxy S6 they will retrieve only products with this name.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page.
     */
     @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp="";
        // getting the current session
        HttpSession session = request.getSession();
        // new object to acces the database
        ProductDao instance = new ProductDao();
        // Getting the user stored into the session
        String product = request.getParameter("productName");
        
        // searching for an user based on the ID
        HashMap<String, Product> searchByName = instance.getProductLikeName(product);
        
        for(TreeSet<Product> item : searchByName.getArrayOfTreeSets())
        {
            for(Product p : item)
            {
                System.out.println(p);
            }
        }
        
         System.out.println("\n\n" + searchByName.getCount());
        // If it's not null setting boolean variable as true and setting the userfound (searchById) as an attribute
        
        if (searchByName != null) {
            
            session.setAttribute("products", searchByName);
        }
        // If null, setting the boolean variable as false and the user as null
        else {
            session.setAttribute("products", null);
        }
        forwardToJsp="/phoneStore.jsp";
        // returning the page to forward
        return forwardToJsp;
    }
}