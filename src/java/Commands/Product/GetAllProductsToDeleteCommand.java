/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Product;

import Business.HashMap;
import Business.Product;
import Commands.Command;
import Daos.ProductDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The GetAllProductsToDeleteCommand retrieves a list of all products currently
 * on the system.
 *
 * @author Sergio
 */
public class GetAllProductsToDeleteCommand implements Command {

    /**
     * The execute method of the GetAllProductsToDeleteCommand simply retrieves
     * a HashMap of all products on the database and passes the on in an
     * attribute
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        // Seting the jsp page to forward to
        String forwardToJsp = "";
        // Creating the instance to reteave all the data from the database
        ProductDao instance = new ProductDao();
        // Setting an ArrayList with all the information retreaved
        HashMap<String,Product> lst = instance.getAllProducts();
        // Checking if the list is null
        
        // Getting the session
        HttpSession session = request.getSession();
        // Setting the list as an attibute in the session
        request.setAttribute("products", lst);
        // Setting the page where the servlet will forward the request
        forwardToJsp = "/deleteProduct.jsp";
        

        return forwardToJsp;

    }
    
}
