/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Product;

import Commands.Command;
import Daos.ProductDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The DeleteProductByNameCommand allows an Admin to delete products from the
 * database
 *
 * @author Sergio
 */
public class DeleteProductByNameCommand implements Command {

    
    /**
     * The execute method of the DeleteProductByNameCommand allows the admin to
     * delete a product from the database if if name is a match to the product
     * name supplied.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        // Getting the current session
        HttpSession session = request.getSession();
        // creating a new instance to access the database
        ProductDao instance = new ProductDao();
        
        String productName = null;
        // getting the parameters to search from the request
        productName = (String)request.getAttribute("product");
        
        if(productName != null)
        {
        // deleting the object from the database
        boolean deleted = instance.deleteProduct(productName);
        // setting a new arrayList with the products returned
        session.setAttribute("deleted", deleted);
        }
        
        // site where the information is processed
        forwardToJsp = "success.jsp";

        return forwardToJsp;
    }
}
