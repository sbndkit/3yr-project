/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Multipart;

import Business.Product;
import Commands.Command;
import Daos.ProductDao;
import java.io.File;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * The MultipartAddProductCommand our way of registering products with pictures
 * in our application
 *
 * @author Sergio
 */
public class MultipartAddProductCommand implements Command {

    /**
     * The execute method in the MultipartAddProductCommand uploads a product to
     * our database using the multipart form which is needed to process files to
     * the backend as a normal session request simply cannot read inputs from
     * type file in the frond end(client side). Multipart however is able to
     * read these and as such we are now capable of uploading a product photo
     * associated with the product and store it in our local image folder.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        String forwardToJsp = "error.jsp";

        final String UPLOAD_DIRECTORY = request.getServletContext().getRealPath("") + "images/phoneStore/";
        //UPLOAD_DIRECTORY = (java.lang.String) "/Users/Sergio/3yr-project/build/web/../../web/images/phoneStore/"

        //process only if its multipart content
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                String name = null;

                Product newProduct = new Product();

                List<FileItem> multiparts = new ServletFileUpload(
                        new DiskFileItemFactory()).parseRequest(request);
                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        name = new File(item.getName()).getName();
                        item.write(new File(UPLOAD_DIRECTORY + File.separator + name));
                        forwardToJsp = "test";
                    } else {
                        String fieldName = item.getFieldName();
                        String fieldValue = item.getString();

                        switch (fieldName) {
                            case "productName":
                                newProduct.setProductName(fieldValue);
                                break;
                            case "productBrand":
                                newProduct.setBrand(fieldValue);
                                break;
                            case "productDesc":
                                newProduct.setDesc(fieldValue);
                                break;
                            case "productPrice":
                                newProduct.setPrice(Double.parseDouble(fieldValue));
                                break;
                            case "productStock":
                                newProduct.setStock(Integer.parseInt(fieldValue));
                                break;
                            default:
                                break;
                        }
                    }

                }
                name = name.substring(0, name.indexOf('.'));
                newProduct.setPictureLink(name);

                ProductDao PDInstance = new ProductDao();

                if (PDInstance.registerProduct(newProduct)) {
                    forwardToJsp = "success.jsp";
                } else {
                    forwardToJsp = "error.jsp";
                }

                //File uploaded successfully
            } catch (Exception ex) {
                forwardToJsp = "error.jsp";
            }
        } else {
            forwardToJsp = "error.jsp";
        }
        return forwardToJsp;
    }
}
