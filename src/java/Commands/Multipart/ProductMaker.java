/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Multipart;

import Business.Product;
import com.oreilly.servlet.MultipartRequest;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Sergio
 */

public class ProductMaker {

    private Product product = new Product();

    public ProductMaker(HttpServletRequest request) {
        try {
            //Getting the product to be registered
            MultipartRequest mr = new MultipartRequest(request,(request.getServletContext().getRealPath("") + "../../web/images/phones"),256000);
            String [] nombres = mr.getParameterValues("txtNombres");
            String nombre = nombres[0];
            
            
            
            product.setProductName(nombre);
            product.setBrand(request.getParameter("productBrand"));
            product.setDesc(request.getParameter("desc"));
            product.setPrice(Double.parseDouble(request.getParameter("productPrice")));
            product.setStock(Integer.parseInt(request.getParameter("productStock")));
        } catch (IOException ex) {
            Logger.getLogger(ProductMaker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Product getProduct() {
        return product;
    }

}
