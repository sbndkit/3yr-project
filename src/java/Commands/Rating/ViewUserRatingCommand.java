/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Rating;

import Business.Rating;
import Commands.Command;
import Daos.RatingDao;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The ViewUserRatingCommand allows the viewing of all a users rating and
 * comments
 *
 * @author XPS
 */
public class ViewUserRatingCommand implements Command {

    /**
     * The execute method of the ViewUserRatingCommand allows a User/Admin to
     * view all ratings and comments associated with their/an account
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether a rating object is null or not.
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "error.jsp";

        RatingDao ratingDao = new RatingDao();

        ArrayList<Rating> userRatings = ratingDao.FindAllCommentsByUserId(Integer.parseInt(request.getParameter("userId")));

        // Checking for null value
        if (userRatings != null) {
            HttpSession session = request.getSession();
            session.setAttribute("userRatings", userRatings);
            forwardToJsp = "/userRatings.jsp";

        } else {

            forwardToJsp = "/noUserRatings.jsp";
        }

        return forwardToJsp;
    }

}
