/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Rating;

import Business.Rating;
import Commands.Command;
import Daos.RatingDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The AddRatingCommand allows the user to rates products in our application
 *
 * @author XPS
 */
public class AddRatingCommand implements Command {

    /**
     * The execute method of the AddRatingCommand allows the user to submit a
     * rating. However to insert a valid rating a rating and a comment must be
     * submitted through the request
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether a rating object is null or not.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "error.jsp";

        //Getting the user to be registered
        Rating rating = new Rating();

        if(!request.getParameter("comments").equals("") && request.getParameter("userId") != null && request.getParameter("productId") != null && request.getParameter("stars") != null)
        {
        rating.setUserId(Integer.parseInt(request.getParameter("userId")));
        rating.setProductId(Integer.parseInt(request.getParameter("productId")));
        rating.setComment(request.getParameter("comments"));
        rating.setRating(Float.parseFloat(request.getParameter("stars")));
        }
        
        // Checking for null value
        if (rating != null)
        {
            RatingDao instance = new RatingDao();
            if (instance.addRating(rating.getUserId(), rating.getProductId(), (int)rating.getRating(), rating.getComment()))
            {
                forwardToJsp = "/processRequest?action=getproductbyid&id="+rating.getProductId();
            }
            else
            {
                forwardToJsp = "/error.jsp";
            }
        }
        
        return forwardToJsp ;
    }
    
}
