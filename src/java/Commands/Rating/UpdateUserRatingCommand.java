/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Rating;

import Business.Rating;
import Commands.Command;
import Daos.RatingDao;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The UpdateUserRatingCommand allows users to update their ratings and comments
 * on products
 *
 * @author XPS
 */
public class UpdateUserRatingCommand implements Command {

    /**
     * The execute method of the UpdateUserRatingCommand allows a user alter
     * valid comments and ratings made by them
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether a rating object is null or not.
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "error.jsp";

        //Getting the user to be registered
        Rating rating = new Rating();

        if(!request.getParameter("comments").equals("") && request.getParameter("userId") != null && request.getParameter("productId") != null && request.getParameter("stars") != null)
        {
        rating.setUserId(Integer.parseInt(request.getParameter("userId")));
        rating.setProductId(Integer.parseInt(request.getParameter("productId")));
        rating.setComment(request.getParameter("comments"));
        rating.setRating(Float.parseFloat(request.getParameter("stars")));
        }
        
        // Checking for null value
        if (rating != null)
        {
            RatingDao instance = new RatingDao();
            if (instance.editRating(rating.getUserId(), rating.getProductId(), rating.getComment(), rating.getRating()))
            {
                RatingDao ratingDao = new RatingDao();
                ArrayList<Rating> userRatings = ratingDao.FindAllCommentsByUserId(Integer.parseInt(request.getParameter("userId")));
                
                HttpSession session = request.getSession();
                session.setAttribute("userRatings", userRatings);
                forwardToJsp = "/userRatings.jsp";
            }
            else
            {
                forwardToJsp = "/error.jsp";
            }
        }
        
        return forwardToJsp ;
    }
    
}
