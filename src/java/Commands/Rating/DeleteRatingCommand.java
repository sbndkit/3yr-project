/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Rating;

import Business.Rating;
import Commands.Command;
import Daos.RatingDao;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The DeleteRatingCommand allows a user/admin to delete ratings
 *
 * @author XPS
 */
public class DeleteRatingCommand implements Command {

    /**
     * The execute method of the DeleteRatingCommand allows a user to delete
     * comments based on two requirements. The first being that the user's id is
     * valid and the second being the the products rating that we wish to delete
     * has a valid product id also.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     * depending on whether a rating object is null or not.
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "error.jsp";

        //Getting the user to be registered
        Rating rating = new Rating();

        if(request.getParameter("userId") != null && request.getParameter("productId") != null )
        {
        rating.setUserId(Integer.parseInt(request.getParameter("userId")));
        rating.setProductId(Integer.parseInt(request.getParameter("productId")));
        }
        
        // Checking for null value
        if (rating != null)
        {
            RatingDao instance = new RatingDao();
            if (instance.deleteUserCommentOnProduct(rating.getUserId(), rating.getProductId()))
            {
                RatingDao ratingDao = new RatingDao();
                ArrayList<Rating> userRatings = ratingDao.FindAllCommentsByUserId(Integer.parseInt(request.getParameter("userId")));
                
                HttpSession session = request.getSession();
                session.setAttribute("userRatings", userRatings);
                forwardToJsp = "/userRatings.jsp";
            }
            else
            {
                forwardToJsp = "/error.jsp";
            }
        }
        
        return forwardToJsp ;
    }
    
}
