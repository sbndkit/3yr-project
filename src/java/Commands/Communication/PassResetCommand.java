/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Communication;

import Business.PasswordRecovery;
import Commands.Command;
import Daos.UserDao;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The PassResetCommand ensures that when a user has specified that they have
 * forgotten their password that when they have clicked the link sent to them in
 * the necessary time-frame. If they have they are then directed to the next
 * stage of changing their password
 *
 * @author Sergio
 */
public class PassResetCommand implements Command {

    /**
     * The execute method in the PassResetCommand is used to verify that a user
     * requesting to enter a new password has used the token sent to them in the
     * necessary time-frame. If a user has taken too long to use their token
     * they will be redirected to the forgotPassword page where they will need
     * to request another token otherwise they will progress the
     * passwordRecovery page where they may enter in a new password
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();

        String forwardToJSP = null;
        long al = -1;
        long bl = -1;
        long timePassed = -1;

        String id = request.getParameter("id");
        UserDao instance = new UserDao();

        PasswordRecovery item = instance.passwordRecoveryById(id);
        if (item != null) {
            Date a = new Date();
            Date b = item.getTimestamp();

            al = a.getTime();
            bl = b.getTime();

            timePassed = al - bl;
        }
        if (timePassed > 3600000) //5916151
        {
            forwardToJSP = "forgotPassword.jsp";
        } else {
            session.setAttribute("recoverItem", item);

            forwardToJSP = "passwordRecovery.jsp";
        }

        return forwardToJSP;
    }
}
