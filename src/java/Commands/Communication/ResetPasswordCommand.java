/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Communication;

import Business.PasswordRecovery;
import Business.User;
import Commands.Command;
import Daos.UserDao;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The ResetPasswordCommand is the final step of our applications way of
 * allowing a user to change their password. This stage is only reached if a
 * user entered a valid email into the PasswordRecoveryCommand and accessed the
 * token link in the required time-frame.
 *
 * @author Sergio
 */
public class ResetPasswordCommand implements Command {

    /**
     * The execute method in the ResetPasswordCommand allows a user to change
     * their password. This is done by creating a PasswordRecovery object with
     * the details in the attribute recoverItem.It is then passed to several
     * functions and once they are all successful the user is then directed to
     * the index page else-wise they are directed to the error page and must
     * start again.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        String forwardToJSP;

        PasswordRecovery item = (PasswordRecovery) session.getAttribute("recoverItem");

        UserDao instance = new UserDao();

        if (instance.passwordResetByEmail(item.getEmail(), getHash(request.getParameter("password").toCharArray()))) {
            if (instance.deletePasswordResetByEmail(item.getEmail())) {
                forwardToJSP = "index.jsp";
            } else {
                forwardToJSP = "error.jsp";
            }
        } else {
            forwardToJSP = "error.jsp";
        }

        return forwardToJSP;
    }

    /**
     * The getHash method is our applications primary method of encrypting a
     * user's password. This is done by using the Secure Hash Algorithm or
     * SHA-256, 256 being the size 256-bit
     *
     * @param pass The User's new password we wish to encrypt is passed to the
     * method as a Char array.
     * @return Once the password has been taken in it is converted from a char
     * array to a byte array it is then encrypted by our main hashing algorithm
     * when finally it is passed to the toHex method and then returned as a
     * String
     */
    private String getHash(char[] pass) {

        byte[] digest = null;
        byte[] buffer = charToByteArray(pass);

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.reset();
            messageDigest.update(buffer);
            digest = messageDigest.digest();

            return toHex(digest);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }

        return toHex(digest);
    }

    /**
     * charToByteArray is our method for preparing a hashed password which is a
     * char array to be sent to toHex which only accepts byte arrays
     *
     * @param pass This is the already hashed password the user has entered
     * @return Returns the users hashed password but as a Byte array instead of
     * a Char array and thus preparing it to be sent to the toHex method
     */
    private byte[] charToByteArray(char[] pass) {

        byte[] b = new byte[pass.length];

        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) pass[i];
        }

        return b;
    }

    /**
     * The toHex performs similarly to a salt method in that it adds extra
     * security to our ability to safely store a users password by adding an
     * extra layer of encryption to the passwords.
     *
     * @param digest This is the hashed password being passed to this method
     * from the toHash method after it has been converted to a byte array.
     * @return Returns a fully encrypted password which is represented as a
     * String with which we then store on the database in that users entry.
     */
    private String toHex(byte[] digest) {
        String hash = "";
        for (byte aux : digest) {
            int b = aux & 0xff;
            if (Integer.toHexString(b).length() == 1) {
                hash += "0";
            }
            hash += Integer.toHexString(b);
        }
        return hash;
    }
}
