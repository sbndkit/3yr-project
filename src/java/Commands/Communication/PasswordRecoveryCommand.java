/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Communication;

import Business.PasswordRecovery;
import Business.User;
import Commands.Command;
import Daos.UserDao;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The PasswordRecoveryCommand is a feature of our application which allows a
 * user to request permission to create a new password if they have forgotten
 * their old one.
 *
 * @author Sergio
 */
public class PasswordRecoveryCommand implements Command {

    /**
     * The execute method in the PasswordRecoveryCommand allows a user to safely
     * change their password based on a token system. The method is doing this
     * by way of sending an email to a user with the unique token to allow them
     * to change their password. The method is able to do this by using the
     * javax.mail package to send the email without actually being on a live
     * server.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        //Creating a result for getting status that messsage is delivered or not!

        // Get recipient's email-ID, message & subject-line from index.html page
        final String to = request.getParameter("username");
        final String subject = "Password recovery process | Phones R Us";

        // Sender's email ID and password needs to be mentioned
        final String from = "mobileshop.3yr@gmail.com";
        final String pass = "MobileShop-3yr";

        // Defining the gmail host
        String host = "smtp.gmail.com";

        // Creating Properties object
        Properties props = new Properties();

        // Defining properties
        props.put("mail.smtp.host", host);
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.user", from);
        props.put("mail.password", pass);
        props.put("mail.port", "465");

        // Authorized the Session object.
        Session mailSession = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, pass);
            }
        });

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(mailSession);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));

            // Set Subject: header field
            message.setSubject(subject);

            // Create the message part 
            BodyPart messageBodyPart = new MimeBodyPart();

            String toHash = (to + (new Date().getTime()));
            // Fill the message
            messageBodyPart.setText("Please click the link below to reset your password and if the link doesn't work, copy and paste it in your browser.\n\n"
                    + "http://localhost:8084/PhoneStoreProject/processRequest?action=passReset&id=" + getHash(toHash.toCharArray()));

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            UserDao instance = new UserDao();

            PasswordRecovery current = new PasswordRecovery();
            current.setTimestamp(new Timestamp(new Date().getTime()));
            current.setEmail(to);
            current.setId(getHash(toHash.toCharArray()));

            if (instance.registerNewpasswordRecovery(current)) {
                // Send message
                Transport.send(message);
            }

        } catch (MessagingException ex) {
            ex.printStackTrace();

        }
        return "passwordLisnkSent.jsp";
    }

    /**
     * The getHash method in the PasswordRecoveryCommand is used to make the
     * token unique and secure. This is done by using the Secure Hash Algorithm
     * or SHA-256, 256 being the size 256-bit
     *
     * @param pass The User's token we wish to encrypt is passed to the method
     * as a Char array.
     * @return Once the token has been taken in it is converted from a char
     * array to a byte array it is then encrypted by our main hashing algorithm
     * when finally it is passed to the toHex method and then returned as a
     * String
     */
    private String getHash(char[] pass) {

        byte[] digest = null;
        byte[] buffer = charToByteArray(pass);

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.reset();
            messageDigest.update(buffer);
            digest = messageDigest.digest();

            return toHex(digest);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }

        return toHex(digest);
    }

    /**
     * charToByteArray is our method for preparing a hashed token which is a
     * char array to be sent to toHex which only accepts byte arrays
     *
     * @param pass This is the already hashed token the user has entered
     * @return Returns the users hashed token but as a Byte array instead of
     * a Char array and thus preparing it to be sent to the toHex method
     */
    private byte[] charToByteArray(char[] pass) {

        byte[] b = new byte[pass.length];

        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) pass[i];
        }

        return b;
    }

    /**
     * The toHex performs similarly to a salt method in that it adds extra
     * security to our ability to safely store a users token by adding an extra
     * layer of encryption to it.
     *
     * @param digest This is the hashed token being passed to this method from
     * the toHash method after it has been converted to a byte array.
     * @return Returns a fully encrypted token which is represented as a String.
     */
    private String toHex(byte[] digest) {
        String hash = "";
        for (byte aux : digest) {
            int b = aux & 0xff;
            if (Integer.toHexString(b).length() == 1) {
                hash += "0";
            }
            hash += Integer.toHexString(b);
        }
        return hash;
    }
}
