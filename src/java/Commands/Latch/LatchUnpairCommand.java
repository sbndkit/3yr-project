/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Latch;

import Business.User;
import Commands.Command;
import Daos.LatchDao;
import Daos.UserDao;
import com.elevenpaths.latch.Latch;
import com.elevenpaths.latch.LatchResponse;
import com.elevenpaths.latch.LatchUser;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The LatchPairCommand allows a user to un-pair their account with their own
 * latch account
 *
 * @author Sergio
 */
public class LatchUnpairCommand implements Command {

    /**
     * The execute method in the LatchUnpairCommand allows a user to un-pair
     * their profile with their profile on the latch server. We allow the user
     * to do this by implementing the latch plug-in to do the "heavy lifting".
     * The heavy lifting being that of actually establishing a connection to the
     * latch servers. Once the connection is established we un-pair the
     * device/application from latch thus making it independent of latch
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;
        //If login successful, store the session id for this client...
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        LatchUser info = new LatchDao().getLatchInfo();

        Latch latch = new Latch(info.getAppId(), info.getSecretKey());
        LatchResponse lResponse = latch.unpair(u.getAccountId());

        if (lResponse != null) {
            if (lResponse.getData() == null) {
                new UserDao().updateUserAccountIdForLatch(u.getEmail(), "");
                u.setAccountId("");
                session.setAttribute("user", u);
            }

        }

        if (!u.getAccountId().isEmpty()) {
            session.setAttribute("view", "unpair");
        } else {
            session.setAttribute("view", "pair");
        }

        return "/latch.jsp";
    }
}
