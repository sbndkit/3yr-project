/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Order;

import Business.Order;
import Commands.Command;
import Daos.OrderDao;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The GetAllOrdersCommand retrieves all orders that have been placed on the
 * system
 *
 * @author Sergio
 */
public class GetAllOrdersCommand implements Command {

    /**
     * The execute method of the GetAllOrdersCommand is used by the Admins of
     * the application for viewing all the orders that have been placed and
     * stored in the database
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page with
     * an attribute containing a list of all orders placed on the system.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        // Seting the jsp page to forward to
        String forwardToJsp = "";
        // Creating the instance to reteave all the data from the database
        OrderDao instance = new OrderDao();
        // Setting an ArrayList with all the information retreaved
        ArrayList<Order> lst = instance.getAllOrders();
        // Getting the session
        HttpSession session = request.getSession();
        // Setting the list as an attibute in the session
        session.setAttribute("orderLst", lst);
        // Setting the page where the servlet will forward the request
        forwardToJsp = "orderList.jsp";


        return forwardToJsp;
    }
}
