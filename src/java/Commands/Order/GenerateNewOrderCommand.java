/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Order;

import Business.Address;
import Business.CartItem;
import Business.Order;
import Business.OrderProduct;
import Business.User;
import Commands.Command;
import Daos.AddressDao;
import Daos.OrderDao;
import Daos.OrderProductDao;
import Daos.UserDao;
import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Customer;
import com.braintreegateway.CustomerRequest;
import com.braintreegateway.Environment;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The GenerateNewOrderCommand allows the user to submit a new order to the
 * database
 *
 * @author Sergio
 */
public class GenerateNewOrderCommand implements Command {

    /**
     * The execute method in the GenerateNewOrderCommand allows a user to
     * generates an order from then if there are products in the cart the method
     * will then create an Order object with the information supplied by the
     * request and then check to see if the wallet credits functionality was
     * chosen. Once these are completed we create an OrderProduct object and
     * save both it and the Order object to the database
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     */
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        String forwardToJsp = "/cart.jsp";

        ArrayList<CartItem> cartList = (ArrayList<CartItem>) session.getAttribute("currentCart");

        Order currentOrder = new Order();
        OrderDao ODinstance = new OrderDao();
        OrderProductDao OPDinstance = new OrderProductDao();
        User user = (User) session.getAttribute("user");
        UserDao userDao = new UserDao();
        int addressID = -1;
        int userID = user.getUserId();

        String address = request.getParameter("addressSelection");
        if (address != null && address != "") {
            if (address.equalsIgnoreCase("newAddress")) {
                Address newAddress = new Address();
                AddressDao ADInstance = new AddressDao();
                newAddress.setAddress(request.getParameter("address"));
                newAddress.setCounty(request.getParameter("county"));
                newAddress.setCountry(request.getParameter("country"));
                newAddress.setUserID(userID);
                ADInstance.insertAddres(newAddress);
                addressID = ADInstance.selectLastIdAdded(userID);
            } else {
                addressID = Integer.parseInt(request.getParameter("addressSelection"));
            }
        }

        //Creating the Order
        int orderID = ODinstance.getLastOrderId() + 1;
        currentOrder.setUserId(userID);
        currentOrder.setDateOrdered(new Date());
        currentOrder.setOrderId(orderID);
        currentOrder.setAddressID(addressID);
        boolean inserted = false;

        String isChecked = request.getParameter("useWalletCredits");
        if (isChecked != null && isChecked != "") {
            currentOrder.setTotalPrice(0);
            userDao.updateUserWallet((int) ((user.getWalletCredits() - ((float) session.getAttribute("totalPrice")))), userID);
            session.setAttribute("checked", true);
        } else {
            currentOrder.setTotalPrice((float) session.getAttribute("totalPrice"));
            int creditsEarned = (user.getWalletCredits() + (int) ((float) session.getAttribute("totalPrice")) / 100);
            userDao.updateUserWallet(creditsEarned, userID);
            session.setAttribute("checked", false);

        }

        // getting the gateway
        BraintreeGateway gateway = (BraintreeGateway) session.getAttribute("gateway");

        //Creating a Braintree customer
        CustomerRequest CustomerRequest = new CustomerRequest()
                .firstName(user.getfName())
                .lastName(user.getlName())
                .email(user.getEmail())
                .phone(user.getPhone());

        Result<Customer> CustomerResult = gateway.customer().create(CustomerRequest);

        if (CustomerResult.isSuccess()) {
            Customer customer = CustomerResult.getTarget();

            String custID = customer.getId();

            // For production \\
            //String nonceFromTheClient = request.getParameter("nonce-from-the-client");
            String nonce = request.getParameter("nonce");
            
            TransactionRequest TRequest;
            if (nonce == null || nonce == "") {
                    TRequest = new TransactionRequest()
                        .amount(new BigDecimal(currentOrder.getTotalPrice()))
                        .customerId(custID)
                        .paymentMethodNonce("fake-valid-mastercard-nonce")
                        .customerId(custID)
                        .orderId("" + orderID)
                        .options()
                        .submitForSettlement(true)
                        .done();
            } else {
                    TRequest = new TransactionRequest()
                        .amount(new BigDecimal(currentOrder.getTotalPrice()))
                        .customerId(custID)
                        .paymentMethodNonce(nonce)
                        .customerId(custID)
                        .orderId("" + orderID)
                        .options()
                        .submitForSettlement(true)
                        .done();
            }

            Result<Transaction> TResult = gateway.transaction().sale(TRequest);

            String error = TResult.getMessage();
            boolean boo = TResult.isSuccess();

            if (TResult.isSuccess()) {

                if (ODinstance.insertOrder(currentOrder)) {
                    for (CartItem item : cartList) {
                        OrderProduct newProduct = new OrderProduct();
                        newProduct.setOrderId(orderID);
                        newProduct.setProductId(item.getProduct().getProductID());
                        newProduct.setQty(item.getQty());

                        inserted = OPDinstance.insertOrderProduct(newProduct);

                    }

                    if (inserted) {
                        if (isChecked != null && isChecked != "") {
                            String date = currentOrder.getDateOrdered().getDate() + "-" + currentOrder.getDateOrdered().getMonth() + "-" + currentOrder.getDateOrdered().getYear();
                            forwardToJsp = "processRequest?action=getOrderDetails&orderID=" + currentOrder.getOrderId() + "&date=" + date + "&totalPrice=" + (0.0);
                            ArrayList<Order> lst = (ArrayList<Order>) session.getAttribute("orderLst");
                            session.setAttribute("currentCart", null);
                        } else {
                            String date = currentOrder.getDateOrdered().getDate() + "-" + currentOrder.getDateOrdered().getMonth() + "-" + currentOrder.getDateOrdered().getYear();
                            forwardToJsp = "processRequest?action=getOrderDetails&orderID=" + currentOrder.getOrderId() + "&date=" + date + "&totalPrice=" + currentOrder.getTotalPrice();
                            ArrayList<Order> lst = (ArrayList<Order>) session.getAttribute("orderLst");
                            session.setAttribute("currentCart", null);
                        }
                    }
                } else {
                    forwardToJsp = "/cart.jsp";
                }
            }
        }
        return forwardToJsp;

    }

}
