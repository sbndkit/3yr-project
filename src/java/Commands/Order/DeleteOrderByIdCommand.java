/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Order;

import Business.Order;
import Commands.Order.*;
import Commands.Command;
import Daos.OrderDao;
import Daos.OrderProductDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The DeleteOrderByIdCommand gives the applications admin the ability to delete
 * order entries from the database and application
 *
 * @author Sergio
 */
public class DeleteOrderByIdCommand implements Command {

    /**
     * The execute method of the DeleteOrderByIdCommand allows the Admin to
     * safely delete order entries by ensuring the order exists firstly then it
     * passes then it goes to the deleteOrderProductById with the id of the
     * order to finally delete the order.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        int orderID = -1;
        // getting the parameters to search from the request
        try {
            orderID = Integer.parseInt(request.getParameter("orderId"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        OrderProductDao instance = new OrderProductDao();

        instance.deleteOrderProductById(orderID);
        
        forwardToJsp = "/success.jsp";

        return forwardToJsp;
    }
}
