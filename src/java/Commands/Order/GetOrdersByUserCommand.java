package Commands.Order;

import Business.Order;
import Business.User;
import Commands.Command;
import Daos.OrderDao;
import Daos.UserDao;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The GetOrdersByUserCommand is used to display to a user all the orders they
 * have made in our application.
 *
 * @author Sergio
 */
public class GetOrdersByUserCommand implements Command {

    /**
     * The execute method of the GetOrdersByUserCommand allows a user to
     * retrieve all orders in relation to their profile in the application
     * whereas an admin retrieves the orders of all users in the system.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page along
     * with two attributes those being userOrders and allUsers if they
     * are on admin or only userOrders if they are an ordinary user
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        // Seting the jsp page to forward to
        String forwardToJsp = "";
        // getting the current session
        HttpSession session = request.getSession();
        // Getting the user stored into the session
        User user = (User) session.getAttribute("user");
        OrderDao ODInstance = new OrderDao();
        
        
        
        if(!user.getIsIsAdmin()){
            //If user is not an admin, it creates an ArrayList with all the orders for this user
            ArrayList<Order> userOrders = ODInstance.getAllOrdersByUserId(user.getUserId());
            
            session.setAttribute("userOrders", userOrders);
        }else{
            ArrayList<Order> userOrders = ODInstance.getAllOrders();
            UserDao UInstance = new UserDao();
            ArrayList<User> allUsers = new ArrayList();

            for (Order item : userOrders)
            {
                allUsers.add(UInstance.getUserById(item.getUserId()));
            }
            session.setAttribute("userOrders", userOrders);
            session.setAttribute("allUsers", allUsers);
        }
        forwardToJsp = request.getParameter("forwardToJsp");
                
        if (forwardToJsp == null){
            forwardToJsp="orderList.jsp";
        }        


        return forwardToJsp;
    }
}


