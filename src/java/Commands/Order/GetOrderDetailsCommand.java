/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands.Order;

import Business.Order;
import Business.Product;
import Commands.Command;
import Daos.OrderDao;
import Daos.ProductDao;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The GetOrderDetailsCommand allows a user or admin to see the details of an
 * order
 *
 * @author Sergio
 */
public class GetOrderDetailsCommand implements Command {

    /**
     * The execute method of the GetOrderDetailsCommand allows a user to view
     * all the details of one of their orders or in the case of an admin the
     * details of an order for example: What products were ordered, how many,
     * cost per product and then total cost.
     *
     * @param request Gives access to cookies, headers and any parameters
     * supplied by the client.
     * @param response Provides a Writer object with ability to write data out
     * to the client (i.e. create HTML content within the servlet)
     * @return We return a string which is a link to a particular jsp page along
     * with several attributes containing a list of Products and the desired
     * order.
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        
        // getting the current session
        HttpSession session = request.getSession();
        // Getting the OrderID to search the products
        int orderId = Integer.parseInt(request.getParameter("orderID"));
        // Getting the order
        Order currentOrder =  new OrderDao().getOrderById(orderId);
        // Creating instance to be able to search the products
        ProductDao instance = new ProductDao();
        // Searching all the products related with an specific order
        ArrayList<Product> productList = instance.getAllProductsByOrderId(orderId);
        // Storing this information into the session object
        session.setAttribute("productList", productList);
        session.setAttribute("currentOrder", currentOrder);
        // returning the page to forward
        return  "/orderDetails.jsp";
    }
}
