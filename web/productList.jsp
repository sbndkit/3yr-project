<%-- 
    Document   : suppliers
    Created on : Dec 12, 2015, 10:37:34 AM
    Author     : Megatronus
--%>

<%@page import="Business.CartItem"%>
<%@page import="Business.Product"%>
<%@page import="Business.User"%>
<%@page import="Business.Supplier"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head><!--Links calling on the different bootstrap packages and css file -->
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <title>PHONES R US</title>

        <!-- favicons are otherwise known as shortcut icons -->
        <link rel="shortcut icon" href="http://teamtreehouse.com/assets/favicon.ico">
        <link rel="icon" href="http://teamtreehouse.com/assets/favicon.ico">

        <!-- These three lines call on the three css files stored in the css folder of the project -->
        <link rel="stylesheet" type="text/css" media="all" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="all" href="css/bootstrap-responsive.min.css">
        <link rel="stylesheet" type="text/css" media="all" href="css/global.css">

        <!-- This imports the jquery file that eases our use of javascript, the min jquery file is used as it removes all unnecessary characters-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
        <script type="text/javascript" language="javascript" charset="utf-8" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" language="javascript" charset="utf-8" src="js/cookie.js"></script>
        <!-- Start of LiveChat (www.livechatinc.com) code -->
        <script type="text/javascript">
            window.__lc = window.__lc || {};
            window.__lc.license = 7433351;
            (function () {
                var lc = document.createElement('script');
                lc.type = 'text/javascript';
                lc.async = true;
                lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(lc, s);
            })();
        </script>
        <!-- End of LiveChat code -->
    </head>
    <body>
        <script>
            var cookie = getCookie("rememberMe");
            document.getElementById("username").value = cookie;
        </script>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <nav id="navigation">
            <div class="container">
                <ul id="navbar" class="navlinks">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="processRequest?action=getAllProducts">Phone Store</a></li>
                    <li><a href="about.jsp">About us</a></li>
                    <li><a href="contacts.jsp">Contacts</a></li>
                    <li style="float: right;list-style-type: none">
                        <%
                            User user = (User) session.getAttribute("user");

                            if (user == null || user.getIsIsAdmin() == false) {
                                response.sendRedirect("error.jsp");
                            }

                            ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

                            if (currentCart == null) {
                                currentCart = new ArrayList();
                                session.setAttribute("currentCart", currentCart);
                            }

                            if (session.getAttribute("loggedSessionId") != null && session.getAttribute("user") != null) {

                                out.println("<a href='processRequest?action=logout'> Log out</a><a href='profile.jsp'>\tHello " + user.getfName() + "</a>");
                            } else {
                        %>


                        <a href="#myPopupPhoneList" data-rel="popup" onclick="fill()">Login or Register</a>
                        <div data-role="popup" id="myPopupPhoneList" style="min-width:250px;">
                            <form class="login-form" form method="post" action="processRequest" id="login" onsubmit="updateCookie()">
                                <script>
                                    function fill()
                                    {
                                        var cookie = getCookie("rememberMe");

                                        $('#rememberMe').prop('checked', 'checked');
                                        $('#username').val(cookie);

                                    }
                                </script>
                                <div class="header">
                                    <h1>Login Form</h1>
                                    <span>Fill out the form below to login.</span>
                                </div>

                                <div class="content">
                                    <input name="username" type="email" class="input username" placeholder="Email" required id="username"/>
                                    <input name="password" type="password" class="input password" placeholder="Password" required pattern="[A-Za-z]{6,}" title="Please insert 6 digit or more" id="password" />
                                    <label for="rememberMe">
                                        <input type="checkbox" name="rememberMe" id="rememberMe" value="1">Remember me</label>
                                </div>

                                <div class="footer1">
                                    <input type="submit" name="action" value="Login" class="button"/>
                                    <input type="hidden" name="forwardToJsp" value="productList.jsp">
                                    <a href = "register.jsp"><input type="submit" name="submit" value="Register" class="register" /></a>
                                    <br><br>
                                    <br>
                                    <a href="forgotPassword.jsp" style="text-align: center">Password recovery</a>
                                </div>
                            </form>
                        </div>

                        <%
                            }
                        %>
                    </li>

                    <%
                        if (user != null && user.getIsIsAdmin() == true) {
                    %>

                    <li><a href="adminController.jsp">Admin Panel</a></li>
                        <%
                        } else if (user != null && user.getIsIsAdmin() != true) {
                        %>
                    <li><a href="processRequest?action=getAllOrders">Orders</a></li>

                    <%    }
                    %>
                    <li><a href="cart.jsp">Cart</a></li>
                    <li><a href="legal.jsp">Legal</a></li>

                </ul>
            </div>
        </nav>
        <br>
        <br>
        <br>
        <div id="main-content">
            <div class="container" id="productListContainer">
                <div class="row">
                    <br>
                    <%
                        ArrayList<Product> supplierList = (ArrayList<Product>) request.getAttribute("productList");
                        if (supplierList != null)
                            for (int i = 0; i < supplierList.size(); i++) {
                                Product p = supplierList.get(i);
                                if (i % 3 == 0) {
                    %>
                </div>
                <div class="row">
                    <%
                        }
                    %>

                    <div class ="span4">
                        <a href="processRequest?action=GetProductById&id=<%= p.getProductID()%>" class="thumbnail" target="user">
                            <div>
                                <br><p><%
                                    int id = p.getProductID();
                                    out.println("<font color='black'>ID: </font>" + id);
                                    %></p>
                                <br>
                                <p><%
                                    String name = p.getProductName();
                                    out.println("<font color='black'>Name: </font>" + name);
                                    %></p>
                                <br>
                                <p><%
                                    String desc = p.getDesc();
                                    if (desc.length() > 80) {
                                        desc = desc.substring(0, 80) + "...";
                                    }
                                    out.println("<font color='black'>description: </font>" + desc);
                                    %></p>
                            </div>
                        </a>
                        <br>
                    </div>

                    <%
                        }
                    %>
                </div>
                <div>
                    <a href="adminController.jsp">Back to the control panel</a>
                </div>
            </div><!-- @end .container -->
        </div><!-- @end #main-content -->
        <div id="footer">
            <ul id="List">
                <li><p>Please feel free to <a href="contacts.jsp">Email</a> us</p></li>
                <li><p><a href="http://latch.elevenpaths.com" target="_blank"><img src="img/latch_logo.png" width="150" alt="http://latch.elevenpaths.com" ></a></p></li>
                <li><p id="twitterLink">
                        <small>Follow us on twitter: 
                            <a href="<a href=https://twitter.com/NiallMulready" 
                               class="twitter-follow-button" 
                               data-show-count="false" data-size="large" 
                               data-show-screen-name="false">Follow @NiallMulready</a>
                            <script>!function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0],
                                            p = /^http:/.test(d.location) ? 'http' : 'https';
                                    if (!d.getElementById(id))
                                    {
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = p + '://platform.twitter.com/widgets.js';
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }
                                }
                                (document, 'script', 'twitter-wjs');
                            </script>
                        </small>
                    </p></li>
                <li><p>Please have a look at our <a href="FAQ.jsp"><b>F.A.Q</b></a> if you have any questions<p></li>
                <li></li>
                <li class="fb-like" data-href="https://www.facebook.com/Phones-R-US-118666461853804/?skip_nax_wizard=true" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></li>
                <li><input id="narratorButton" type="button" name="action" value="narratorOn" class="button" onclick="narrator()"/></li>
            </ul>
            <script>
                if (getCookie("narratorStatus") === "narratorOn") {

                    document.getElementById("narratorButton").value = "narratorOff";
                    var script = document.createElement('script');
                    script.src = 'js/reader.js';
                    script.type = 'text/javascript';
                    var head = document.getElementsByTagName("head")[0];
                    document.getElementById("productListContainer").addEventListener("mouseover", function (e) {
                        if (e.target && e.target.nodeName === "P") {
                            speak(e.target.textContent);
                        }
                    });
                }

            </script>
        </div>
    </body>
</html>
