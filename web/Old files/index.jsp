<%--
    Document   : index
    Created on : Dec 8, 2015, 2:58:05 PM
    Author     : sergio
testing
--%>


<%@page import="Business.CartItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Business.User"%>
<%@page import="Service.UserService"%>
<!doctype html>
<html lang="en-US">
    <head><!--Links calling on the different bootstrap packages and css file ***hello sergio**** -->
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <title>PHONES R US</title>

        <!-- favicons are otherwise known as shortcut icons -->
        <link rel="shortcut icon" href="http://teamtreehouse.com/assets/favicon.ico">
        <link rel="icon" href="http://teamtreehouse.com/assets/favicon.ico">

        <!-- These three lines call on the three css files stored in the css folder of the project -->
        <link rel="stylesheet" type="text/css" media="all" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="all" href="css/bootstrap-responsive.min.css">
        <link rel="stylesheet" type="text/css" media="all" href="css/global.css">

        <!-- This imports the jquery file that eases our use of javascript, the min jquery file is used as it removes all unnecessary characters-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
        <script type="text/javascript" language="javascript" charset="utf-8" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" language="javascript" charset="utf-8" src="js/cookie.js"></script>

        <!-- Start of LiveChat (www.livechatinc.com) code -->
        <script type="text/javascript">
            window.__lc = window.__lc || {};
            window.__lc.license = 7433351;
            (function () {
                var lc = document.createElement('script');
                lc.type = 'text/javascript';
                lc.async = true;
                lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(lc, s);
            })();
        </script>
        <!-- End of LiveChat code -->
    </head>
    <body>
        <script>
            var cookie = getCookie("rememberMe");
            document.getElementById("username").value = cookie;
        </script>
        <div id="fb-root"></div>
        <!--        <script>(function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id))
                            return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                </script>-->
        <nav id="navigation">
            <div class="container">
                <ul id="navbar" class="navlinks">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="processRequest?action=getAllProducts">Phone Store</a></li>
                    <li><a href="about.jsp">About us</a></li>
                    <li><a href="contacts.jsp">Contacts</a></li>
                    <li style="float: right;list-style-type: none">
                        <%
                            User user = (User) session.getAttribute("user");

                            ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

                            if (currentCart == null) {
                                currentCart = new ArrayList();
                                session.setAttribute("currentCart", currentCart);
                            }

                            if (session.getAttribute("loggedSessionId") != null && session.getAttribute("user") != null) {

                                out.println("<a href='processRequest?action=logout'> Log out</a><a href='profile.jsp'>\tHello " + user.getfName() + "</a>");
                            } else {
                        %>


                        <a href="#myPopupIndex" data-rel="popup" onclick="fill()">Login or Register</a>
                        <div data-role="popup" id="myPopupIndex" style="min-width:250px;">
                            <form class="login-form" form method="post" action="processRequest" id="login" onsubmit="updateCookie()">
                                <script>
                                    function fill()
                                    {
                                        var cookie = getCookie("rememberMe");

                                        $('#rememberMe').prop('checked', 'checked');
                                        $('#username').val(cookie);

                                    }
                                </script>
                                <div class="header">
                                    <h1>Login Form</h1>
                                    <span>Fill out the form below to login.</span>
                                </div>

                                <div class="content">
                                    <input name="username" type="email" class="input username" placeholder="Email" required id="username"/>
                                    <input name="password" type="password" class="input password" placeholder="Password" required pattern="[A-Za-z]{6,}" title="Please insert 6 digit or more" id="password" />
                                    <label for="rememberMe">
                                        <input name="forwardToJsp" type="hidden" value="index.jsp"/>
                                        <input type="checkbox" name="rememberMe" id="rememberMe" value="1">Remember me</label>
                                </div>

                                <div class="footer1">
                                    <input type="submit" name="action" value="Login" class="button"/>
                                    <a href = "register.jsp"><input type="submit" name="submit" value="Register" class="register" /></a>

                                    <div style="float: right; margin-top: 30px">
                                        <a href="forgotPassword.jsp" style="text-align: center">Password recovery</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <%
                            }
                        %>
                    </li>

                    <%
                        if (user != null && user.getIsIsAdmin() == true) {
                    %>

                    <li><a href="adminController.jsp">Admin Panel</a></li>
                        <%
                        } else if (user != null && user.getIsIsAdmin() != true) {
                        %>
                    <li><a href="processRequest?action=getAllOrders">Orders</a></li>

                    <%    }
                    %>
                    <li><a href="cart.jsp">Cart</a></li>
                    <li><a href="legal.jsp">Legal</a></li>

                </ul>
            </div>
        </nav>
        <header id="heading">
            <div class="container text-center">
                <br>
                <h1 style="color: #DCDCDC">PHONES R US</h1>

                <p><a href="processRequest?action=getAllProducts" class="btn btn-large"><strong>SHOP NOW</strong></a></p>
            </div>
        </header>
        <div id="main-content">
            <div class="container" id="indexContainer">
                <div class="row">
                    <div class="span4">
                        <h2>The Team</h2>
                        <p>Say Hi to Niall, Sergio or Brian and ask questions about our products simply by emailing us.</p>
                        <br>
                        <p class="text-center"><a class="btn" href="about.jsp">Read more</a></p>
                    </div>

                    <div class="span4">
                        <h2>Our Products</h2>
                        <p>We supply a wide range of phones ranging from iPhones to Android.
                            We pride ourselves on high quality merchandise and support.</p>
                        <br>
                        <p class="text-center">
                            <a class="btn" href="contacts.jsp">
                                Contact us</a></p>
                    </div>
                    <div class="span4">
                        <h2>Networks</h2>
                        <p>We currently are in dealership with Three mobile who supply many brilliant plans for mobiles of all makes as well as supplying modems</p>
                        <br>
                        <p class="text-center"><a class="btn" href="http://www.three.ie/">Take me there</a></p>
                    </div>
                </div><!-- @end .row -->
                <form action="processRequest" method="post">
                    <div class="row-fluid">
                        <ul class="thumbnails">
                            <li class="span4">
                                <a href="processRequest?action=applyfilters&Apple" class="thumbnail" target="_parent" name="Apple">
                                    <img src="images/phones/appleBrand.jpg" alt="Apple Phones" id="image">
                                </a>
                            </li>
                            <li class="span4">
                                <a href="processRequest?action=applyfilters&HTC" class="thumbnail" target="_parent" name="htc">
                                    <img src="images/phones/htcBrand.jpg" alt="HTC Phones" id="image">
                                </a>
                            </li>
                            <li class="span4">
                                <a href="processRequest?action=applyfilters&Microsoft" class="thumbnail" target="_parent" name="microsoft">
                                    <img src="images/phones/microsoftBrand.jpg" alt="Microsoft Phones" id="image">
                                </a>
                            </li>
                        </ul>
                    </div><!-- @end .row-fluid -->

                    <div class="row-fluid">
                        <ul class="thumbnails">

                            <li class="span4">
                                <a href="processRequest?action=applyfilters&Samsung" class="thumbnail" target="_parent" name = "samsung">
                                    <img src="images/phones/samsungBrand.png" alt="Samsung Phones" id="image">
                                </a>
                            </li>
                            <li class="span4">
                                <a href="processRequest?action=applyfilters&Sony" class="thumbnail" target="_parent" name="sony">
                                    <img src="images/phones/sonyBrand.jpg" alt="Sony Phones" id="image">
                                </a>
                            </li>
                            <li class="span4">
                                <a href="processRequest?action=getAllProducts" class="thumbnail" target="_parent" name="other">
                                    <img src="images/phones/lucky.jpg" alt="Other brands" id="image">
                                </a>
                            </li>
                        </ul>
                    </div><!-- @end .row-fluid -->
                </form>
            </div><!-- @end .container -->
        </div><!-- @end #main-content -->
        <div id="footer">
            <ul id="List">
                <li><p>Please feel free to <a href="contacts.jsp">Email</a> us</p></li>
                <li><p><a href="http://latch.elevenpaths.com" target="_blank"><img src="img/latch_logo.png" width="150" alt="http://latch.elevenpaths.com" ></a></p></li>
                <li><p id="twitterLink">
                        <small>Follow us on twitter: 
                            <a href="<a href=https://twitter.com/NiallMulready" 
                               class="twitter-follow-button" 
                               data-show-count="false" data-size="large" 
                               data-show-screen-name="false">Follow @NiallMulready</a>
                            <script>!function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0],
                                            p = /^http:/.test(d.location) ? 'http' : 'https';
                                    if (!d.getElementById(id))
                                    {
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = p + '://platform.twitter.com/widgets.js';
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }
                                }
                                (document, 'script', 'twitter-wjs');
                            </script>
                        </small>
                    </p></li>
                <li><p>Please have a look at our <a href="FAQ.jsp"><b>F.A.Q</b></a> if you have any questions<p></li>
                <li></li>
                <li class="fb-like" data-href="https://www.facebook.com/Phones-R-US-118666461853804/?skip_nax_wizard=true" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></li>
                <li><input id="narratorButton" type="button" name="action" value="narratorOn" class="button" onclick="narrator()"/></li>
            </ul>
            <script>
                if (getCookie("narratorStatus") === "narratorOn") {


                    document.getElementById("narratorButton").value = "narratorOff";

                    var script = document.createElement('script');
                    script.src = 'js/reader.js';
                    script.type = 'text/javascript';
                    var head = document.getElementsByTagName("head")[0];
                    head.appendChild(script);

                    document.getElementById("indexContainer").addEventListener("mouseover", function (e) {
                        if (e.target && e.target.nodeName === "H2")
                        {
                            speak(e.target.textContent);
                        } else if (e.target && e.target.nodeName === "P")
                        {
                            speak(e.target.textContent);
                        } else if (e.target && e.target.nodeName === "IMG") {
                            speak(e.target.alt);
                        }
                    });
                }
            </script>
        </div>
    </body>
</html>
