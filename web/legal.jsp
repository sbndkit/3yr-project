<%@page import="Business.Order"%>
<%@page import="Business.Product"%>
<%@page import="Business.CartItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Business.User"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
    <head>
        <title>Phones R Us</title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Classic Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- //for-mobile-apps -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- js -->
        <script src="js/jquery.min.js"></script>
        <!-- //js -->
        <!-- cart -->
        <script src="js/simpleCart.min.js"></script>
        <!-- cart -->
        <!-- for bootstrap working -->
        <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
        <!-- //for bootstrap working -->
        <!-- animation-effect -->
        <link href="css/animate.min.css" rel="stylesheet"> 
        <script src="js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <!-- //animation-effect -->
        <link href='//fonts.googleapis.com/css?family=Cabin:400,500,600,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <!-- header -->
        <div class="header">
            <div class="header-grid">
                <div class="container">

                    <div class="header-right animated wow fadeInRight" data-wow-delay=".5s">
                        <div class="header-right1 ">
                            <ul>
                                <%
                                    session.setAttribute("forwardToJsp", "index.jsp");

                                    User user = (User) session.getAttribute("user");

                                    ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

                                    if (currentCart == null) {
                                        currentCart = new ArrayList();
                                        session.setAttribute("currentCart", currentCart);
                                    }

                                    if (session.getAttribute("loggedSessionId") != null && session.getAttribute("user") != null) {

                                        out.println("<a href='processRequest?action=logout'> Log out</a><a href='profile.jsp'>\tHello " + user.getfName() + "</a>");
                                    } else {
                                %>
                                <li><i class="glyphicon glyphicon-log-in" ></i><a href="login.jsp">Login</a></li>
                                <li><i class="glyphicon glyphicon-book" ></i><a href="register.jsp">Register</a></li>
                                        <%  }
                                        %>

                            </ul>
                        </div>
                        <div class="header-right2">
                            <h3> <div class="total">
                                    <a href="cart.jsp" class="simpleCart_empty"><img src="images/cart.png" alt="" />&nbsp;&nbsp;&nbsp;Cart</a></div>

                            </h3>
                            <div class="clearfix"> </div>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="container">
                <div class="logo-nav">

                    <nav class="navbar navbar-default">
                        <div class="navbar-header nav_2">

                            <div class="navbar-brand logo-nav-left wow fadeInLeft animated" data-wow-delay=".5s">
                                <h1 class="animated wow pulse" data-wow-delay=".5s"><a href="index.jsp">Phones<span>R</span>Us</a></h1>
                            </div>

                        </div>
                        <div class="navbar-header nav_2">
                            <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                        </div>
                        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul class="nav navbar-nav">
                                <li><a href="index.jsp">Home</a></li>
                                <li><a href="processRequest?action=getAllProducts">Phone Store</a></li>
                                    <%
                                        if (user != null && user.getIsIsAdmin() == true) {
                                    %>

                                <li><a href="adminController.jsp">Admin Panel</a></li>
                                    <%
                                    } else if (user != null && user.getIsIsAdmin() != true) {
                                    %>
                                <li><a href="processRequest?action=getAllOrders">Orders</a></li>

                                <%    }
                                %>
                                <li><a href="about.jsp">About us</a></li>
                                <li><a href="contacts.jsp">Contacts</a></li>
                                <li><a href="legal.jsp">Legal</a></li>
                                <li><a href="FAQ.jsp">FAQs</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </div>
        <!-- //header -->
        <!--banner-->
        <div class="banner-top">
            <div class="container">
                <h2 class="animated wow fadeInLeft" data-wow-delay=".5s">Legal Information</h2>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div id="main-content">
            <div class="container" id="legalContainer">
                <div class="row">
                    <br><br>
                    <H4>
                        <b>These terms and conditions govern your use of this website; by using this website,
                            you accept these terms and conditions in full.
                        </b>
                    </H4>
                    <p>
                        <b>If you disagree with these terms
                            and conditions or any part of these terms and conditions, you must not use this
                            website.
                        </b>
                    </p>
                    <br>
                    <p>
                        <b>This website uses cookies.By using this
                            website and agreeing to these terms and conditions, 
                            you consent to our use of cookies in accordance with the terms of PhoneRUs's
                        </b>
                    </p>
                    <br>
                    <p>
                        <b>All user account's cannot be removed until approved by an administrator,
                            if you wish to close your account please contact us on the contact page
                            of PhoneRUs's website.
                        </b>
                    </p>
                    <br>
                    <h4><b>License to use website</b></h4>

                    <p>
                        Unless otherwise stated, PhoneRUs and/or its licensors own
                        the intellectual property rights in the website and material on the
                        website.Subject to the license
                        below, all these intellectual property rights are reserved.

                    </p>
                    <br>

                    <p>
                        You may view, download for caching purposes only, and print pages from the website
                        for your own personal use, subject to the restrictions set out below and
                        elsewhere in these terms and conditions.

                    </p>

                    <H4><b>You must not:</b>
                        republish material from this website (including
                        republication on another website)</H4>

                    <p>sell, rent or sub-license material from the website;</p>

                    <p>show any material from the website in public;</p>

                    <p>reproduce, duplicate, copy or otherwise exploit
                        material on this website for a commercial purpose;</p>

                    <p>edit or otherwise modify any material on the website;</p>
                    <br>

                    <p><b>Acceptable use</b></p>
                    <br>
                    <p>You must not use this website in any way that causes, or may cause, damage to the
                        website or impairment of the availability or accessibility of the website; or
                        in any way which is unlawful, illegal, fraudulent or harmful, or in connection
                        with any unlawful, illegal, fraudulent or harmful purpose or activity.
                    </p>
                    <br>
                    <p>You must not use this website to copy, store, host, transmit, send, use, publish or
                        distribute any material which consists of (or is linked to) any spyware,
                        computer virus, Trojan horse, worm, keystroke logger, rootkit or other
                        malicious computer software.
                    </p>
                    <br>
                    <p>You must not conduct any systematic or automated data collection activities
                        (including without limitation scraping, data mining, data extraction and data
                        harvesting) on or in relation to this website without <span class=SpellE>PhoneRUs</span>
                        express written consent.
                    </p>
                    <br>
                    <p>You must not use this website to transmit or send unsolicited commercial communications.</p>
                    <br>
                    <p>You must not use this website for any purposes related to marketing without PhoneRUs express written consent.</p>
                    <br>
                    <p><b>Restricted access</b></p>
                    <br>
                    <p>Access to certain areas of this website is restricted.</p>
                    <p>PhoneRUs reserves the right to restrict access to other areas of this website, or indeed
                        this entire website, at PhoneRUs discretion.
                    </p>
                    <br>
                    <p>If PhoneRUs provides you with a user ID and password to enable
                        you to access restricted areas of this website or other content or services,
                        you must ensure that the user ID and password are kept confidential.</p>
                    <br>
                    <p>
                        PhoneRUs may disable your user ID and password in PhoneRUs
                        sole discretion without notice or explanation.
                    </p>
                    <br>
                    <p><b>Limitations of liability</b></p>
                    <br>
                    <p>PhoneRUs will not be liable to you (whether under the law of contact, the law of torts
                        or otherwise) in relation to the contents of, or use of, or otherwise in
                        connection with, this website:
                    </p>
                    <br>
                    <p>to the extent that the website is provided free-of-charge,
                        for any direct loss;
                    </p>

                    <p>for any indirect, special or consequential loss; or</p>

                    <p>for any business losses, loss of revenue, income,
                        profits or anticipated savings, loss of contracts or business relationships,
                        loss of reputation or goodwill, or loss or corruption of information or data.
                    </p>
                    <br>
                    <p>These limitations of liability apply even if PhoneRUs has been expressly advised of the potential loss.</p>
                    <br>
                    <p><b>Exceptions</b></p>
                    <br>
                    <p>Nothing in this website disclaimer will exclude or limit any warranty implied by law
                        that it would be unlawful to exclude or limit; and nothing in this website
                        disclaimer will exclude or limit PhoneRUs liability
                        in respect of any:
                    </p>
                    <br>
                    <p>fraud or fraudulent misrepresentation on the part of PhoneRUs; or</p>

                    <p>matter which it would be illegal or unlawful for PhoneRUs to exclude or limit, or to attempt or purport to
                        exclude or limit, its liability.
                    </p>
                    <br>
                    <p>Reasonableness</p>
                    <br>
                    <p>
                        By using this website, you agree that the exclusions
                        and limitations of liability set out in this website disclaimer are
                        reasonable.
                    </p>
                    <br>
                    <p>
                        If you do not think they are reasonable, you must not
                        use this website.
                    </p>
                    <br>
                    <p>Other parties</p>
                    <br>
                    <p>
                        You accept that, as a limited liability entity, PhoneRUs has an interest in limiting the personal liability
                        of its officers and employees.You
                        agree that you will not bring any claim personally against PhoneRUs
                        officers or employees in respect of any losses you suffer in connection with
                        the website.
                    </p>
                    <br>
                    <p>
                        Without prejudice to the foregoing paragraph, you agree that the limitations of
                        warranties and liability set out in this website disclaimer will protect PhoneRUs officers, employees, agents, subsidiaries,
                        successors, assigns and sub-contractors as well as PhoneRUs.
                    </p>
                    <br>
                    <p><b>Unenforceable provisions</b></p>
                    <br>
                    <p>
                        If any provision of this website disclaimer is, or is found to be, unenforceable
                        under applicable law, that will not affect the enforceability of the other
                        provisions of this website disclaimer.
                    </p>
                    <br>
                    <p><b>Indemnity</b></p>
                    <br>
                    <p>
                        You hereby indemnify PhoneRUs and undertake to keep PhoneRUs indemnified against any losses, damages, costs,
                        liabilities and expenses (including without limitation legal expenses and any
                        amounts paid by PhoneRUs to a third party in
                        settlement of a claim or dispute on the advice of PhoneRUs
                        legal advisers) incurred or suffered by PhoneRUs
                        arising out of any breach by you of any provision of these terms and conditions,
                        or arising out of any claim that you have breached any provision of these terms
                        and conditions.
                    </p>
                    <br>
                    <p><b>Breaches of these terms and conditions</b></p>
                    <br>
                    <p>
                        Withoutvprejudice toPhoneRUs other rights under these terms
                        and conditions, if you breach these terms and conditions in any way, PhoneRUs
                        may take such action as PhoneRUs
                        deems appropriate to deal with the breach, including suspending your access to
                        the website, prohibiting you from accessing the website, blocking computers
                        using your IP address from accessing the website, contacting your internet
                        service provider to request that they block your access to the website and/or
                        bringing court proceedings against you.
                    </p>
                    <br>
                    <p><b>Variation</b></p>
                    <br>
                    <p>
                        PhoneRUs may revise these terms and conditions from time-to-time.
                        Revised terms and conditions will apply to the use of this website from the date of the publication of the revised
                        terms and conditions on this website.Please check this page regularly to ensure you are familiar with the
                        current version.
                    </p>
                    <br>
                    <p><b>Assignment</b></p>
                    <br>
                    <p>
                        PhoneRUs may transfer, sub-contract or otherwise deal with PhoneRUs
                        rights and/or obligations under these terms and conditions without notifying
                        you or obtaining your consent.
                    </p>
                    <br>
                    <p>
                        You may not transfer, sub-contract or otherwise deal
                        with your rights and/or obligations under these terms and conditions.
                    </p>
                    <br>
                    <p><b>Severability</b></p>
                    <br>
                    <p>
                        If a provision of these terms and conditions is determined by any court or other
                        competent authority to be unlawful and/or unenforceable, the other provisions
                        will continue in effect.If any
                        unlawful and/or unenforceable provision would be lawful or enforceable if part
                        of it were deleted, that part will be deemed to be deleted, and the rest of the
                        provision will continue in effect.
                    </p>
                    <br>
                    <p><b>Entire agreement</b></p>
                    <br>
                    <p>
                        These terms and conditions constitute the entire agreement between you and PhoneRUs in relation to your use of this website, and
                        supersede all previous agreements in respect of your use of this website.
                    </p>
                    <br>
                    <p><b>Law and jurisdiction</b></p>
                    <br>
                    <p>
                        These terms and conditions will be governed by and construed in accordance with Irish
                        law, and any disputes relating to these terms and conditions will be subject to
                        the exclusive jurisdiction of the courts of Irish law.
                    </p>
                    <br>
                    <p><b>Registrations and authorisations</b></p>
                    <br>
                    <p>
                        PhoneRUs is registered with Ireland registration office. You can find the online version of the
                        register at <a href="http://www.registration.ie">www.registration.ie</a> PhoneRUs
                        registration number is 1234567
                    </p>
                    <br>
                    <p>
                        PhoneRUs registration number is 1234567.
                    </p>
                    <br>
                    <p><b>PhoneRUs details</b></p>
                    <br>
                    <p>
                        The full name of PhoneRUs is PhoneRUs.
                    </p>
                    <br>
                    <p>You can contact PhonesRUs by sending a email to admin@phonesrus</p>
                    <br>
                </div>
            </div><!-- @end .container -->
        </div><!-- @end #main-content -->
        <!-- footer --><div class="footer">
            <div class="container">
                <div class="footer-top">
                    <div class="col-md-9 footer-top1">
                        <a href="http://latch.elevenpaths.com" target="_blank"><img src="img/latch_logo.png" width="150" alt="http://latch.elevenpaths.com" ></a>
                    </div>
                    <div class="col-md-3 footer-top2">
                        <a href="contacts.jsp">Contact Us</a>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="footer-grids">
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".5s">
                        <h3>About Us</h3>
                        <p>We are a small company especialised in the last and more advance mobile technology. <span>Click here to know our team.</span> <a href="about.jsp">The team</a></p>
                    </div>
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".6s">
                        <h3>Contact Info</h3>
                        <ul>
                            <li><i class="glyphicon glyphicon-map-marker" ></i>1234k Avenue, 4th block, <span>New York City.</span></li>
                            <li class="foot-mid"><i class="glyphicon glyphicon-envelope" ></i><a href="mailto:info@example.com">info@example.com</a></li>
                            <li><i class="glyphicon glyphicon-earphone" ></i>+1234 567 567</li>
                        </ul>
                    </div>
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".7s">
                        <h3>Sign up for newsletter </h3>
                        <form>
                            <input type="text" placeholder="Email"  required="">
                            <input type="submit" value="Submit">
                        </form>

                    </div>

                    <div class="clearfix"> </div>
                </div>

                <div class="copy-right animated wow fadeInUp" data-wow-delay=".5s">
                    <p>&copy 2016 Classic Style. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
                </div>
            </div>
        </div>
        <!-- //footer -->
    </body>
</html>