<%@page import="Daos.UserDao"%>
<%@page import="Business.Rating"%>
<%@page import="Daos.RatingDao"%>
<%@page import="Business.Product"%>
<%@page import="Business.CartItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Business.User"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
    <head>
        <title>Phones R Us</title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Classic Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- //for-mobile-apps -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- js -->
        <script src="js/jquery.min.js"></script>
        <!-- //js -->
        <!-- cart -->
        <script src="js/simpleCart.min.js"></script>
        <!-- cart -->
        <!-- for bootstrap working -->
        <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
        <!-- //for bootstrap working -->
        <!-- animation-effect -->
        <link href="css/animate.min.css" rel="stylesheet"> 
        <script src="js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <!-- //animation-effect -->
        <link href='//fonts.googleapis.com/css?family=Cabin:400,500,600,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <!-- header -->
        <div class="header">
            <div class="header-grid">
                <div class="container">

                    <div class="header-right animated wow fadeInRight" data-wow-delay=".5s">
                        <div class="header-right1 ">
                            <ul>
                                <%
                                    session.setAttribute("forwardToJsp", "index.jsp");

                                    User user = (User) session.getAttribute("user");

                                    ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

                                    if (currentCart == null) {
                                        currentCart = new ArrayList();
                                        session.setAttribute("currentCart", currentCart);
                                    }

                                    if (session.getAttribute("loggedSessionId") != null && session.getAttribute("user") != null) {

                                        out.println("<a href='processRequest?action=logout'> Log out</a><a href='profile.jsp'>\tHello " + user.getfName() + "</a>");
                                    } else {
                                %>
                                <li><i class="glyphicon glyphicon-log-in" ></i><a href="login.jsp">Login</a></li>
                                <li><i class="glyphicon glyphicon-book" ></i><a href="register.jsp">Register</a></li>
                                        <%  }
                                        %>

                            </ul>
                        </div>
                        <div class="header-right2">
                            <h3> <div class="total">
                                    <a href="cart.jsp" class="simpleCart_empty"><img src="images/cart.png" alt="" />&nbsp;&nbsp;&nbsp;Cart</a></div>

                            </h3>
                            <div class="clearfix"> </div>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="container">
                <div class="logo-nav">

                    <nav class="navbar navbar-default">
                        <div class="navbar-header nav_2">
                            <div class="navbar-brand logo-nav-left wow fadeInLeft animated" data-wow-delay=".5s">
                                <h1 class="animated wow pulse" data-wow-delay=".5s"><a href="index.jsp">Phones<span>R</span>Us</a></h1>
                            </div>

                        </div> 
                        <div class="navbar-header nav_2">
                            <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                        </div>
                        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul class="nav navbar-nav">
                                <li><a href="index.jsp">Home</a></li>
                                <li><a href="processRequest?action=getAllProducts">Phone Store</a></li>
                                    <%
                                        if (user != null && user.getIsIsAdmin() == true) {
                                    %>

                                <li><a href="adminController.jsp">Admin Panel</a></li>
                                    <%
                                    } else if (user != null && user.getIsIsAdmin() != true) {
                                    %>
                                <li><a href="processRequest?action=getAllOrders">Orders</a></li>

                                <%    }
                                %>
                                <li><a href="about.jsp">About us</a></li>
                                <li><a href="contacts.jsp">Contacts</a></li>
                                <li><a href="legal.jsp">Legal</a></li>
                                <li><a href="FAQ.jsp">FAQs</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </div>
        <!-- //header -->
        <!--banner-->
        <div class="banner-top">
            <div class="container">
                <h2 class="animated wow fadeInLeft" data-wow-delay=".5s">Phone</h2>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!--content-->
        <div class="product">
            <div class="container">

                <%
                    user = (User) session.getAttribute("user");
                    Product p = (Product) request.getAttribute("searchByName");
                    RatingDao rDao = new RatingDao();
                    ArrayList<Rating> comments = rDao.FindAllCommentsByProductId(p.getProductID());
                    float rating = rDao.FindTotalRating(p.getProductID());
                    UserDao userDao = new UserDao();
                %>


                <form action="processRequest" method="post" id="hiddenInfo">

                    <input type="hidden" value="<%=p.getProductID()%>" name="id"> 
                    <input type="hidden" value="<%=p.getProductName()%>" name="name">
                    <input type="hidden" value="<%=p.getPrice()%>" name="price">
                    <input type="hidden" value="<%=p.getDesc()%>" name="desc">
                    <input type="hidden" value="<%=p.getStock()%>" name="stock">
                    <input type="hidden" name="action" value="registerNewOrderProduct"/>
                </form>
                <div class="col-md-9 animated wow fadeInRight" data-wow-delay=".5s">
                    <div class="col-md-5 grid-im">		
                        <div class="flexslider">
                            <ul class="slides">
                                <li data-thumb="images/si.jpg">
                                    <div class="thumb-image"> <img src="images/phoneStore/<%=p.getPictureLink()%>.jpg" data-imagezoom="true" class="img-responsive"> </div>
                                </li>

                            </ul>
                        </div>
                    </div>	
                    <div class="col-md-7 single-top-in">
                        <div class="span_2_of_a1 simpleCart_shelfItem">
                            <h3><%=p.getProductName()%></h3>
                            <p class="in-para"> Here you can find the details for the selected item.</p>
                            <div class="price_single">
                                <span class="reducedfrom item_price">$<%=p.getPrice()%></span>


                                <div class="col-md-4 footer-grid" style="width: 100%; margin-top: 10px">
                                    <select class="form-control " name="quantity" title="quantity" style="width: 98%" form="hiddenInfo">
                                        <%
                                            for (int i = 0; i < p.getStock(); i++) {
                                                out.println("<option value=" + (i + 1) + ">" + (i + 1) + "</option>");
                                            }
                                        %>
                                    </select>
                                    <br>
                                    <input type="submit" name="save" value="Add To Cart" class="button"  form="hiddenInfo" style="width: 100%"/>
                                </div>
                                <div class="clearfix"></div>
                            </div>



                            <div class="clearfix"> </div>
                        </div>
                        <!----- tabs-box ---->
                        <div class="sap_tabs">	
                            <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                                <ul class="resp-tabs-list">
                                    <li class="resp-tab-item " aria-controls="tab_item-0" role="tab" style="width: 50%"><span>Product Description</span></li>
                                    <li class="resp-tab-item" aria-controls="tab_item-2" role="tab" style="width: 50%"><span>Reviews</span></li>
                                    <div class="clearfix"></div>
                                </ul>				  	 
                                <div class="resp-tabs-container">
                                    <h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span>Product Description</h2><div class="tab-1 resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
                                        <div class="facts">
                                            <p > <%=p.getDesc()%> </p>

                                        </div>

                                    </div>

                                    <h2 class="resp-accordion" role="tab" aria-controls="tab_item-2"><span class="resp-arrow"></span>Reviews</h2><div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
                                        <%
                                            for (Rating item : comments) {
                                        %>
                                        <div class="comments-top-top">

                                            <div class="top-comment-right">
                                                <p><a href="#"><%=item.getComment()%></a><p>

                                            </div>
                                            <div class="clearfix"> </div>
                                            <%  }
                                            %>

                                        </div>
                                        <%
                                            if (user != null) {
                                        %>
                                        <a class="add-re" data-toggle="modal" data-target="#myModal" />Add a review</a>

                                        <%
                                            }
                                        %>


                                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title" id="myModalLabel">Select your shipping address</h4>
                                                    </div>
                                                    <div class="panel-body">

                                                    </div>
                                                    <div class="panel-footer">
                                                        <a href="#cancel"><input type="submit" class="btn btn-default" value="Cancel" data-dismiss="modal" /></a>
                                                        <input type="submit" name="action" value="Use this address" class="btn btn-primary" form="submitForm" />

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
                            <script type="text/javascript">
            $(document).ready(function () {
                $('#horizontalTab').easyResponsiveTabs({
                    type: 'default', //Types: default, vertical, accordion           
                    width: 'auto', //auto or any width like 600px
                    fit: true   // 100% fit in a container
                });
            });
                            </script>	
                            <!---->
                        </div>

                    </div>
                    <!----->
                    <div class="clearfix"> </div>

                </div class="clearfix"></div>
        </div>			
    </div>


    <!-- footer -->
    <div class="footer">
        <div class="container">
            <div class="footer-top">
                <div class="col-md-9 footer-top1">
                    <a href="http://latch.elevenpaths.com" target="_blank"><img src="img/latch_logo.png" width="150" alt="http://latch.elevenpaths.com" ></a>
                </div>
                <div class="col-md-3 footer-top2">
                    <a href="contacts.jsp">Contact Us</a>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="footer-grids">
                <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".5s">
                    <h3>About Us</h3>
                    <p>We are a small company especialised in the last and more advance mobile technology. <span>Click here to know our team.</span> <a href="about.jsp">The team</a></p>
                </div>
                <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".6s">
                    <h3>Contact Info</h3>
                    <ul>
                        <li><i class="glyphicon glyphicon-map-marker" ></i>1234k Avenue, 4th block, <span>New York City.</span></li>
                        <li class="foot-mid"><i class="glyphicon glyphicon-envelope" ></i><a href="mailto:info@example.com">info@example.com</a></li>
                        <li><i class="glyphicon glyphicon-earphone" ></i>+1234 567 567</li>
                    </ul>
                </div>
                <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".7s">
                    <h3>Sign up for newsletter </h3>
                    <form>
                        <input type="text" placeholder="Email"  required="">
                        <input type="submit" value="Submit">
                    </form>

                </div>

                <div class="clearfix"> </div>
            </div>

            <div class="copy-right animated wow fadeInUp" data-wow-delay=".5s">
                <p>&copy 2016 Classic Style. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
            </div>
        </div>
    </div>
    <!-- //footer -->
</body>
</html>