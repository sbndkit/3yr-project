
<%@page import="Daos.ProductDao"%>
<%@page import="Daos.ProductDao"%>
<%@page import="Business.Rating"%>
<%@page import="Business.CartItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Business.User"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
    <head>
        <title>Phones R Us</title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Classic Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- //for-mobile-apps -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- js -->
        <script src="js/jquery.min.js"></script>
        <!-- //js -->
        <!-- cart -->
        <script src="js/simpleCart.min.js"></script>
        <!-- cart -->
        <!-- for bootstrap working -->
        <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
        <!-- //for bootstrap working -->
        <!-- animation-effect -->
        <link href="css/animate.min.css" rel="stylesheet"> 
        <script src="js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <!-- //animation-effect -->
        <link href='//fonts.googleapis.com/css?family=Cabin:400,500,600,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <!-- header -->
        <div class="header">
            <div class="header-grid">
                <div class="container">

                    <div class="header-right animated wow fadeInRight" data-wow-delay=".5s">
                        <div class="header-right1 ">
                            <ul>
                                <%
                                    session.setAttribute("forwardToJsp", "index.jsp");

                                    User user = (User) session.getAttribute("user");

                                    ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

                                    if (currentCart == null) {
                                        currentCart = new ArrayList();
                                        session.setAttribute("currentCart", currentCart);
                                    }

                                    if (session.getAttribute("loggedSessionId") != null && session.getAttribute("user") != null) {

                                        out.println("<a href='processRequest?action=logout'> Log out</a><a href='profile.jsp'>\tHello " + user.getfName() + "</a>");
                                    } else {
                                %>
                                <li><i class="glyphicon glyphicon-log-in" ></i><a href="login.jsp">Login</a></li>
                                <li><i class="glyphicon glyphicon-book" ></i><a href="register.jsp">Register</a></li>
                                        <%  }
                                        %>

                            </ul>
                        </div>
                        <div class="header-right2">
                            <h3> <div class="total">
                                    <a href="cart.jsp" class="simpleCart_empty"><img src="images/cart.png" alt="" />&nbsp;&nbsp;&nbsp;Cart</a></div>

                            </h3>
                            <div class="clearfix"> </div>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="container">
                <div class="logo-nav">

                    <nav class="navbar navbar-default">
                        <div class="navbar-header nav_2">

                            <div class="navbar-brand logo-nav-left wow fadeInLeft animated" data-wow-delay=".5s">
                                <h1 class="animated wow pulse" data-wow-delay=".5s"><a href="index.jsp">Phones<span>R</span>Us</a></h1>
                            </div>
                            <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                        </div>  
                        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul class="nav navbar-nav">
                                <li><a href="index.jsp">Home</a></li>
                                <li><a href="processRequest?action=getAllProducts">Phone Store</a></li>
                                    <%
                                        if (user != null && user.getIsIsAdmin() == true) {
                                    %>

                                <li><a href="adminController.jsp">Admin Panel</a></li>
                                    <%
                                    } else if (user != null && user.getIsIsAdmin() != true) {
                                    %>
                                <li><a href="processRequest?action=getAllOrders">Orders</a></li>

                                <%    }
                                %>
                                <li><a href="about.jsp">About us</a></li>
                                <li><a href="contacts.jsp">Contacts</a></li>
                                <li><a href="legal.jsp">Legal</a></li>
                                <li><a href="FAQ.jsp">FAQs</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </div>
        <!-- //header -->
        <!--banner-->
        <div class="banner-top">
            <div class="container">
                <h2 class="animated wow fadeInLeft" data-wow-delay=".5s">New user</h2>
                <div class="clearfix"> </div>
            </div>
        </div>

        <!-- content -->
        <div class="login" style="margin-bottom: 5%">
            <div class="container">

                <form class="center-block " method="post" action="processRequest" style="width: 50%">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h1>My ratings</h1>
                            <span><p>Ratings left about my purchases<p></span>
                        </div>
                        <%
                            ArrayList<Rating> userRatings = (ArrayList<Rating>) session.getAttribute("userRatings");
                            ProductDao productDao = new ProductDao();
                            if (userRatings.size() > 0) {
                                for (Rating r : userRatings) {
                        %>

                        <div class="panel-body">
                            <%
                                out.println("<p><b>Product: </b>" + productDao.getProduct(r.getProductId()).getProductName() + "</p>");

                                out.println("<p><b>Comment: </b></br>" + r.getComment() + "</p>");

                                out.println("<p><b>Star Rating: " + r.getRating() + "</b></br></p>");
                            %>

                        </div>
                        <%
                                }
                            }else{
out.println("<div class='panel-body'><p><b>You don't have any comment</b></p></div>");
}
                        %>


                        <div class="panel-footer" style="text-align: right">
                            <% if (user.getIsIsAdmin()) {
                            %>
                            <a href = "processRequest?action=getuserbyid&user=<%=user.getUserId()%>" class="btn btn-primary">Back</a>
                                <%} else {%>
                            <a href = "index.jsp" class="btn btn-primary"/>Back</a>
                                <%}%>
                        </div>
                    </div>


                </form>


            </div>
        </div>

        <!-- footer -->
        <div class="footer">
            <div class="container">
                <div class="footer-top">
                    <div class="col-md-9 footer-top1">
                        <a href="http://latch.elevenpaths.com" target="_blank"><img src="img/latch_logo.png" width="150" alt="http://latch.elevenpaths.com" ></a>
                    </div>
                    <div class="col-md-3 footer-top2">
                        <a href="contacts.jsp">Contact Us</a>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="footer-grids">
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".5s">
                        <h3>About Us</h3>
                        <p>We are a small company especialised in the last and more advance mobile technology. <span>Click here to know our team.</span> <a href="about.jsp">The team</a></p>
                    </div>
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".6s">
                        <h3>Contact Info</h3>
                        <ul>
                            <li><i class="glyphicon glyphicon-map-marker" ></i>1234k Avenue, 4th block, <span>New York City.</span></li>
                            <li class="foot-mid"><i class="glyphicon glyphicon-envelope" ></i><a href="mailto:info@example.com">info@example.com</a></li>
                            <li><i class="glyphicon glyphicon-earphone" ></i>+1234 567 567</li>
                        </ul>
                    </div>
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".7s">
                        <h3>Sign up for newsletter </h3>
                        <form>
                            <input type="text" placeholder="Email"  required="">
                            <input type="submit" value="Submit">
                        </form>

                    </div>

                    <div class="clearfix"> </div>
                </div>

                <div class="copy-right animated wow fadeInUp" data-wow-delay=".5s">
                    <p>&copy 2016 Classic Style. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
                </div>
            </div>
        </div>
        <!-- //footer -->
    </body>
</html>