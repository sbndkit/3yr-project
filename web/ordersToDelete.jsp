<%-- 
    Document   : deleteProduct
    Created on : 13-Dec-2015, 16:04:43
    Author     : BrianMcM
--%>

<%@page import="Business.CartItem"%>
<%@page import="Business.Order"%>
<%@page import="Business.User"%>
<%@page import="Business.Supplier"%>
<%@page import="java.util.ArrayList"%>
<%@page import="javax.websocket.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
    <head>
        <title>Phones R Us</title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Classic Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- //for-mobile-apps -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- js -->
        <script src="js/jquery.min.js"></script>
        <!-- //js -->
        <!-- cart -->
        <script src="js/simpleCart.min.js"></script>
        <!-- cart -->
        <!-- for bootstrap working -->
        <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
        <!-- //for bootstrap working -->
        <!-- animation-effect -->
        <link href="css/animate.min.css" rel="stylesheet"> 
        <script src="js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <!-- //animation-effect -->
        <link href='//fonts.googleapis.com/css?family=Cabin:400,500,600,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <!-- header -->
        <div class="header">
            <div class="header-grid">
                <div class="container">

                    <div class="header-right animated wow fadeInRight" data-wow-delay=".5s">
                        <div class="header-right1 ">
                            <ul>
                                <%
                                    session.setAttribute("forwardToJsp", "index.jsp");

                                    User user = (User) session.getAttribute("user");

                                    ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

                                    if (currentCart == null) {
                                        currentCart = new ArrayList();
                                        session.setAttribute("currentCart", currentCart);
                                    }

                                    if (session.getAttribute("loggedSessionId") != null && session.getAttribute("user") != null) {

                                        out.println("<a href='processRequest?action=logout'> Log out</a><a href='profile.jsp'>\tHello " + user.getfName() + "</a>");
                                    } else {
                                %>
                                <li><i class="glyphicon glyphicon-log-in" ></i><a href="login.jsp">Login</a></li>
                                <li><i class="glyphicon glyphicon-book" ></i><a href="register.jsp">Register</a></li>
                                        <%  }
                                        %>

                            </ul>
                        </div>
                        <div class="header-right2">
                            <h3> <div class="total">
                                    <a href="cart.jsp" class="simpleCart_empty"><img src="images/cart.png" alt="" />&nbsp;&nbsp;&nbsp;Cart</a></div>

                            </h3>
                            <div class="clearfix"> </div>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="container">
                <div class="logo-nav">

                    <nav class="navbar navbar-default">
                        <div class="navbar-header nav_2">

                            <div class="navbar-brand logo-nav-left wow fadeInLeft animated" data-wow-delay=".5s">
                                <h1 class="animated wow pulse" data-wow-delay=".5s"><a href="index.jsp">Phones<span>R</span>Us</a></h1>
                            </div>

                        </div> 
                        <div class="navbar-header nav_2">
                            <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                        </div>
                        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul class="nav navbar-nav">
                                <li><a href="index.jsp">Home</a></li>
                                <li><a href="processRequest?action=getAllProducts">Phone Store</a></li>
                                    <%
                                        if (user != null && user.getIsIsAdmin() == true) {
                                    %>

                                <li><a href="adminController.jsp">Admin Panel</a></li>
                                    <%
                                    } else if (user != null && user.getIsIsAdmin() != true) {
                                    %>
                                <li><a href="processRequest?action=getAllOrders">Orders</a></li>

                                <%    }
                                %>
                                <li><a href="about.jsp">About us</a></li>
                                <li><a href="contacts.jsp">Contacts</a></li>
                                <li><a href="legal.jsp">Legal</a></li>
                                <li><a href="FAQ.jsp">FAQs</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </div>
        <!-- //header -->
        <!--banner-->
        <div class="banner-top">
            <div class="container">
                <h2 class="animated wow fadeInLeft" data-wow-delay=".5s">User List</h2>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="panel panel-primary" style="width: 60%; margin-left: 20%; margin-top: 5%; margin-bottom: 5%">


            <div class="panel-heading">
                <h1>Order history</h1>
                <span>Click on any order to see the details</span>
            </div>

            <div class="panel-body ">
                <table width="100%" >
                    <tr>

                        <td id = 1 style="width: 25%; text-align: center; font-size: 20px; color: #777">
                            Order ID
                        </td>
                        <td id = 1 style="width: 25%; text-align: center; font-size: 20px; color: #777">
                            User
                        </td>
                        <td id = 1 style="width: 25%; text-align: center; font-size: 20px; color: #777">
                            Date of Order
                        </td>
                        <td id = 1 style="width: 25%; text-align: center; font-size: 20px; color: #777">
                            Total price
                        </td>
                    </tr>
                </table>

                <%                            
                    ArrayList<Order> userOrders = (ArrayList<Order>) session.getAttribute("userOrders");
                    ArrayList<User> allUsers = (ArrayList<User>) session.getAttribute("allUsers");
                    if (userOrders != null) {
                        for (int i = 0; i < userOrders.size(); i++) {
                            Order item = userOrders.get(i);
                            User nUser = allUsers.get(i);
                            String date = item.getDateOrdered().getDate() + "-" + item.getDateOrdered().getMonth() + "-" + (1900 + item.getDateOrdered().getYear());
                %>
                
                <a data-toggle="modal" data-target="#dialog-userID<%=item.getOrderId()%>" class="thumbnail" style="margin-top: 20px;" id="list">
                    <table width="100%">
                        <tr>

                            <td id = 1 style="width: 25%; text-align: center;">
                                <%=item.getOrderId()%>
                            </td>
                            <td id = 2 style="width: 25%; text-align: center;">
                                <%=nUser.getfName() + " " + nUser.getlName()%>
                            </td>

                            <td id = 3 style="width: 25%; text-align: center;">
                                <%
                                    out.print(date);
                                %>
                            </td>
                            <td id = 4 style="width: 25%; text-align: right; ">

                                € <%=item.getTotalPrice()%>
                            </td>

                        </tr>
                    </table>
                </a>

                <div class="modal fade" id="dialog-userID<%=item.getOrderId()%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="width: 50%; margin: 20% 0 0 25%; text-align: center ">
                    <div class="panel panel-danger ">
                        <div class="panel-heading ">Are you sure you want to delete the order?</div>
                        <div class="panel-body ">Clicking yes the order <%=item.getOrderId()%> for the user <%=nUser.getfName() + " " + nUser.getlName()%> will be deleted</div>

                        <!-- Buttons, both options close the window in this demo -->
                        <div class="panel-footer"  style="text-align: center ">
                            <a href="processRequest?action=DeleteOrderById&orderId=<%=item.getOrderId()%>" class="btn btn-success">Yes, delete it!</a>
                            <a href="#cancel" style="margin-left: 20% "><input type="submit" class="btn btn-danger" value="No, don't do it!" data-dismiss="modal" /></a>

                        </div>
                    </div>	
                </div>


                <%
                    }
                %>
            </div>                    
            <%
                } else {
                    out.print("<a>You don't have any order.</a>");
                }
            %>



            <div class="panel-footer" style="text-align: right ">

                <a href = "adminController.jsp" class="btn btn-primary">Back</a>

            </div>
        </div>
        <br>
        <br>
    </div>

</div>
<div id="footer">
    <ul id="List">
        <li><p>Please feel free to <a href="contacts.jsp">Email</a> us</p></li>
        <li><p><a href="http://latch.elevenpaths.com" target="_blank"><img src="img/latch_logo.png" width="150" alt="http://latch.elevenpaths.com" ></a></p></li>
        <li><p id="twitterLink">
                <small>Follow us on twitter: 
                    <a href="<a href=https://twitter.com/NiallMulready" 
                       class="twitter-follow-button" 
                       data-show-count="false" data-size="large" 
                       data-show-screen-name="false">Follow @NiallMulready</a>
                    <script>!function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0],
                                    p = /^http:/.test(d.location) ? 'http' : 'https';
                            if (!d.getElementById(id))
                            {
                                js = d.createElement(s);
                                js.id = id;
                                js.src = p + '://platform.twitter.com/widgets.js';
                                fjs.parentNode.insertBefore(js, fjs);
                            }
                        }
                        (document, 'script', 'twitter-wjs');
                    </script>
                </small>
            </p></li>
        <li><p>Please have a look at our <a href="FAQ.jsp"><b>F.A.Q</b></a> if you have any questions<p></li>
        <li></li>
        <li class="fb-like" data-href="https://www.facebook.com/Phones-R-US-118666461853804/?skip_nax_wizard=true" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></li>
    </ul>
</div>
</body>
</html>
