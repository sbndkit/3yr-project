<%@page import="java.util.TreeSet"%>
<%@page import="Business.Product"%>
<%@page import="java.util.Arrays"%>
<%@page import="Business.HashMap"%>
<%@page import="Business.CartItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Business.User"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
    <head>
        <title>Phones R Us</title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Classic Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- //for-mobile-apps -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- js -->
        <script src="js/jquery.min.js"></script>
        <!-- //js -->
        <!-- cart -->
        <script src="js/simpleCart.min.js"></script>
        <!-- cart -->
        <!-- for bootstrap working -->
        <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
        <!-- //for bootstrap working -->
        <!-- animation-effect -->
        <link href="css/animate.min.css" rel="stylesheet"> 
        <script src="js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <!-- //animation-effect -->
        <link href='//fonts.googleapis.com/css?family=Cabin:400,500,600,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <!-- header -->
        <div class="header">
            <div class="header-grid">
                <div class="container">

                    <div class="header-right animated wow fadeInRight" data-wow-delay=".5s">
                        <div class="header-right1 ">
                            <ul>
                                <%
                                    session.setAttribute("forwardToJsp", "processRequest?action=getAllProducts");

                                    User user = (User) session.getAttribute("user");

                                    ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

                                    if (currentCart == null) {
                                        currentCart = new ArrayList();
                                        session.setAttribute("currentCart", currentCart);
                                    }

                                    if (session.getAttribute("loggedSessionId") != null && session.getAttribute("user") != null) {

                                        out.println("<a href='processRequest?action=logout'> Log out</a><a href='profile.jsp'>\tHello " + user.getfName() + "</a>");
                                    } else {
                                %>
                                <li><i class="glyphicon glyphicon-log-in" ></i><a href="login.jsp">Login</a></li>
                                <li><i class="glyphicon glyphicon-book" ></i><a href="register.jsp">Register</a></li>
                                        <%  }
                                        %>

                            </ul>
                        </div>
                        <div class="header-right2">
                            <h3> <div class="total">
                                    <a href="cart.jsp" class="simpleCart_empty"><img src="images/cart.png" alt="" />&nbsp;&nbsp;&nbsp;Cart</a></div>

                            </h3>
                            <div class="clearfix"> </div>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="container">
                <div class="logo-nav">

                    <nav class="navbar navbar-default">
                        <div class="navbar-header nav_2">

                            <div class="navbar-header nav_2">
                                <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>

                            </div>
                            <div class="navbar-brand logo-nav-left wow fadeInLeft animated" data-wow-delay=".5s">
                                <h1 class="animated wow pulse" data-wow-delay=".5s"><a href="index.jsp">Phones<span>R</span>Us</a></h1>
                            </div>

                        </div>  
                        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul class="nav navbar-nav">
                                <li><a href="index.jsp">Home</a></li>
                                <li><a href="processRequest?action=getAllProducts">Phone Store</a></li>
                                    <%
                                        if (user != null && user.getIsIsAdmin() == true) {
                                    %>

                                <li><a href="adminController.jsp">Admin Panel</a></li>
                                    <%
                                    } else if (user != null && user.getIsIsAdmin() != true) {
                                    %>
                                <li><a href="processRequest?action=getAllOrders">Orders</a></li>

                                <%    }
                                %>
                                <li><a href="about.jsp">About us</a></li>
                                <li><a href="contacts.jsp">Contacts</a></li>
                                <li><a href="legal.jsp">Legal</a></li>
                                <li><a href="FAQ.jsp">FAQs</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </div>
        <!-- //header -->
        <!--banner-->
        <div class="banner-top">
            <div class="container">
                <h2 class="animated wow fadeInLeft" data-wow-delay=".5s">Products</h2>

                <div class="clearfix"> </div>
            </div>
        </div>
        <!--content-->
        <div class="product">
            <div class="container">
                <div class="col-md-3 product-bottom">
                    <!--categories-->
                    <div class="categories animated wow fadeInUp animated" data-wow-delay=".5s" >
                        <h3>Categories</h3>

                        <form method="post" action="processRequest">
                            <ul class="cate">
                                <li><i class="glyphicon glyphicon-menu-right" ></i>Brands</li>
                                    <%
                                        HashMap myMap = (HashMap) session.getAttribute("products");
                                        boolean[] rangesToDisplay = (boolean[]) request.getAttribute("rangesFilter");
                                        boolean displayAll = false;

                                        if (rangesToDisplay == null) {
                                            rangesToDisplay = new boolean[4];
                                            Arrays.fill(rangesToDisplay, true);
                                            displayAll = true;
                                        }
                                        // Array with the brands for the filter checkbox
                                        Object[] a = (Object[]) session.getAttribute("filterList");
                                        if (a != null)
                                            for (int i = 0; i < a.length; i++) {
                                                String s = (String) a[i];
                                    %>

                                <li style="margin-left: 25px ">
                                    <label for="<%=s%>" class="checkbox ">
                                        <input id="<%=s%>" name="<%=s%>" type="checkbox" value="<%=i%>"><%=s%></label>

                                </li>
                                <%
                                    }
                                %>

                                <li><i class="glyphicon glyphicon-menu-right" ></i>Price</li>
                                <li style="margin-left: 25px ">
                                    <label for="0-99" class="checkbox ">
                                        <input id ="0-99" type="checkbox" name="range-0"  value="0">0-99</label>
                                </li>
                                <li style="margin-left: 25px ">
                                    <label for="100-199" class="checkbox ">
                                        <input id="100-199" type="checkbox" name="range-1" value="1" >100-199</label>
                                </li>
                                <li style="margin-left: 25px ">
                                    <label for="200-499" class="checkbox ">
                                        <input id="200-499" type="checkbox" name="range-2" value="2">200-499</label>
                                </li>
                                <li style="margin-left: 25px ">
                                    <label for="500+" class="checkbox ">
                                        <input id="500+" type="checkbox" name="range-3" value="3">500+</label>
                                </li>
                                <br>

                                <div class="col-xs-15 footer-grid">
                                    <input class="button" type="submit" value="Filter" name="searchByName" style="width: 75%;">
                                    <!--<a href="processRequest?action=getAllProducts" class="btn btn-default" style="width: 75%">Filter</a>-->
                                </div>
                                <br>
                                <li><i class="glyphicon glyphicon-menu-right" ></i>
                                    <a href="processRequest?action=getAllProducts">Clear filters</a></li>

                                <br>

                                <input type="hidden" name="action" value="applyFilters">
                            </ul>
                        </form>



                    </div>
                    <!--//menu-->
                    <!--price-->
                    <div class="price animated wow fadeInUp animated" data-wow-delay=".5s" >

                    </div>
                    <!--//price-->
                    <!--colors-->
                    <div class="colors animated wow fadeInLeft animated" data-wow-delay=".5s" >

                    </div>

                </div>
                <div class="col-md-9 animated wow fadeInRight" data-wow-delay=".5s">
                    <div class="mens-toolbar">
                        <!--   SORTING 

                        <p class="showing">Sorting By
                            <select>
                                <option value=""> Name</option>
                                <option value="">  Rate</option>
                                <option value=""> Color </option>
                                <option value=""> Price </option>
                            </select>
                        </p> 
                        <p>Show
                            <select>
                                <option value="">  9</option>
                                <option value=""> 10</option>
                                <option value=""> 11 </option>
                                <option value=""> 12 </option>
                            </select>
                        </p>

                        -->

                        <div class="col-xs-15 footer-grid animated wow fadeInLeft" data-wow-delay=".7s">
                            <form action="processRequest" method="post" id="SearchName">
                                <input type="text" name="productName" placeholder="What are you looking for?" id="search_text" form="SearchName"/>
                                <input type="submit" name="search" value="Search" id="search_button" class="button" form="SearchName"/>
                                <input type="hidden" value ="getProductByName" name="action">
                            </form>
                        </div>




                        <div class="clearfix"></div>		
                    </div>

                    <div class="mid-popular">
                        <%
                            if (myMap != null) {
                                int capacity = myMap.getCapacity();
                                for (int i = 0; i < capacity; i++) {
                                    TreeSet tree = myMap.getArrayOfTreeSets()[i];
                                    if (displayAll || rangesToDisplay[i]) {
                                        for (Object o : tree) {
                                            Product p = (Product) o;
                        %>

                        <!-- Products here -->
                        <div class="col-sm-4 item-grid item-gr  simpleCart_shelfItem">
                            <div class="grid-pro">
                                <div class="women">
                                    <a href="processRequest?action=getproductbyid&id=<%= p.getProductID()%>" class="thumbnail" target="_self" name="<%=p.getProductID()%>"><img src="images/phoneStore/<%= p.getPictureLink()%>.jpg" alt="<%=p.getProductName()%>" style="width: 75% "></a>
                                    <h6><a href="processRequest?action=getproductbyid&id=<%= p.getProductID()%>" class="thumbnail" target="_self" name="<%=p.getProductID()%>"><%=p.getProductName()%></a></h6>
                                    <p><em class="item_price">$<%=p.getPrice()%></em></p>

                                </div>
                            </div>
                        </div>


                        <%
                                        }
                                    }
                                }
                            }
                        %>

                        <div class="clearfix"></div>
                    </div>
                </div>


            </div class="clearfix"></div>



        <!-- footer -->
        <div class="footer">
            <div class="container">
                <div class="footer-top">
                    <div class="col-md-9 footer-top1">
                        <a href="http://latch.elevenpaths.com" target="_blank"><img src="img/latch_logo.png" width="150" alt="http://latch.elevenpaths.com" ></a>
                    </div>
                    <div class="col-md-3 footer-top2">
                        <a href="contacts.jsp">Contact Us</a>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="footer-grids">
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".5s">
                        <h3>About Us</h3>
                        <p>We are a small company especialised in the last and more advance mobile technology. <span>Click here to know our team.</span> <a href="about.jsp">The team</a></p>
                    </div>
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".6s">
                        <h3>Contact Info</h3>
                        <ul>
                            <li><i class="glyphicon glyphicon-map-marker" ></i>1234k Avenue, 4th block, <span>New York City.</span></li>
                            <li class="foot-mid"><i class="glyphicon glyphicon-envelope" ></i><a href="mailto:info@example.com">info@example.com</a></li>
                            <li><i class="glyphicon glyphicon-earphone" ></i>+1234 567 567</li>
                        </ul>
                    </div>
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".7s">
                        <h3>Sign up for newsletter </h3>
                        <form>
                            <input type="text" placeholder="Email"  required="">
                            <input type="submit" value="Submit">
                        </form>

                    </div>

                    <div class="clearfix"> </div>
                </div>

                <div class="copy-right animated wow fadeInUp" data-wow-delay=".5s">
                    <p>&copy 2016 Classic Style. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
                </div>
            </div>
        </div>
        <!-- //footer -->
    </body>
</html>