<%@page import="Business.Address"%>
<%@page import="Business.Product"%>
<%@page import="Business.CartItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Business.User"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
    <head>
        <title>Phones R Us</title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Classic Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- //for-mobile-apps -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- js -->
        <script src="js/jquery.min.js"></script>
        <!-- //js -->
        <!-- cart -->
        <script src="js/simpleCart.min.js"></script>
        <!-- cart -->
        <!-- for bootstrap working -->
        <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
        <!-- //for bootstrap working -->
        <!-- animation-effect -->
        <link href="css/animate.min.css" rel="stylesheet"> 
        <script src="js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <!-- //animation-effect -->
        <link href='//fonts.googleapis.com/css?family=Cabin:400,500,600,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <!-- header -->
        <div class="header">
            <div class="header-grid">
                <div class="container">

                    <div class="header-right animated wow fadeInRight" data-wow-delay=".5s">
                        <div class="header-right1 ">
                            <ul>
                                <%
                                    session.setAttribute("forwardToJsp", "cart.jsp");

                                    User user = (User) session.getAttribute("user");

                                    ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

                                    if (currentCart == null) {
                                        currentCart = new ArrayList();
                                        session.setAttribute("currentCart", currentCart);
                                    }

                                    if (session.getAttribute("loggedSessionId") != null && session.getAttribute("user") != null) {

                                        out.println("<a href='processRequest?action=logout'> Log out</a><a href='profile.jsp'>\tHello " + user.getfName() + "</a>");
                                    } else {
                                %>
                                <li><i class="glyphicon glyphicon-log-in" ></i><a href="login.jsp">Login</a></li>
                                <li><i class="glyphicon glyphicon-book" ></i><a href="register.jsp">Register</a></li>
                                        <%  }
                                        %>

                            </ul>
                        </div>
                        <div class="header-right2">
                            <h3> <div class="total">
                                    <a href="cart.jsp" class="simpleCart_empty"><img src="images/cart.png" alt="" />&nbsp;&nbsp;&nbsp;Cart</a></div>

                            </h3>
                            <div class="clearfix"> </div>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="container">
                <div class="logo-nav">

                    <nav class="navbar navbar-default">
                        <div class="navbar-header nav_2">

                            <div class="navbar-brand logo-nav-left wow fadeInLeft animated" data-wow-delay=".5s">
                                <h1 class="animated wow pulse" data-wow-delay=".5s"><a href="index.jsp">Phones<span>R</span>Us</a></h1>
                            </div>

                        </div>  
                        <div class="navbar-header nav_2">
                            <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                        </div>
                        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul class="nav navbar-nav">
                                <li><a href="index.jsp">Home</a></li>
                                <li><a href="processRequest?action=getAllProducts">Phone Store</a></li>
                                    <%
                                        if (user != null && user.getIsIsAdmin() == true) {
                                    %>

                                <li><a href="adminController.jsp">Admin Panel</a></li>
                                    <%
                                    } else if (user != null && user.getIsIsAdmin() != true) {
                                    %>
                                <li><a href="processRequest?action=getAllOrders">Orders</a></li>

                                <%    }
                                %>
                                <li><a href="about.jsp">About us</a></li>
                                <li><a href="contacts.jsp">Contacts</a></li>
                                <li><a href="legal.jsp">Legal</a></li>
                                <li><a href="FAQ.jsp">FAQs</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </div>
        <!-- //header -->
        <!--banner-->
        <div class="banner-top">
            <div class="container">
                <h2 class="animated wow fadeInLeft" data-wow-delay=".5s">Checkout</h2>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!-- contact -->
        <div class="check-out">	 
            <div class="container">	 

                <script>$(document).ready(function (c) {
                        $('.close1').on('click', function (c) {
                            $('.cross').fadeOut('slow', function (c) {
                                $('.cross').remove();
                            });
                        });
                    });
                </script>
                <script>$(document).ready(function (c) {
                        $('.close2').on('click', function (c) {
                            $('.cross1').fadeOut('slow', function (c) {
                                $('.cross1').remove();
                            });
                        });
                    });
                </script>	
                <script>$(document).ready(function (c) {
                        $('.close3').on('click', function (c) {
                            $('.cross2').fadeOut('slow', function (c) {
                                $('.cross2').remove();
                            });
                        });
                    });
                </script>
                <%
                    double totalValue = 0;

                %>

                <form class="login-form" method="post" action="/PhoneStoreProject/checkout.jsp" id="submitForm" style="width: 80%; margin-left: 10%;">
                    
                    <table style="margin-left: 0%; " class="table animated wow fadeInLeft" data-wow-delay=".5s">
                        <tr>
                            <th class="t-head"></th>
                            <th class="t-head head-it">Item</th>
                            <th class="t-head">Price</th>
                            <th class="t-head">Quantity</th>
                            <th class="t-head">Update</th>
                            <th class="t-head">Delete</th>
                        </tr>
                        <%                        if (currentCart != null) {
                                for (CartItem item : currentCart) {
                                    int amountLimit = item.getProduct().getStock();
                                    //Integer.parseInt(request.getParameter("stock"));
                                    Product p = item.getProduct();

                        %>
                        <tr class="cross" style="text-align: center">
                            <td class="t-data">
                                <img src="images/phoneStore/<%=item.getProduct().getPictureLink()%>.jpg" style="min-width: 60px" class="img-responsive" alt="">
                            </td>
                            <td class="t-data ring-in">
                                <div>
                                    <h3><%=p.getProductName()%></h3>
                                </div>
                                <div class="clearfix"> </div>
                            </td>
                            <td class="t-data">$<%=p.getPrice()%></td>
                            <td class="t-data"  style="align-content: center">
                                <div>            

                                    <input type="hidden" name="itemId" value="<%=item.getProduct().getProductID()%>" form="updateQttyForm<%=item.getProduct().getProductID()%>"/>
                                    <input type="hidden" name="stock" value="<%=request.getParameter("stock")%>" form="updateQttyForm<%=item.getProduct().getProductID()%>"/>
                                    <input type="hidden" name="action" value="updateqtty" form="updateQttyForm<%=item.getProduct().getProductID()%>">
                                    <select class="form-control " name="qtty" title="quantity" id="quantity" form="updateQttyForm<%=item.getProduct().getProductID()%>">
                                        <option value="0">0</option>
                                        <%
                                            for (int i = 0; i < p.getStock(); i++) {
                                                if ((i + 1) == item.getQty()) {
                                                    out.println("<option selected value=" + (i + 1) + ">" + (i + 1) + "</option>");
                                                } else {
                                                    out.println("<option value=" + (i + 1) + ">" + (i + 1) + "</option>");
                                                }

                                            }
                                        %>
                                    </select>
                                </div>


                            </td>
                            </form>
                            <td class="t-data">
                                <input type="image" src="img/updateQtty.png" style="width: 30%;" form="updateQttyForm<%=item.getProduct().getProductID()%>" />
                            </td>
                            <td class="t-data">
                                <form method="post" action="processRequest">
                                    <input type="hidden" name="cartItemToDelete" value="<%=item.getProduct().getProductID()%>" />
                                    <input type="hidden" name="action" value="deleteProductFromCart" />
                                    <input type="hidden" name="stock" value="<%=request.getParameter("stock")%>" />
                                    <input type="image" src="img/deleteFromCart.png" style="width: 30%;" />
                                </form>
                                <form id="updateQttyForm<%=item.getProduct().getProductID()%>" method="post" action="processRequest">
                                    <input type="hidden" name="action" value="updateqtty">
                                </form>
                            </td>

                        </tr>
                        <%                                totalValue += (item.getProduct().getPrice() * item.getQty());
                                }
                            }
                        %>


                    </table>
                    <div class="cart-total">
                        <ul class="total_price">
                            <li class="last_price"> <h5>TOTAL</h5></li>	
                            <li class="last_price"><h5><%
                                int temp = (int) totalValue * 100;

                                totalValue = temp / 100.0;

                                session.setAttribute("totalPrice", (float) totalValue);

                                out.print(totalValue);
                                    %></h5></li>
                            <div class="clearfix"> </div>
                        </ul>
                        <br>



                        <!-- Load the Client component. -->
                        <script src="https://js.braintreegateway.com/web/3.0.0/js/client.min.js"></script>

                        <!-- Load the Hosted Fields component. -->
                        <script src="https://js.braintreegateway.com/web/3.0.0/js/hosted-fields.min.js"></script>

                        <script>
                    var authorization = '<%=(String) session.getAttribute("clientToken")%>';

                    //window.alert(authorization);
                    braintree.client.create({
                        authorization: authorization
                    }, function (clientErr, clientInstance) {
                        if (clientErr) {
                            // Handle error in client creation
                            return;
                        }

                        braintree.hostedFields.create({
                            client: clientInstance,
                            styles: {
                                'input': {
                                    'font-size': '14pt'
                                },
                                'input.invalid': {
                                    'color': 'red'
                                },
                                'input.valid': {
                                    'color': 'green'
                                }
                            },
                            fields: {
                                number: {
                                    selector: '#card-number',
                                    placeholder: '4111 1111 1111 1111'
                                },
                                cvv: {
                                    selector: '#cvv',
                                    placeholder: '123'
                                },
                                expirationDate: {
                                    selector: '#expiration-date',
                                    placeholder: '10 / 2019'
                                }
                            }
                        }, function (hostedFieldsErr, hostedFieldsInstance) {
                            if (hostedFieldsErr) {
                                // Handle error in Hosted Fields creation
                                return;
                            }
                            submit.removeAttribute('disabled');
                        });
                    });
                        </script>   

                        <div class="col-md-4 footer-grid">
                            <%
                                if (user != null && !currentCart.isEmpty()) {
                            %>


                            <a data-toggle="modal" data-target="#myModal" />Checkout</a>
                            <%
                                } else if (currentCart.isEmpty()) {
                                    out.print("<h5>No items</h5>");
                                } else {
                                    out.print("<h5>Please Login</h5>");
                                }

                            %>
                        </div>

                    </div>


            
            </div>
        </div>
        <!--quantity-->
        <script>
            $('.value-plus').on('click', function () {
                var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) + 1;
                divUpd.text(newVal);
            });

            $('.value-minus').on('click', function () {
                var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
                if (newVal >= 1)
                    divUpd.text(newVal);
            });
        </script>
        <!--quantity-->

        <div class="social animated wow fadeInUp" data-wow-delay=".1s">

        </div>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title" id="myModalLabel">Select your shipping address</h4>
                    </div>
                    <div class="panel-body">
                        <fieldset class="addresses" required>  
                            <%ArrayList<Address> addresses = (ArrayList<Address>) session.getAttribute("addressList");
                                if (addresses != null) {
                                    for (Address item : addresses) {
                            %>
                            <label for="<%=item.getAddressID()%>"><input type="radio" name="addressSelection" id="<%=item.getAddressID()%>" value="<%=item.getAddressID()%>" form="submitForm" /><%=item.display()%></label>
                                <%
                                        }
                                    } else {
                                        out.println("No addresses registered");
                                    }

                                %>
                            <hr>
                            <label for="newAddress"><input type='radio' name='addressSelection' id="newAddress" value="newAddress" form='submitForm' />Add a new address</label>
                        </fieldset>
                        <input name="address" type="text" placeholder="Address" form="submitForm"/>
                        <input name="county" type="text" placeholder="County" form="submitForm"/>
                        <input name="country" type="text" placeholder="Country" form="submitForm"/>

                        
                    </div>
                    <div class="panel-footer">
                        <a href="#cancel"><input type="submit" class="btn btn-default" value="Cancel" data-dismiss="modal" /></a>
                        <input type="submit" name="action" value="Proceed to pay" class="btn btn-primary" form="submitForm" />

                    </div>
                </div>
            </div>
        </div>

        <!-- footer -->
        <div class="footer">
            <div class="container">
                <div class="footer-top">
                    <div class="col-md-9 footer-top1">
                        <a href="http://latch.elevenpaths.com" target="_blank"><img src="img/latch_logo.png" width="150" alt="http://latch.elevenpaths.com" ></a>
                    </div>
                    <div class="col-md-3 footer-top2">
                        <a href="contacts.jsp">Contact Us</a>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="footer-grids">
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".5s">
                        <h3>About Us</h3>
                        <p>We are a small company especialised in the last and more advance mobile technology. <span>Click here to know our team.</span> <a href="about.jsp">The team</a></p>
                    </div>
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".6s">
                        <h3>Contact Info</h3>
                        <ul>
                            <li><i class="glyphicon glyphicon-map-marker" ></i>1234k Avenue, 4th block, <span>New York City.</span></li>
                            <li class="foot-mid"><i class="glyphicon glyphicon-envelope" ></i><a href="mailto:info@example.com">info@example.com</a></li>
                            <li><i class="glyphicon glyphicon-earphone" ></i>+1234 567 567</li>
                        </ul>
                    </div>
                    <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".7s">
                        <h3>Sign up for newsletter </h3>
                        <form>
                            <input type="text" placeholder="Email"  required="">
                            <input type="submit" value="Submit">
                        </form>

                    </div>

                    <div class="clearfix"> </div>
                </div>

                <div class="copy-right animated wow fadeInUp" data-wow-delay=".5s">
                    <p>&copy 2016 Classic Style. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
                </div>
            </div>
        </div>
        <!-- //footer -->
    </body>
</html>