/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function speak(text) {
    // Create a new SpeechSynthesisUtterance variable.
    var msg = new SpeechSynthesisUtterance();
    // Set the text.
    msg.text = text;
    // Set the attributes.
    msg.volume = parseFloat(10);
    msg.rate = parseFloat(1);
    msg.pitch = parseFloat(1);
    //the next line selects the actual voice we wish to use
    msg.voice = speechSynthesis.getVoices().filter(function (voice)
    {//In this case we are using Google UK English Male
        return voice.name == 'Google UK English Male';
    })[0];

    // this is what finally transfer the information from text to speech
    window.speechSynthesis.speak(msg);
}

document.getElementById("navbar").addEventListener("mouseover", function (e) {
    //this type of event listener is known as event delegation
    if (e.target && e.target.nodeName === "LI") {
        speak(e.target.textContent);
    }
});

document.getElementById("List").addEventListener("mouseover", function (e) {
    if (e.target && e.target.nodeName === "P")
    {
        if (e.target.id === "twitterLink") {
            speak("a link to follow us on twitter");
        } else {
            speak(e.target.textContent);
        }
    } else if (e.target && e.target.nodeName === "INPUT") {
        speak(e.target.value);
    } else if (e.target && e.target.nodeName === "IMG") {
        speak(e.target.alt);
    }
});

document.getElementById("shopNowButton").addEventListener("mouseover", function (e)
{
    speak(e.target.textContent);
});