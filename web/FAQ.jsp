<%@page import="Business.Order"%>
<%@page import="Business.Product"%>
<%@page import="Business.CartItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Business.User"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
    <head>
        <title>Phones R Us</title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Classic Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- //for-mobile-apps -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- js -->
        <script src="js/jquery.min.js"></script>
        <!-- //js -->
        <!-- cart -->
        <script src="js/simpleCart.min.js"></script>
        <!-- cart -->
        <!-- for bootstrap working -->
        <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
        <!-- //for bootstrap working -->
        <!-- animation-effect -->
        <link href="css/animate.min.css" rel="stylesheet"> 
        <script src="js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <!-- //animation-effect -->
        <link href='//fonts.googleapis.com/css?family=Cabin:400,500,600,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <!-- header -->
        <div class="header">
            <div class="header-grid">
                <div class="container">

                    <div class="header-right animated wow fadeInRight" data-wow-delay=".5s">
                        <div class="header-right1 ">
                            <ul>
                                <%
                                    session.setAttribute("forwardToJsp","index.jsp");
                                    
                                    User user = (User) session.getAttribute("user");

                                    ArrayList<CartItem> currentCart = (ArrayList<CartItem>) session.getAttribute("currentCart");

                                    if (currentCart == null) {
                                        currentCart = new ArrayList();
                                        session.setAttribute("currentCart", currentCart);
                                    }

                                    if (session.getAttribute("loggedSessionId") != null && session.getAttribute("user") != null) {

                                        out.println("<a href='processRequest?action=logout'> Log out</a><a href='profile.jsp'>\tHello " + user.getfName() + "</a>");
                                    } else {
                                %>
                                <li><i class="glyphicon glyphicon-log-in" ></i><a href="login.jsp">Login</a></li>
                                <li><i class="glyphicon glyphicon-book" ></i><a href="register.jsp">Register</a></li>
                                        <%  }
                                        %>
                                
                            </ul>
                        </div>
                        <div class="header-right2">
                                    <h3> <div class="total">
                                            <a href="cart.jsp" class="simpleCart_empty"><img src="images/cart.png" alt="" />&nbsp;&nbsp;&nbsp;Cart</a></div>
                                        
                                    </h3>
                                <div class="clearfix"> </div>
                            	
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="container">
                <div class="logo-nav">

                    <nav class="navbar navbar-default">
                        <div class="navbar-header nav_2">

                            <div class="navbar-brand logo-nav-left wow fadeInLeft animated" data-wow-delay=".5s">
                                <h1 class="animated wow pulse" data-wow-delay=".5s"><a href="index.jsp">Phones<span>R</span>Us</a></h1>
                            </div>

                        </div> 
                        <div class="navbar-header nav_2">
                            <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul class="nav navbar-nav">
                                <li><a href="index.jsp">Home</a></li>
                                <li><a href="processRequest?action=getAllProducts">Phone Store</a></li>
                                    <%
                                        if (user != null && user.getIsIsAdmin() == true) {
                                    %>

                                <li><a href="adminController.jsp">Admin Panel</a></li>
                                    <%
                                    } else if (user != null && user.getIsIsAdmin() != true) {
                                    %>
                                <li><a href="processRequest?action=getAllOrders">Orders</a></li>

                                <%    }
                                %>
                                <li><a href="about.jsp">About us</a></li>
                                <li><a href="contacts.jsp">Contacts</a></li>
                                <li><a href="legal.jsp">Legal</a></li>
                                <li><a href="FAQ.jsp">FAQs</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </div>
        <!-- //header -->
        <!--banner-->
        <div class="banner-top">
            <div class="container">
                <h2 class="animated wow fadeInLeft" data-wow-delay=".5s">F.A.Q.s</h2>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div id="main-content" style="margin-bottom: 5%; margin-top: 5%">
            <div class="container" id="legalContainer">
                <div class="row">

                    <H4><b>Got a question about buying online?<br><br></b></H4>
                    <H4><b> Check out our Online FAQ below with helpful tips and advices.</b></H4>
                    <br>
                    <p><b>How long does it take to apply for a Bill Pay phone or broadband modem?<br><br></b></p>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>If your order is placed before 4pm we aim to respond within the same day. Sometimes we may require further information or documentation from you before our order processing team can make a decision on an application.</li>
                            <li>We'll keep you updated via e-mail regarding your status and sometimes may need to contact you via phone regarding your order.</li>
                        </ul>
                    </p2>
                    <br>
                    <p><b>What are the possible decision outcomes?<br><br></b></p>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>Customers who meet our service criteria will be approved and we will then send out your order.</li>
                            <li>Sometimes, we may require a deposit from the customer before we can approve the application. If this is required, our team will phone you and you can make payment by credit or debit card. This deposit will be applied as a credit to your Three account after your sixth bill, subject to terms and conditions.</li>
                            <li>Unfortunately, from time to time applications may not meet our service criteria and are unsuccessful.</li>
                        </ul>
                    </p2>
                    <br>
                    <p><b>Help! I've made an error in my order. Who can I contact?<br><br></b></p>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>Don't worry, mistakes happen! Please contact our Sales team at 1800-949-546 as soon as possible and they'll be happy to help you regarding your application. Please make sure you have your order reference number handy.</li>
                        </ul>
                    </p2>
                    <br>
                    <H4><b>Delivery.</b></H4>
                    <p><b>Is it Free?<br><br></b></p>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>Yes, absolutely! We offer our online customers free delivery in ROI.</li>
                        </ul>
                    </p2>
                    <br>
                    <p><b>What ways can my order be delivered?<br><br></b></p>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>Your order can be delivered to your home address (Listed as 'Current Address' in your order form).</li>
                            <li>It can also be delivered to your workplace or an alternative address (Simply click 'no' when you are asked 'Is this also your delivery address?').<br><br></li>
                            <li>Certain products can be collected in a PhonesRUS Store of your choice with our 'Click & Collect' service. Simply select 'Store Collection' when ordering your product and select from the list of participating stores. </li>
                        </ul>
                    </p2>
                    <br>
                    <p><b>How long will I have to wait for delivery?<br><br></b></p>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>The delivery period depends on the product ordered. In the event of an unforeseen delay in your delivery you will be notified via e-mail and will be contacted by our support team.</li>
                            <li>Phones and Mobile Broadband Modems: We offer next working day delivery by courier (unless otherwise stated) for all phones and modems which are credit approved before 4pm. Online Orders placed after 4pm will be delivered in two working days. The courier agent will require you to sign for these.</li>
                        </ul>
                    </p2>
                    <br>
                    <p><b>How Can I Check My Delivery Status?<br><br></b></p>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>You can check the status of your online delivery anytime! Simply go on Three.ie, click 'Shop' and 'Track My Order'. You'll need to provide your Order Reference Number and Surname.</li>
                        </ul>
                    </p2>
                    <br>
                    <H4><b>Click & Collect.</b></H4>
                    <p><b>What is Click & Collect?<br><br></b></p>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>Click and Collect allows customers to purchase products online at PhonesRUS.com or via our telesales team at 1800-949-546 and collect these products from certain PhonesRUS Stores.</li>
                        </ul>
                    </p2>
                    <br>
                    <p><b>How does Click & Collect work?<br><br></b></p>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>Select your product and proceed to checkout, under "Delivery/Store Collection" select "Store Collection" if available. You must then select the store you wish to collect the device from. You will then proceed with the application as normal.</li>
                            <li>When you arrive in-store you must provide your Order Reference Number. In the case of Bill Pay applications you must also provide the exact Proof of Identity and Proof of Address (dated within the last 90 days) which you provided in your application.</li>
                        </ul>
                    </p2>
                    <br>
                    <h4><b>Returns.</b></h4>
                    <p><b>What's Three's returns policy for online purchases?<br><br></b></p>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>As a consumer, you may cancel the purchase of the device within 14 days (this does not affect your statutory rights under the Sale of Goods Acts) and receive a full refund under the terms of the European Union (Consumer Information, Cancellation and Other Rights) Regulations 2013.</li>
                            <li>You may return your device within 14 days if you have experience a verifiable network fault which has been confirmed by Three Customer Services.</li>
                        </ul>
                    </p2>
                    <br>
                    <p><b>How do you return online purchases?<br><br></b></p>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>Firstly, you'll need to action your 7/14 day return in accordance with the distance selling regulations.</li>
                            <li>We'll provide you with the return address, case reference number and device value (you'll need this for your receipt for payment).</li>
                            <li>Peel off the return label included in your delivery, attach it to your envelope and return by registered post.</li>
                            <li>Responsibility for the return of goods lies with the customer and Three strongly advises using registered post.</li>
                        </ul>
                    </p2>
                    <br>
                    <h4><b>Still Have A Question?<br><br></b></h4>
                    <p2 style="justify-content: center">
                        <ul>
                            <li>Why not ask our Sales Web Chat team in the contact us panal for more information.</li>
                        </ul>
                    </p2>
                </div>
            </div><!-- @end .container -->
        </div><!-- @end #main-content -->
        <div class="footer">
        <div class="container">
            <div class="footer-top">
                <div class="col-md-9 footer-top1">
                    <a href="http://latch.elevenpaths.com" target="_blank"><img src="img/latch_logo.png" width="150" alt="http://latch.elevenpaths.com" ></a>
                </div>
                <div class="col-md-3 footer-top2">
                    <a href="contacts.jsp">Contact Us</a>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="footer-grids">
                <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".5s">
                    <h3>About Us</h3>
                    <p>We are a small company especialised in the last and more advance mobile technology. <span>Click here to know our team.</span> <a href="about.jsp">The team</a></p>
                </div>
                <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".6s">
                    <h3>Contact Info</h3>
                    <ul>
                        <li><i class="glyphicon glyphicon-map-marker" ></i>1234k Avenue, 4th block, <span>New York City.</span></li>
                        <li class="foot-mid"><i class="glyphicon glyphicon-envelope" ></i><a href="mailto:info@example.com">info@example.com</a></li>
                        <li><i class="glyphicon glyphicon-earphone" ></i>+1234 567 567</li>
                    </ul>
                </div>
                <div class="col-md-4 footer-grid animated wow fadeInLeft" data-wow-delay=".7s">
                    <h3>Sign up for newsletter </h3>
                    <form>
                        <input type="text" placeholder="Email"  required="">
                        <input type="submit" value="Submit">
                    </form>

                </div>

                <div class="clearfix"> </div>
            </div>

            <div class="copy-right animated wow fadeInUp" data-wow-delay=".5s">
                <p>&copy 2016 Classic Style. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
            </div>
        </div>
    </div>
    <!-- //footer -->
</body>
</html>