/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Business.Address;
import Business.User;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author XPS
 */
public class AddressDaoTest {
    
    private MyDataSource db = new MyDataSource();
    private AddressDao instance = new AddressDao(db);
    
    

    public AddressDaoTest() {
    }
    
    
    @BeforeClass
    public static void setUpClass() {  
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    
    /**
     * Test of getAllUserAddress method, of class AddressDao.
     */
    @Test
    public void testgetAllUserAddress() {
        System.out.println("getAllUserAddress");
        ArrayList<Address> expResult = new ArrayList();
        expResult = instance.getAllUserAddress(1);
        assertNotNull(expResult);
        int result = 2;
        assertTrue(expResult.size()>=result);
    }
    
    
     /**
     * Test of insertAddres method, of class AddressDao.
     */
    @Test
     //testing for a null value
    public void testinsertNullAddres() 
    {
        System.out.println("insertNullAddres");
        Address nAddress = null;
        
        boolean expResult = false;
        boolean result = instance.insertAddres(nAddress);
        assertEquals(expResult, result);
    }
    
    
     @Test
    // Teting for a valid value
    public void testinsertValidAddres() 
    {
        MyDataSource db2 = new MyDataSource();
        AddressDao instance2 = new AddressDao(db2);
        
        UserDao userDao = new UserDao(db2);
        User user = new User();
        user.setUserId(333);
        user.setEmail("myTestEmail2");
        user.setIsAdmin(false);
        user.setPasswordHashed("passwrd");
        user.setPhone("0987654321");
        user.setWalletCredits(100);
        user.setfName("this");
        user.setlName("that");
        userDao.registerNewUser(user);
        
        AddressDao addressDao = new AddressDao(db);
        Address address = new Address();
        address.setCountry("test");
        address.setAddress("test");
        address.setCounty("test");
        address.setUserID(333);
        address.setAddressID(999);        
        
        System.out.println("insertValidAddress");
        
        boolean expResult = true;
        boolean result = instance.insertAddres(address);
        assertEquals(expResult, result);
        
        // Checking that the product is in the database
        ArrayList<Address> addressList = instance.getAllUserAddress(user.getUserId());
        
        boolean actualResult = false;
        
        for(int i = 0; i<addressList.size(); i++)
        {
            Address item = addressList.get(i);
            if(item.equals(address))
            {
                actualResult = true;
                break;
            }
        }
        
        assertTrue(actualResult);
        // Deleting address for future tests
        boolean deleted = userDao.deleteUser(user.getUserId());
        User u = userDao.getUserById(user.getUserId());
        
        if(u == null)
        {
            deleted = true;
        }
        
        assertTrue(deleted);
    }
    
    
    /**
     * Test of deleteAddressById method, of class AddressDao.
     */
    @Test
    // testing for a null value
    public void testdeleteNullAddressById() 
    {
        System.out.println("deleteNullAddressById");
        
        boolean expResult = false;
        boolean result = instance.deleteAddressById(-1);
        assertEquals(expResult, result);
    }
    
    
    @Test
    // Teting for a valid value
    public void testdeleteValidAddressById() 
    {
        MyDataSource db2 = new MyDataSource();
        AddressDao instance2 = new AddressDao(db2);
        
        UserDao userDao = new UserDao(db2);
        User user = new User();
        user.setUserId(333);
        user.setEmail("myTestEmail2");
        user.setIsAdmin(false);
        user.setPasswordHashed("passwrd");
        user.setPhone("0987654321");
        user.setWalletCredits(100);
        user.setfName("this");
        user.setlName("that");
        userDao.registerNewUser(user);
        
        AddressDao addressDao = new AddressDao(db);
        Address address = new Address();
        address.setCountry("test");
        address.setAddress("test");
        address.setCounty("test");
        address.setUserID(333);
        address.setAddressID(666);       
          
        
        System.out.println("DeleteValidAddressById");
        boolean expResult = true;
        Boolean result = addressDao.insertAddres(address);
        assertTrue(result);
        
        assertEquals(expResult, result);
        
        
        result = addressDao.deleteAddressById(address.getAddressID());
        assertTrue(result);
        
        assertEquals(expResult, result);
        
        
        // Checking that the product is in the database
        ArrayList<Address> addressList = instance.getAllUserAddress(user.getUserId());
        
        boolean actualResult = false;
        
        
            if(addressList.size()<=0)
            {
                actualResult = true;
            }
        
        
        assertTrue(actualResult);
        
        boolean deleted = userDao.deleteUser(user.getUserId());
        assertTrue(deleted);
        
    }
    
    
    /**
     * Test of deleteAllAddressesById method, of class AddressDao.
     */
    @Test
    // testing for a null value
    public void testdeleteAllNullAddressesById() 
    {
        System.out.println("deleteAllNullAddressesById");
        
        boolean expResult = false;
        boolean result = instance.deleteAllAddressesById(-1);
        assertEquals(expResult, result);
    }
    
    
    @Test
    // Teting for a valid value
    public void testdeleteAllValidAddressById() 
    {
        MyDataSource db2 = new MyDataSource();
        AddressDao instance2 = new AddressDao(db2);
        
        UserDao userDao = new UserDao(db2);
        User user = new User();
        user.setUserId(333);
        user.setEmail("myTestEmail2");
        user.setIsAdmin(false);
        user.setPasswordHashed("passwrd");
        user.setPhone("0987654321");
        user.setWalletCredits(100);
        user.setfName("this");
        user.setlName("that");
        userDao.registerNewUser(user);
        
        AddressDao addressDao = new AddressDao(db);
        Address address = new Address();
        address.setCountry("test");
        address.setAddress("test");
        address.setCounty("test");
        address.setUserID(333);
        address.setAddressID(333); 
        
        Address address1 = new Address();
        address1.setCountry("test1");
        address1.setAddress("test1");
        address1.setCounty("test1");
        address1.setUserID(333);
        address1.setAddressID(000); 
          
        
        System.out.println("DeleteAllValidAddressesById");
        boolean expResult = true;
        Boolean result = addressDao.insertAddres(address);
        assertTrue(result);
        
        assertEquals(expResult, result);
        
        result = addressDao.insertAddres(address1);
        assertTrue(result);
        
        assertEquals(expResult, result);
        
        
        result = addressDao.deleteAllAddressesById(address.getAddressID());
        assertTrue(result);
        
        assertEquals(expResult, result);
        
        
        // Checking that the product is in the database
        ArrayList<Address> addressList = instance.getAllUserAddress(user.getUserId());
        
        boolean actualResult = false;
        
        
            if(addressList.size()<=0)
            {
                actualResult = true;
            }
        
        
        assertTrue(actualResult);
        
        boolean deleted = userDao.deleteUser(user.getUserId());
        assertTrue(deleted);
        
    }
    
    
     /**
     * Test of updateAddress method, of class AddressDao.
     */
    @Test
    // testing for a null value
    public void testupdateNullAddress() 
    {
        System.out.println("updateNullAddress");
        AddressDao addressDao = new AddressDao(db);
        Address address = new Address();
        address.setCountry(null);
        address.setAddress(null);
        address.setCounty(null);
        address.setUserID(-1);
        address.setAddressID(-1); 
        
        boolean expResult = false;
        boolean result = instance.updateAddress(address);
        assertEquals(expResult, result);
    }
    
    
    @Test
    // Teting for a valid value
    public void testUpdateValidAddress() 
    {
        MyDataSource db2 = new MyDataSource();
        AddressDao instance2 = new AddressDao(db2);
        
        UserDao userDao = new UserDao(db2);
        User user = new User();
        user.setUserId(333);
        user.setEmail("myTestEmail2");
        user.setIsAdmin(false);
        user.setPasswordHashed("passwrd");
        user.setPhone("0987654321");
        user.setWalletCredits(100);
        user.setfName("this");
        user.setlName("that");
        userDao.registerNewUser(user);
        
        AddressDao addressDao = new AddressDao(db);
        Address address = new Address();
        address.setCountry("test");
        address.setAddress("test");
        address.setCounty("test");
        address.setUserID(333);
        address.setAddressID(333); 
        addressDao.insertAddres(address);
        
        
        Address address1 = new Address();
        address1.setCountry("test1");
        address1.setAddress("test1");
        address1.setCounty("test1");
        address1.setUserID(333);
        address1.setAddressID(333); 
        
        System.out.println("updateValidAddress");
        Boolean result = addressDao.updateAddress(address1);
        boolean expResult = true; 
        assertTrue(result);
        assertEquals(expResult, result);
        
        
        // Checking that the product is in the database
        ArrayList<Address> addressList1 = instance.getAllUserAddress(user.getUserId());
        
        boolean actualResult = false;
        for(int i = 0; i< addressList1.size(); i++)
        {
            Address item = addressList1.get(i);
            if(item .equals(address1))
            {
                actualResult = true;
                break;
            }
        }
        
        
        
        assertTrue(actualResult);
        
        
        
        
        
        result = addressDao.deleteAllAddressesById(address.getAddressID());
        assertTrue(result);
        
        assertEquals(expResult, result);
        
        
        // Checking that the product is in the database
        ArrayList<Address> addressList = instance.getAllUserAddress(user.getUserId());
        
        actualResult = false;
        
        
            if(addressList.size()<=0)
            {
                actualResult = true;
            }
        
        
        assertTrue(actualResult);
        
        boolean deleted = userDao.deleteUser(user.getUserId());
        assertTrue(deleted);
        
    }
    
    
    /**
     * Test of selectLastIdAdded method, of class AddressDao.
     */
    @Test
    // testing for a null value
    public void testSelectNullLastIdAdded() 
    {
        System.out.println("selectNullLastIdAdded");
        AddressDao addressDao = new AddressDao(db);
        Address address = new Address();
        int i = addressDao.selectLastIdAdded(-1);
        boolean result = false;
        if(i == 0)
        {
            result = false;
        }
        else
        {
            result = true;
        }
        
        boolean expResult = false;
        assertEquals(expResult, result);
    }
    
    
    @Test
    // Teting for a valid value
    public void testSelectValidLastIdAdded() 
    {
        System.out.println("selectNullLastIdAdded");
        AddressDao addressDao = new AddressDao(db);
        Address address = new Address();
        int i = addressDao.selectLastIdAdded(1);
        boolean result = false;
        if(i == 0)
        {
            result = false;
        }
        else
        {
            result = true;
        }
        
        boolean expResult = true;
        assertEquals(expResult, result);
    }
    
    
    
}
