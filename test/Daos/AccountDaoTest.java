/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Business.Account;
import Business.User;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author XPS
 */
public class AccountDaoTest {
    
    private MyDataSource db = new MyDataSource();
    private AccountDao instance = new AccountDao(db);
    
    

    public AccountDaoTest() {
    }
    
    
    @BeforeClass
    public static void setUpClass() {  
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    
    /**
     * Test of updateAccount method, of class AccountDao.
     */
    @Test
    // testing for a null value
    public void testupdateNullAccount() 
    {
        
        System.out.println("updateNullAccount");
      
        boolean result = instance.update(null);
        boolean expResult = false;
        assertEquals(result,expResult);
        
    }
    
    
    @Test
    // Teting for a valid value
    public void testUpdateValidAccount() 
    {
        System.out.println("updateValidAccount");
        UserDao userDao = new UserDao(db);
        User user = new User();
        user.setUserId(333);
        user.setEmail("myTestEmail2");
        user.setIsAdmin(false);
        user.setPasswordHashed("passwrd");
        user.setPhone("0987654321");
        user.setWalletCredits(100);
        user.setfName("this");
        user.setlName("that");
        userDao.registerNewUser(user);
        
        boolean result = instance.update(user.getEmail());
        assertTrue(result);
        
        result = instance.reset(user.getEmail());
        assertTrue(result);
        
        boolean deleted = userDao.deleteUser(user.getUserId());
        assertTrue(deleted);
    }
    
    
    /**
     * Test of getStatus method, of class AccountDao.
     */
    @Test
    // testing for a null value
    public void testgetNullStatus() 
    {
        
        System.out.println("getNullStatus");
        boolean result = false;
      Account account = instance.getStatus(null);
      if(account==null)
      {
          result = true;
      }
        assertTrue(result);
        
    }
    
    
    @Test
    // Teting for a valid value
    public void testGetValidStatus() 
    {
        System.out.println("getValidStatus");
        UserDao userDao = new UserDao(db);
        User user = new User();
        user.setUserId(333);
        user.setEmail("myTestEmail2");
        user.setIsAdmin(false);
        user.setPasswordHashed("passwrd");
        user.setPhone("0987654321");
        user.setWalletCredits(100);
        user.setfName("this");
        user.setlName("that");
        userDao.registerNewUser(user);
        
        boolean result = instance.update(user.getEmail());
        assertTrue(result);
        
        result = false;
       Account account = instance.getStatus(user.getEmail());
      if(account!=null)
      {
          result = true;
      }
        assertTrue(result);
        
        
        
        result = instance.reset(user.getEmail());
        assertTrue(result);
        
        boolean deleted = userDao.deleteUser(user.getUserId());
        assertTrue(deleted);
    }
    
    
    /**
     * Test of reset method, of class AccountDao.
     */
    @Test
    // testing for a null value
    public void testNullReset() 
    {
        
        System.out.println("nullReset");
        boolean expResult = false;
        boolean result = instance.reset(null);
            assertEquals(result,expResult);
        
    }
    
    @Test
    // Teting for a valid value
    public void testValidReset() 
    {
        System.out.println("getValidStatus");
        UserDao userDao = new UserDao(db);
        User user = new User();
        user.setUserId(333);
        user.setEmail("myTestEmail2");
        user.setIsAdmin(false);
        user.setPasswordHashed("passwrd");
        user.setPhone("0987654321");
        user.setWalletCredits(100);
        user.setfName("this");
        user.setlName("that");
        userDao.registerNewUser(user);
        
        boolean result = instance.update(user.getEmail());
        assertTrue(result);
        
        result = false;
       Account account = instance.getStatus(user.getEmail());
      if(account!=null)
      {
          result = true;
      }
        assertTrue(result);
        
        
        
        result = instance.reset(user.getEmail());
        assertTrue(result);
        
        boolean deleted = userDao.deleteUser(user.getUserId());
        assertTrue(deleted);
    }
    
}
