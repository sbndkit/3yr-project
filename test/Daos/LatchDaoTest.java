/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import com.elevenpaths.latch.LatchUser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sergio
 */
public class LatchDaoTest {
    
    private MyDataSource db = new MyDataSource();
    private LatchDao instance = new LatchDao(db);
    public LatchDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getLatchInfo method, of class LatchDao.
     */
    @Test
    public void testGetLatchInfo() {
        System.out.println("getLatchInfo");
        //Setting the appID and the secret for our application
        String appID = "EHRkWTMsAhgZCfnNVh37";
        String secret = "dhxAGeN7JscWYFdxJjetXyiTmJeG8K4Ef8a3zEws";
        // creating Latch user
        LatchUser expResult = null;
        // seting the informaiton in the LatchUser
        expResult = new LatchUser(appID, secret);
        // Checking that the LatchUser object is not null
        assertNotNull(expResult);
        // Getting information from the database
        LatchUser result = instance.getLatchInfo();
        // Checking that the information recived object is not null
        assertNotNull(result);
        // comparing the appID received from the server
        assertTrue(expResult.getAppId().equals(appID));
        // Comparing the secret received from the server
        assertTrue(expResult.getSecretKey().equals(secret));
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setLatchInfo method, of class LatchDao.
     */
    @Test
    public void testSetLatchInfo() {
        System.out.println("setLatchInfo");
        //Setting variables
        String APP_ID = "test";
        String SECRET = "test";
        // Inserting the information into the database
        instance.setLatchInfo(APP_ID, SECRET);
        // Getting the information back from the database
        LatchUser result = instance.getLatchInfo();
        //
        LatchUser expResult = new LatchUser(APP_ID,SECRET);
        // Checking that the information recived object is not null
        assertNotNull(result);
        // comparing the appID received from the server
        assertTrue(expResult.getAppId().equals(APP_ID));
        // Comparing the secret received from the server
        assertTrue(expResult.getSecretKey().equals(SECRET));
        String appID = "EHRkWTMsAhgZCfnNVh37";
        String secret = "dhxAGeN7JscWYFdxJjetXyiTmJeG8K4Ef8a3zEws";
        // Setting the right information back
        instance.setLatchInfo(appID, secret);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    
    
}
