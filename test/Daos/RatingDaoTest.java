/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Business.Rating;
import Business.User;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author XPS
 */
public class RatingDaoTest {
    
    private MyDataSource db = new MyDataSource();
    private RatingDao instance = new RatingDao(db);
    
    

    public RatingDaoTest() {
    }
    
    /**
     * This setUpClass sets up a user and gives that user rating entries
     * so we can work with this data without affecting other entries in the database
     */
    @BeforeClass
    public static void setUpClass() {
        
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    
    /**
     * Test of addComment method, of class RatingDao.
     */
//    @Test
    // testing for a null value
//    public void testAddNullComment() 
//    {
//        System.out.println("addNullComment");
//        Rating nRating = null;
//        
//        boolean expResult = false;
//        boolean result = instance.addComment(-1,-1,null);
//        assertEquals(expResult, result);
//    }
    
    
//     @Test
    // Teting for a valid value
//    public void testAddValidComment() 
//    {
//        System.out.println("AddValidComment");
//        int userId =1;
//        int productId = 1;
//        String comment = "testing";
//        Rating rating = new Rating(userId,productId,comment);
//  
// 
//        boolean expResult = true;
//        boolean result = instance.addComment(userId,productId,comment);
//        
//        assertEquals(expResult, result);
//        
//        // Checking that the product is in the database
//        ArrayList<Rating> ratingList = instance.FindAllComments();
//        
//        boolean actualResult = false;
//        
//        for(int i = 0; i<ratingList.size(); i++)
//        {
//            if(ratingList.get(i).equals(rating))
//            {
//                actualResult = true;
//                break;
//            }
//        }
//        
//        assertTrue(actualResult);
//        // Deleting item for future tests
//        instance.deleteUserCommentOnProduct(userId,productId);
//    }
    
    
     /**
     * Test of deleteAllUserComment method, of class RatingDao.
     */
    @Test
    // testing for a null value
    public void testDeleteAllNullUserComment() 
    {
        System.out.println("DeleteAllNullUserComment");
        
        boolean expResult = false;
        boolean result = instance.deleteAllUserComment(-1);
        assertEquals(expResult, result);
    }
    
    
    @Test
    // Teting for a valid value
    public void testDeleteAllValidUserComment() 
    {
        MyDataSource db2 = new MyDataSource();
        RatingDao instance2 = new RatingDao(db2);
        
        UserDao userDao = new UserDao(db2);
        User user = new User();
        user.setUserId(333);
        user.setEmail("myTestEmail1");
        user.setIsAdmin(false);
        user.setPasswordHashed("passwrd");
        user.setPhone("0987654321");
        user.setWalletCredits(100);
        user.setfName("this");
        user.setlName("that");
        userDao.registerNewUser(user);
        int rating = 2;
        
        instance2.addRating(user.getUserId(), 1, rating, "test");
        instance2.addRating(user.getUserId(), 2, rating, "test");
        instance2.addRating(user.getUserId(), 3, rating, "test");
        instance2.addRating(user.getUserId(), 4, rating, "test");
        
        System.out.println("DeleteAllValidUserComment");
        int userId =user.getUserId();
  
 
        boolean expResult = true;
        boolean result = instance.deleteAllUserComment(userId);
        
        assertEquals(expResult, result);
        
        // Checking that the product is in the database
        ArrayList<Rating> ratingList = instance.FindAllCommentsByUserId(userId);
        
        boolean actualResult = false;
        
        
            if(ratingList.size()<=0)
            {
                actualResult = true;
            }
        
        
        assertTrue(actualResult);
        
        boolean deleted = userDao.deleteUser(userId);
        assertTrue(deleted);
        
    }
    
    
    /**
     * Test of addComment method, of class RatingDao.
     */
    @Test
    // testing for a null value
    public void testAddNullRating() 
    {
        System.out.println("addNullRating");
        Rating nRating = null;
        
        boolean expResult = false;
        boolean result = instance.addRating(-1,-1,-1,null);
        assertEquals(expResult, result);
    }
    
    
     @Test
    // Teting for a valid value
    public void testAddValidRating() 
    {
        MyDataSource db2 = new MyDataSource();
        RatingDao instance2 = new RatingDao(db2);
        
        UserDao userDao = new UserDao(db2);
        User user = new User();
        user.setUserId(333);
        user.setEmail("myTestEmail2");
        user.setIsAdmin(false);
        user.setPasswordHashed("passwrd");
        user.setPhone("0987654321");
        user.setWalletCredits(100);
        user.setfName("this");
        user.setlName("that");
        userDao.registerNewUser(user);
        float rating = 2;
        
        System.out.println("AddValidRating");
        int userId =user.getUserId();
        int productId = 4;
        String comment = "testing";
        int stars = 1;
        Rating ratingObj = new Rating(productId,userId,stars,comment);
 
        boolean expResult = true;
        boolean result = instance.addRating(userId,productId,stars,comment);
        assertEquals(expResult, result);
        
        // Checking that the product is in the database
        ArrayList<Rating> ratingList = instance.FindAllComments();
        
        boolean actualResult = false;
        
        for(int i = 0; i<ratingList.size(); i++)
        {
            Rating item = ratingList.get(i);
            if(item.equals(ratingObj))
            {
                actualResult = true;
                break;
            }
        }
        
        assertTrue(actualResult);
        // Deleting user for future tests
        boolean deleted = userDao.deleteUser(userId);
        User u = userDao.getUserById(userId);
        
        if(u == null)
        {
            deleted = true;
        }
        
        assertTrue(deleted);
    }
    
    
    
    /**
     * Test of deleteUserCommentOnProduct method, of class RatingDao.
     */
    @Test
    // testing for a null value
    public void testDeleteNullUserCommentOnProduct() 
    {
        System.out.println("DeleteNullUserCommentOnProduct");
        
        boolean expResult = false;
        boolean result = instance.deleteUserCommentOnProduct(-1,-1);
        assertEquals(expResult, result);
    }
    
    
    @Test
    // Teting for a valid value
    public void testDeleteValidUserCommentOnProduct() 
    {
        MyDataSource db2 = new MyDataSource();
        RatingDao instance2 = new RatingDao(db2);
        
        UserDao userDao = new UserDao(db2);
        User user = new User();
        user.setUserId(333);
        user.setEmail("myTestEmail3");
        user.setIsAdmin(false);
        user.setPasswordHashed("passwrd");
        user.setPhone("0987654321");
        user.setWalletCredits(100);
        user.setfName("this");
        user.setlName("that");
        userDao.registerNewUser(user);
        int rating = 2;
        
        instance.addRating(user.getUserId(), 1, rating, "test");
        
        System.out.println("DeleteAllValidUserCommentOnProduct");
        
        int userId =user.getUserId();
        int productId = 1;
  
 
        boolean expResult = true;
        boolean result = instance.deleteUserCommentOnProduct(userId,productId);
        
        assertEquals(expResult, result);
        

        ArrayList<Rating> ratingList = instance.FindAllCommentsByProductId(productId);
        
        boolean actualResult = true;
        
        for(int i =0; i<ratingList.size(); i++)
        {
            if(ratingList.get(i).getUserId()==userId)
            {
                actualResult = false;
                break;
            }
        }
        
        assertTrue(actualResult);
        
        boolean deleted = userDao.deleteUser(userId);
        assertTrue(deleted);
        
    }
    
    
    /**
     * Test of deleteUserCommentOnProduct method, of class RatingDao.
     */
    @Test
    // testing for a null value
    public void testEditNullRating() 
    {
        System.out.println("EditNullRating");
        
        boolean expResult = false;
        boolean result = instance.editRating(-1,-1,null,-1);
        assertEquals(expResult, result);
    }
    
    
    @Test
    // Teting for a valid value
    public void testEditValidRating() 
    {
        MyDataSource db2 = new MyDataSource();
        RatingDao instance2 = new RatingDao(db2);
        
        UserDao userDao = new UserDao(db2);
        User user = new User();
        user.setUserId(333);
        user.setEmail("myTestEmail4");
        user.setIsAdmin(false);
        user.setPasswordHashed("passwrd");
        user.setPhone("0987654321");
        user.setWalletCredits(100);
        user.setfName("this");
        user.setlName("that");
        userDao.registerNewUser(user);
        int rating = 2;
        
        instance2.addRating(user.getUserId(), 1, rating, "test");

        System.out.println("EditValidRating");
        ArrayList<User> userArray = userDao.FindAllUsers();
        for(int i = 0; i< userArray.size(); i++)
        {
            if(userArray.get(i).getEmail().equals(user.getEmail()) && userArray.get(i).getPassword().equals(user.getEmail()))
            {
                user = userArray.get(i);
            }
        }
        int userId =user.getUserId();
        int productId = 1;
  
 
        boolean expResult = true;
        boolean result = instance.editRating(userId,productId, "newTest",4);
        assertEquals(expResult, result);
        

        ArrayList<Rating> ratingList = instance.FindAllCommentsByProductId(productId);
        
        boolean actualResult = false;
        
        for(int i =0; i<ratingList.size(); i++)
        {
           Rating item = ratingList.get(i);
            Rating userRating = new Rating(productId, userId,4,"newTest");
            if(item.equals(userRating))
            {
                actualResult = true;
                break;
            }
        }
        
        assertTrue(actualResult);
        
        boolean deleted = userDao.deleteUser(userId);
        assertTrue(deleted);
        
    }
    
    
    
    /**
     * Test of FindAllCommentsByUserId, of class RatingDao.
     */
    @Test
    public void testFindAllCommentsByUserId()
    {
        System.out.println("FindAllCommentsByUserId");
        ArrayList<Rating> expResult = new ArrayList();
        expResult = instance.FindAllCommentsByUserId(2);
        assertNotNull(expResult);
        int result = 2;
        assertTrue(expResult.size()>=result);
    }
    
     /**
     * Test of FindAllCommentsByProductId, of class RatingDao.
     */
    @Test
    public void testFindAllCommentsByProductId()
    {
        System.out.println("FindAllCommentsByProductId");
        ArrayList<Rating> expResult = new ArrayList();
        expResult = instance.FindAllCommentsByProductId(1);
        assertNotNull(expResult);
        int result = 1;
        assertTrue(expResult.size()>=result);
    }
    
    
    
    /**
     * Test of FindAllComments method, of class RatingDao.
     */
    @Test
    public void testFindTotalRating() {
        System.out.println("FindAllTotalRating");
        float expResult = instance.FindTotalRating(1);
        assertNotNull(expResult);
        float result = (float)3.5;
        assertTrue(expResult == result);
        
    }
    
    
    /**
     * Test of FindAllComments method, of class RatingDao.
     */
    @Test
    public void testFindAllComments() {
        System.out.println("FindAllComments");
        ArrayList<Rating> expResult = new ArrayList();
        expResult = instance.FindAllComments();
        assertNotNull(expResult);
        int result = 4;
        assertTrue(expResult.size()>=result);
        
    }
    
    
    
    

}
