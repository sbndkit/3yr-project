/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Business.HashMap;
import Business.Product;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sergio
 */
public class ProductDaoTest {
    
    private MyDataSource db = new MyDataSource();
    private ProductDao instance = new ProductDao(db);
    
    public ProductDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAllProducts method, of class ProductDao.
     */
    @Test
    public void testGetAllProducts() throws SQLException {
        System.out.println("getAllProducts");
        HashMap<String,Product> expResult = null;
        expResult = instance.getAllProducts();
        int result = 10;
        assertEquals(expResult.getCount(), result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of registerProduct method, of class ProductDao.
     */
    @Test
    // testing for a null value
    public void testInsertNullProduct() 
    {
        System.out.println("registerNullProduct");
        Product nProduct = null;
        
        boolean expResult = false;
        boolean result = instance.registerProduct(nProduct);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    // Teting for a valid value
    public void testInsertValidProduct() 
    {
        String name ="Test23";
        System.out.println("registerValidProduct");
        Product nProduct = new Product();
        nProduct.setProductName(name);
        nProduct.setDesc("Test");
        nProduct.setPrice(1.1);
        nProduct.setStock(1);
        
        
        
        boolean expResult = true;
        boolean result = instance.registerProduct(nProduct);
        
        assertEquals(expResult, result);
        
        // Checking that the product is in the database
        HashMap<String, Product> myMap = instance.getProductLikeName(name);
        
        boolean actualResult = false;
        
        for(TreeSet item : myMap.getArrayOfTreeSets()){
            for(Object itemProduct : item){
                Product p = (Product)itemProduct;
                
                if (p.getProductName().equals(name))
                {
                    actualResult = true;
                    break;
                }
            }
        }
        
        assertTrue(actualResult);
        // Deleting item for future tests
        instance.deleteProduct(name);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    /**
     * Test of deleteProduct method, of class ProductDao.
     */
    @Test
    // testing for a non-registered product
    public void testDeleteProductNotRegistered() {
        System.out.println("testDeleteProductNotRegistered");
        int id = -1;
        
        boolean expResult = false;
        boolean result = instance.deleteProduct(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     @Test
    // testing for a registered product by productName
    public void testDeleteProductRegisteredName() {
        System.out.println("testDeleteProductRegisteredName");
        
        // Creating a product object for the test
        Product nProduct = new Product();
        
        String name = "Test23";
        nProduct.setProductName(name);
        nProduct.setDesc("Test");
        nProduct.setPrice(1.1);
        nProduct.setStock(1);
        
        // Creating a ProductDao instance
        
        
        // registering the product into the database
        boolean tt= instance.registerProduct(nProduct);
        
        // Checking that the product is in the database
        HashMap<String, Product> myMap = instance.getProductLikeName(name);
        
        boolean actualResult = false;
        
        for(TreeSet item : myMap.getArrayOfTreeSets()){
            for(Object itemProduct : item){
                Product p = (Product)itemProduct;
                
                if (p.getProductName().equals(name))
                {
                    actualResult = true;
                    break;
                }
            }
        }
        
        assertTrue(actualResult);
        // Checking method returns
        boolean expResult = true;
        boolean result = instance.deleteProduct(name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     @Test
    // testing for a registered product by productName
    public void testUpdateProductRegisteredID() {
        System.out.println("testUpdateProductRegisteredID");
        int id = 331;
        
        
        // Creating a product object to register into the database
        //Product nProduct = new Product(id,"test",1.0,"test",20);
        // Inserting the product into the database
        //instance.registerProduct(nProduct);
        // Checking if the product was registered
        //assertEquals(nProduct, instance.getProduct(id));
        
        // Giving a new value to nProduct
        //nProduct = new Product(id,"jUnit-Test",9.0,"description",9,);
        // Checking returning values
        boolean expResult = true;
        //boolean result = instance.updateProduct(nProduct);
        //assertEquals(expResult, result);
        // Checking update in the database
        //assertEquals(nProduct, instance.getProduct(id));
        // Deleting product for future tests
        instance.deleteProduct(id);
        // Checking if the product was deleted
        assertNull(instance.getProduct(id));
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
}
