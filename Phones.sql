DROP DATABASE IF EXISTS MobileShop;

Create Database if not exists MobileShop;
Use MobileShop;

Create table MobileShop.Product
(	
	productID INT(5) NOT NULL AUTO_INCREMENT,
	productName VARCHAR(100) NOT NULL UNIQUE,
	price Float(6,2) NOT NULL,
	description VARCHAR(10000) NOT NUll,
	stock Int(3) NOT NULL,
        brand VARCHAR(20) NOT NULL,
        pictureLink VARCHAR(100) NOT NULL DEFAULT "default",
	PRIMARY KEY (productID),
        INDEX index_desc (description)
);
Create table MobileShop.User
(	
	userID INT(5) NOT NULL AUTO_INCREMENT,
	fName VARCHAR(100) NOT NULL,
	lName VARCHAR(100) NOT NULL,
	email VARCHAR(100) NOT NULL UNIQUE,
	phone VARCHAR(100) NOT NULL,
	pass VARCHAR(100) NOT NULL,
	isAdmin BOOLEAN NOT NULL,
	walletCredits Int(100) NOT NULL DEFAULT 0, 
        accountId VARCHAR(100) NOT NULL DEFAULT "",
	PRIMARY KEY (userID) 
);

Create table MobileShop.Supplier
(	
	supplierID INT(5) NOT NULL AUTO_INCREMENT,
	supplierName Varchar(100) NOT NULL,
	address Varchar(100) NOT NULL,
	county Varchar(100) NOT NULL,
	country Varchar(100) NOT NULL,
	email VARCHAR(100) NOT NULL UNIQUE,
	phone Varchar(100) NOT NULL,
	PRIMARY KEY (supplierID)
);

Create table MobileShop.Orders	
(
	orderID INT(5) NOT NULL AUTO_INCREMENT,
	dateOrdered DATE NOT NULL,
	userID int(5) NOT NULL,
        addressID int(5) NOT NULL,
        totalPrice float(7,2) NOT Null,
	PRIMARY KEY (orderID),
	constraint fkUserID FOREIGN KEY(userID) references MobileShop.User (userID)
);

Create table MobileShop.OrderProduct
(
	orderID INT(5) NOT NULL,
	productID INT(5) NOT NULL,
	quantity INT(3) NOT NULL CHECK (quantity > 0),
	constraint fkOrder FOREIGN KEY(orderID) references MobileShop.Orders (orderID),
	constraint fkproduct FOREIGN KEY(productID) references MobileShop.Product (productID),
	constraint pk PRIMARY KEY(orderID,productID)
);

Create table MobileShop.ProductSupplier
(
	productID INT(5) NOT NULL,
	supplierID INT(5) NOT NULL,
	CONSTRAINT fkProductPS FOREIGN KEY(productID) references MobileShop.Product (productID),
	CONSTRAINT fkSupplier FOREIGN KEY(supplierID) references MobileShop.Supplier (supplierID),
	CONSTRAINT pk PRIMARY KEY(productID, supplierID)
);		

Create table MobileShop.Latch(
    appID varchar(100),
    secret varchar(100)
);

Create table MobileShop.Account(
    email VARCHAR(100) NOT NULL PRIMARY KEY,
    attempts int(2) not null,
    time DATETIME,
    accountStatus varchar(10) not null,
    constraint fkEmail FOREIGN KEY(email) references MobileShop.User (email)
);

Create table MobileShop.PassRecovery(
    email VARCHAR(100) NOT NULL PRIMARY KEY,
    time DATETIME,
    id VARCHAR(100) NOT NULL,
    constraint fkPassRecovery FOREIGN KEY(email) references MobileShop.User (email)
);

CREATE TABLE MobileShop.Rating(
	userId Int(5) NOT NULL,
	productId Int(5) NOT NULL,
        rating Int(1),
	comment VARCHAR(1000),
	constraint pk PRIMARY KEY(userId,productId),
        constraint fk_rating_user FOREIGN KEY(userId) REFERENCES MobileShop.User(userID),
        constraint fk_rating_product FOREIGN KEY(productId) REFERENCES MobileShop.Product(productID)
);

Create table MobileShop.Address
(
        addressID INT(5) NOT NULL AUTO_INCREMENT UNIQUE,
	userID INT(5) NOT NULL,
	address VARCHAR(100) NOT NULL,
	county VARCHAR(100) NOT NULL,
	country VARCHAR(100) NOT NULL,
        CONSTRAINT PRIMARY KEY (addressID),
	CONSTRAINT fkAddressUserID FOREIGN KEY(userID) REFERENCES MobileShop.User (userID)
);



INSERT INTO  MobileShop.Product (productID ,  ProductName, price, description, stock, brand) 
VALUES (NULL, 'Apple iPhone 6, 16Gb', '649.00', 'iPhone 6 isn’t just bigger — it’s better in every way. A 4.7-inch Retina HD display. An A8 chip with 64-bit desktop-class architecture. A new 8MP iSight camera with Focus Pixels. The Touch ID fingerprint identity sensor. Faster 4G and Wi-Fi. Longer battery life. And iOS 8 and iCloud. All in a 6.9mm thin seamless design.

4.7-inch (diagonal) Retina HD display with 1334x720 resolution
A8 chip with M8 motion coprocessor
8-megapixel iSight camera with Focus Pixels and True Tone flash
1080p HD video recording at 60 fps and slo-mo video recording at 240 fps
FaceTime HD camera
Touch ID fingerprint sensor
4G and 802.11ac Wi-Fi
iOS 8 and iCloud
Available in gold, silver or space grey', '5', 'Apple');

INSERT INTO  MobileShop.Product (productID ,  ProductName, price, description, stock, brand) 
values (NULL,'Apple iPhone 6 Plus', '869.99', 'iPhone 6 Plus isn’t just bigger - it’s better in every way. A 5.5 inch Retina HD display. An A8 chip with 64-bit desktop-class architecture. A new 8MP iSight camera with Focus Pixels and optical image stabilisation. The Touch ID fingerprint identity sensor. Faster 4G and Wi-Fi. Longer battery life. And iOS 8 and iCloud. All in a 7.1mm thin seamless design. 5.5 inch (diagonal) Retina HD display with 1920 x 1080 resolution A8 chip with M8 motion coprocessor 8-megapixel iSight camera with Focus Pixels, True Tone flash and optical image stabilisation 1080p HD video recording at 60 fps and slo-mo video recording at 240 fps FaceTime HD camera Touch ID fingerprint sensor 4G and 802.11ac Wi-Fi iOS 8 and iCloud Available in gold, silver or space grey.', '4', 'Apple');

INSERT INTO MobileShop.Product (productID,  ProductName, price, description, stock, brand) 
VALUES (NULL, 'HTC One A9 16Gb', '599.00', 'Design worth imitating. Photos the pros will admire. High-resolution audio.

The smartphone that gets all the important elements right. From the company that crafted the world''s first all-metal design, HTC again sets a new design standard. The HTC One A9 features a super-thin metal frame with an elegant finish, expandable SD card memory and edge-to-edge Corning Gorilla glass.

Unleash your inner photographer with the HTC One A9. Take blur-free photos with Optical Image Stabilisation (OIS). Save more image details for unsurpassed editing and creative control with RAW capture in Pro mode. Speed up your videos up to 12x for a dramatic effect with Hyperlapse.

The HTC One A9 features an edge- to-edge 5 inch full high definition, 1080p, and energy efficient screen. You get 40% more colour than a normal LCD screen for brilliant graphics and gaming, even in direct sunlight. Breathe new life into your music. Built-in DAC converts 16-bit audio to 24-bit high-resolution sound for richness and depth that you have never heard FROM a smartphone.

The HTC One A9 delivers the best sound quality on the market with Dolby audio surround and an all-new enhanced high-output amplifier. Connect to your everyday faster with Android 6.0 Marshmallow. Quickly realise your possibilities with Now on Tap* and Android Pay* and unlock it all with a tap of your finger. Download films, music and images 2x faster, and charge 40% faster with Qualcomm Quick Charge. 2.0 technology.', '2', 'HTC');

INSERT INTO MobileShop.Product (productID,  ProductName, price, description, stock, brand) 
VALUES (NULL, 'HTC One M8s, 16Gb - Gunmetal', '419.00', 'The HTC One M8s smartphone is now even better AND cheaper – yes, you did read that right!

Boasting a superior camera, processor and battery, an already outstanding phone just got even better. Weighing 160g and just 9.6 mm thick, you still get a stunning 5 full HD inch screen that''s perfect for surfing the web, viewing video and typing messages.

A powerful octa-core processor ensures that everything happens fast. It also feels incredibly good in your hand thanks to a curvy aluminium unibody and speaker grills at the top and bottom.

The 13 megapixel rear camera captures crisp, vivid photos and 1080p HD video, while the 5 megapixel front camera is ideal for superior selfies and video chats.

HTC BoomSound makes all your music sound stunning. The built-in 16Gb of storage can be expanded by up to 128Gb using the microSD card slot. Also 4G compatible.

Useful info:

5 inch full HD touch screen (1920 x 1080)
Android 4.4 KitKat OS
HTC Blink Feed
2Gb RAM
Octo-core Snapdragon 615 processor
13 Mp rear camera with 1080p HD video
5 Mp front camera
BoomSound front-facing speakers
WiFi, 3G, DLNA, Bluetooth 4.0
4G compatible
16Gb built-in storage expandable by up to 128Gb via microSD card
H 146, W 70.6, D 9.6 mm
Weight: 160g
HTC One M8s smartphone - Gunmetal', '3', 'HTC');



INSERT INTO MobileShop.Product (productID,  ProductName, price, description, stock, brand) 
VALUES (NULL, 'LG G3,16Gb - Gold', '349.00', 'The stylish LG G3 smartphone''s outstanding 5.5 inch HD+ touch screen is a real treat for your eyes.

Bring all your web surfing, video and photos to life in spectacular style thanks to a 1440 x 2560 QHD resolution (giving you a stunning 538 pixels per inch for incredibly detail and colour) that makes the most of the superb phablet-style screen size.

Everything happens smooth and fast thanks to a seriously powerful 2.5GHz QualComm Snapdragon 801 processor and the Android 4.4.2 KitKat operating system.

The cameras are equally impressive – a superb 13 megapixel rear camera with laser autofocus designed to be the fastest on the market, plus an LED flash, optical image stabilisation and Ultra HD video. You also get a 2.1 megapixel front-facing camera for video chats or selfies.

You get 16Gb of onboard storage, which can be expanded by up to a massive 128Gb by adding a microSD card, so you can carry around plenty of movies, games, photos and more.

A 3000mAh battery keeps you going all day and you also get support for wireless charging.

Useful info:

Snapdragon 801 quad core processor 2.5GHz
5.5 inch IPS + LCD touch screen(1440 x 2560, 538ppi)
13 Mp autofocus camera with image stabilisation and dual flash
2160p HD video at 30fps
2.1 Mp front-facing camera
16Gb storage expandable by up to 128Gb via microSD card slot
Android 4.4.2 KitKat OS
2Gb RAM
Wi-Fi
GPS
Bluetooth
Dolby Mobile Sound
Dimensions: 146.3 x 74.6 x 8.9 mm
Weight: 149g
LG G3 smartphone 16Gb - Gold', '2', 'LG');


INSERT INTO MobileShop.Product (productID,  ProductName, price, description, stock, brand) 
VALUES (NULL, 'LG G3 S, 8Gb - Black', '239.00', 'The LG G3 S is a compact and slimline smartphone packed with impressive features.

You get a 5 inch screen that brings all your web surfing, video, gaming photos to life thanks to a 1280 x 720 HD resolution in a phone that weighs only 134g.

Everything happens smooth and fast thanks to a Snapdragon 400 processor, 1Gb RAM and the Android 4.4.2 KitKat operating system.

The cameras are equally impressive – an 8 megapixel rear camera with laser autofocus designed to be the fastest on the market, plus an LED flash and HD video. You also get a 1.3 megapixel front-facing camera for video chats or selfies.

There''s 8Gb of onboard storage, which can be expanded by up to 64Gb by adding a microSD card, so you can carry around plenty of movies, games, photos and more.

The brushed metal look and curved edges complete an attractive smartphone that will keep you in touch and entertained WHEREver you go.

Useful info:

5 inch HD touch screen (1280x720)
Snapdragon 400 quad core processor 1.2GHz
1Gb RAM
8 Mp autofocus camera with laser autofocus and HD video
1.3 Mp front-facing camera
8Gb storage expandable by up to 64Gb via microSD card slot
Android 4.4.2 KitKat OS
Wi-Fi, GPS, Bluetooth 4.0
Dimensions: 137.7 x 69.6 x 10.3 mm
Weight: 134g
LG G3 S smartphone 16Gb', '2', 'LG');


INSERT INTO MobileShop.Product (productID,  ProductName, price, description, stock, brand) 
VALUES (NULL, 'Microsoft Lumia 435 8Gb', '82.00', 'Upgradeable to Windows 10 when available. See Microsoft.com/mobile/windows10 for details.

The stylish Microsoft Lumia 435 is a versatile Windows 8.1 smartphone for a truly outstanding price.

The 4 inch touch screen combines with Windows 8.1 to let you tap and swipe your way through the things you love, you''ll have no problems getting to grips with this incredibly user-friendly phone. There''s even a planned upgrade to Windows 10 on the way!

There''s a 2 megapixel camera for shooting photos and video of your adventures, and there''s 8Gb of memory built in. If that''s not enough, it''s compatible with microSD cards up to 128Gb, so you can carry around lots of music, photos and video.

The Lumia 435 weighs only 134g, so it''s very comfortable in your hand, whether you''re making a call or surfing the web.

It comes with popular Microsoft services like Skype, Office and OneDrive – pre-installed and free – giving you a smartphone experience that’s normally found on more expensive high-end phones.

Useful info:

Perfect first smartphone
4 inch IPS LCD touch screen (800x480)
1.2GHz Snapdragon 200 dual core processor
8Gb storage, expandable by up to 128Gb using microSD card
Windows 8.1, planned upgrade to Windows 10
2 Mp camera (1600x1200) and front-facing VGA camera
Wi-Fi, 3G, GPS, Bluetooth 4.0
microUSB 2.0
1Gb RAM
H 118.1, W 64.7, D 11.7 mm
Weight: 134g
Microsoft Lumia 435 smartphone - Green
Colour: Green.', '3', 'Microsoft');

INSERT INTO MobileShop.Product (productID,  ProductName, price, description, stock, brand) 
VALUES (NULL, 'Samsung Galaxy S6 Edge, 64Gb', '699.00', 'When only the best will do, it''s time to get your hands on the Samsung Galaxy S6 Edge.

Wrapped in a sleek metal and glass body, it also boasts super-fast processing power, a state of the art camera, an improved fingerprint sensor and a 5.1 inch Super AMOLED touch screen featuring a market-leading 2560 x 1440 pixel resolution and curved Gorilla Glass 4.

There''s lots of power under the hood, with a cutting-edge octa-core Exynos processor and the Android Lollipop operating system.

A 16 megapixel rear camera lets you capture great photos and 4K video, with image stabilisation keeping blur at bay even in low light. There''s also a front-facing 5 megapixel camera for video chats and selfies.

Boost your security with a fingerprint sensor and the S6 even helps you stay in shape with an upgraded S Health App and heart rate monitor. A handy dashboard provides a simple view of your fitness, including records of your walking/running distance, calories, speed and duration.

With 64Gb of storage, you can carry plenty of photos, MP3s and video with you. You can also fast charge the battery – letting you watch 2 hours of HD video FROM 10 minutes charging.

Features:

5.1 inch Super AMOLED display (1440 x 2560, 557ppi)
Octa-core Exynos processor
3Gb RAM
Android 5.0.2 Lollipop
16 megapixel autofocus camera (2988x5312) with image stabilisation
5 megapixel front camera
Fingerprint sensor
Wi-Fi, 3G, 4G, Bluetooth® 4.0, GPS
64Gb storage
S-Voice commands
Size: 143.4 x 70.5 x 7 mm
Weight: 132g
Samsung Galaxy S6 Edge smartphone 64Gb', '5', 'Samsung');

INSERT INTO MobileShop.Product (productID,  ProductName, price, description, stock, brand) 
VALUES (NULL, 'Samsung Galaxy S6-32Gb', '599.00', 'Wrapped in a sleek metal and glass body, the S6 boasts super-fast processing power, a state of the art camera, an improved fingerprint sensor and a superb 5.1 inch Super AMOLED touch screen featuring a market-leading 2560x1440 pixel resolution with incredible colours and superb detail.

There''s plenty of power under the hood, with a cutting-edge octa-core Exynos processor and the Android Lollipop operating system.

A 16 megapixel rear camera lets you capture great photos and 4K video, with image stabilisation keeping blur at bay even in low light. There''s also a front-facing 5 megapixel camera for video chats and selfies.

Boost your security with a fingerprint sensor and the S6 even helps you stay in shape with an upgraded S Health App and heart rate monitor. A handy dashboard provides a simple view of your fitness, including records of your walking/running distance, calories, speed and duration.

With 32Gb of internal storage, you can carry plenty of photos, MP3s and video with you. You can also fast charge the battery – letting you watch around 2 hours of HD video FROM 10 minutes charging.

Features:

5.1 inch Super AMOLED display (1440 x 2560, 557ppi)
Octa-core Exynos processor
3Gb RAM
Android 5.0.2 Lollipop
16 Mp autofocus camera (2988x5312) with image stabilisation
5 Mp front camera
Fingerprint sensor
Wi-Fi, 3G, 4G, Bluetooth® 4.0, GPS
32Gb storage
S-Voice language commands
Size: 143.4 x 70.5 x 6.8 mm
Weight: 136 g
Samsung Galaxy S6 smartphone', '5', 'Samsung');

INSERT INTO MobileShop.Product (productID,  ProductName, price, description, stock, brand) 
VALUES (NULL, 'Samsung Galaxy Ace 3, 4Gb', '139.00', 'The Samsung Galaxy Ace 3 is a slimline and stylish smartphone packed with great features.

You can enjoy web browsing, watch movies and play games on the scratch-resistant 4 inch display and it weighs only 115g, so it feels incredibly comfortable in your hand. Perfect for youngsters, smartphone novices and occasional users.

There''s a 5 megapixel camera with autofocus and an LED flash that lets you capture that special moment then share it on Facebook or in a message. The Sound and Shot feature even lets you record sound while taking a photo. A secondary VGA front camera is ideal for video chats.

S Translator offers instantaneous translation when messaging with global users. S Travel offers recommendations, travel guides and more for your trips. Smart Stay knows if you are looking at the display, so keeps the screen bright when you are reading a webpage, viewing photos and more.

S Voice uses your voice to wake up the phone, answer incoming calls or even take a photo.

You get 4Gb of storage built in, with the option of adding up to another 64Gb using a microSD card, so you can carry around plenty of music, movies, photos and more.

Features:

4 inch 480 x 800 TFT display
1.2Ghz dual core processor
1Gb RAM
4Gb storage expandable by up to 64Gb via microSD card
5 Mp autofocus rear-facing camera
HD (1280 x 720) @30fps video recording
VGA front camera
S Voice functionality
Games Hub
WiFi, 3G, Bluetooth 4.0, 4G compatible
H 121.2, W 62.7, D 9.8 mm
Weight: 115g
Samsung Galaxy Ace 3 - White', '6', 'Samsung');


INSERT INTO MobileShop.Product (productID,  ProductName, price, description, stock, brand) 
VALUES (NULL, 'Sony Xperia C4, 16Gb', '299.00', 'The Sony Xperia C4 is packed with first-class smartphone features, including a superior selfie camera.


The 5.5 inch full HD screen with enhanced sharpness and contrast is a treat for your eyes, with Super Vivid Mode ensuring you can see everything clearly even in bright sunlight.

Just 7.9 mm thin and weighing 147g, it feels incredibly comfortable in your hand, particularly for a big screen phone. You also get a long-lasting battery that can keep going for up to 2 days.

A 13 megapixel rear camera with an Exmor RS sensor lets you can frame, shoot and review all your footage in rich colour, contrast and detail. Image stabilisation gives you blur-free photos even in low light and you can shoot 1080p full HD video.

But the 5 megapixel front camera is equally impressive, thanks to a wide angle lens and LED flash that ensure perfect selfie group shots with everyone in the frame. Settings are automatically optimised to capture sharp, clear portraits, even in low light, with the soft LED flash providing natural-looking light that ensures everyone is looking their best. 

An octa-core processor and 2Gb RAM ensure everything happens fast, including downloads and playing games.

Sony 3D Surround Sound technology and front-facing speakers create superior audio. There''s also 16Gb of internal storage that''s expandable by 128Gb using a microSD card.

Features:
Octa-core processor 1.7 GHz
5.5 inch IPS LCD touch screen (1920 x 1080, 401 ppi)
2Gb RAM
Android Lollipop 5.0
Mobile BRAVIA® Engine 2
16Gb storage, expandable by up to 128Gb using a microSD card
13 Mp rear camera with Exmor RS sensor and autofocus
HD 1080p video
5 Mp front camera with Exmor R and flash
Wi-Fi, Bluetooth® 4.1, NFC, GPS
2600mAh battery
H 150.3, W 77.4, D 7.9 mm
Weight: 147g
Sony Xperia C4 smartphone


Camera: Y
Depth: 7.9 MM
Height: 150.3 MM
Width: 77.4 MM
Front Facing Camera: Y
Main Camera: Y
Memory (in Gigabyte): 16
Screen Size: 5.5
Screen Type: Touchscreen
Sim Type: Pay as you go
Storage: Micro SD', '2', 'Sony');


INSERT INTO MobileShop.Product (productID,  ProductName, price, description, stock, brand) 
VALUES (NULL, 'Sony Xperia Z3 Plus, 32Gb', '615.00', 'The Sony Xperia Z3+ just got even better – lighter, thinner yet somehow packed with superior features!

Now just 6.9 mm thin and weighing only 144g, it feels incredibly comfortable in your hand as you enjoy the stunning 5.2 inch full HD Triluminos touch screen.

The outstanding 20.7 megapixel camera lets you can frame, shoot and review all your footage in rich colour, contrast and detail. You get a sensor that''s 30% larger than standard, so you can shoot 4K Ultra HD videos 4 times sharper than Full HD. The rear camera has also been upgraded to 5.1 megapixels with full HD video recording.

The Snapdragon 810 octa-core processor and 3Gb RAM do everything at the speed of light, including downloading and play games, browsing the web and watching videos.

The Xperia Z3+ is dust and water-resistant, so you''ll never fear dropping your phone in the sink again - you can even shoot photos under water! There''s 32Gb of internal storage that''s expandable by 128Gb using a microSD card.

Sony 3D Surround Sound technology and front-facing speakers create superior audio.

Useful info:

Qualcomm Snapdragon 810 SoC octa-core processor
5.2 inch full HD Triluminos display (1920x1080)
32Gb storage expandable by up to 128Gb via microSD card
3Gb RAM
Android 5.0 Lollipop
20.7 Mp camera (5248x3936) with 4K video recording
5.1 Mp front camera
IP168 Waterproof and dust protected
3D Surround Sound technology
Long lasting 2930mAh battery
Weight 144 g
H 146, W 72, D 6.9 mm
Sony Xperia Z3+ smartphone phablet', '1', 'Sony');


INSERT INTO MobileShop.User (userID, fName, lName, email, phone, pass, isAdmin) 
VALUES (NULL, 'Sergio', 'Vilaseco', 'test@test.com', '0859876543','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','1');

INSERT INTO MobileShop.User (userID, fName, lName, email, phone, pass, isAdmin)
VALUES (NULL, 'Brian', 'Mc Manus', 'test1@test.com', '0859876543','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','1');

INSERT INTO MobileShop.User (userID, fName, lName, email, phone, pass, isAdmin)
VALUES (NULL, 'Niall', 'Mulready', 'test2@test.com', '0859876543','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','0');


INSERT INTO MobileShop.Orders (orderID, dateOrdered, userID) VALUES
(1, '2015-12-03', 1),
(2, '2015-12-02', 2),
(3, '2015-11-09', 3),
(4, '2015-12-07', 1);

INSERT INTO MobileShop.OrderProduct (orderID, productID, quantity) VALUES
(1, 1, 3),
(1, 4, 1),
(2, 8, 1),
(3, 11, 2);

INSERT INTO MobileShop.Latch (appID, secret) VALUES
('EHRkWTMsAhgZCfnNVh37', 'dhxAGeN7JscWYFdxJjetXyiTmJeG8K4Ef8a3zEws');

INSERT INTO MobileShop.Supplier (supplierID, supplierName, address, county, country, email, phone) 
VALUES 
(NULL, 'Sony', 'Sony Address', 'Dublin', 'Ireland', 'suppor@Sony.ie', '0856530174'),
(NULL, 'Apple', 'Apple Address', 'Dublin', 'Ireland', 'suppor@apple.ie', '0147182937'),
(NULL, 'HTC', 'HTC Address', 'Dublin', 'Ireland', 'suppor@HTC.ie', '0757169273'),
(NULL, 'LG', 'LG Address', 'Dublin', 'Ireland', 'suppor@LG.ie', '0987629372'),
(NULL, 'Samsung', 'Samsung Address', 'Dublin', 'Ireland', 'suppor@samsung.ie', '0144562937'),
(NULL, 'Microsoft', 'Microsoft Address', 'Dublin', 'Ireland', 'suppor@Microsoft.ie', '0144502437');

INSERT INTO MobileShop.Rating VALUES (1,1,4,"This is a great product, would recommend it to any apple fanboys");
INSERT INTO MobileShop.Rating VALUES (2,4,2,"Great phone, battery lasts a long time");
INSERT INTO MobileShop.Rating VALUES (3,2,5,"Cool phone with tons of great features");
INSERT INTO MobileShop.Rating VALUES (2,1,3,"good, phone but the memory isnt that big");

INSERT INTO MobileShop.Address(userID, address, county, country) VALUES (1,'345 Lower Point Rd. Dundalk','Louth','Ireland');
INSERT INTO MobileShop.Address(userID, address, county, country) VALUES (1,'43 South Bank Terrace', 'Armagh','United Kingdom');
INSERT INTO MobileShop.Address(userID, address, county, country) VALUES (2,'23 Parklane, Clogherhead','Louth','Ireland');
INSERT INTO MobileShop.Address(userID, address, county, country) VALUES (2,'64 Damolly fields, Newry','Down','United Kingdom');
INSERT INTO MobileShop.Address(userID, address, county, country) VALUES (3,'2 Brookfield Court Tiptown','Galway','Ireland');
INSERT INTO MobileShop.Address(userID, address, county, country) VALUES (3,'23 Dublin Rd. Dundalk','Louth','Ireland');

UPDATE MobileShop.Product SET pictureLink = 'AppleiPhone616Gb' WHERE productName = 'Apple iPhone 6, 16Gb';
UPDATE MobileShop.Product SET pictureLink = 'AppleiPhone6Plus' WHERE productName = 'Apple iPhone 6 Plus';
UPDATE MobileShop.Product SET pictureLink = 'HTCOneA916Gb' WHERE productName = 'HTC One A9 16Gb';
UPDATE MobileShop.Product SET pictureLink = 'HTCOneM8s16GbG' WHERE productName = 'HTC One M8s, 16Gb - Gunmetal';
UPDATE MobileShop.Product SET pictureLink = 'LGG316GbGold' WHERE productName = 'LG G3,16Gb - Gold';
UPDATE MobileShop.Product SET pictureLink = 'LGG3S8GbBlack' WHERE productName = 'LG G3 S, 8Gb - Black';
UPDATE MobileShop.Product SET pictureLink = 'MicrosoftLumia4358Gb' WHERE productName = 'Microsoft Lumia 435 8Gb';
UPDATE MobileShop.Product SET pictureLink = 'SamsungGalaxyS6Edge6' WHERE productName = 'Samsung Galaxy S6 Edge, 64Gb';
UPDATE MobileShop.Product SET pictureLink = 'SamsungGalaxyS632Gb' WHERE productName = 'Samsung Galaxy S6-32Gb';
UPDATE MobileShop.Product SET pictureLink = 'SamsungGalaxyAce34Gb' WHERE productName = 'Samsung Galaxy Ace 3, 4Gb';
UPDATE MobileShop.Product SET pictureLink = 'SonyXperiaC416Gb' WHERE productName = 'Sony Xperia C4, 16Gb';
UPDATE MobileShop.Product SET pictureLink = 'SonyXperiaZ3Plus32Gb' WHERE productName = 'Sony Xperia Z3 Plus, 32Gb';


UPDATE MobileShop.Product SET productName = 'Apple iPhone 6' WHERE productName = 'Apple iPhone 6, 16Gb';
UPDATE MobileShop.Product SET productName = 'Apple iPhone 6 Plus' WHERE productName = 'Apple iPhone 6 Plus';
UPDATE MobileShop.Product SET productName = 'HTC One A9' WHERE productName = 'HTC One A9 16Gb';
UPDATE MobileShop.Product SET productName = 'HTC One M8s' WHERE productName = 'HTC One M8s, 16Gb - Gunmetal';
UPDATE MobileShop.Product SET productName = 'LG G3-Gold' WHERE productName = 'LG G3,16Gb - Gold';
UPDATE MobileShop.Product SET productName = 'LG G3 S-Black' WHERE productName = 'LG G3 S, 8Gb - Black';
UPDATE MobileShop.Product SET productName = 'Microsoft Lumia 435' WHERE productName = 'Microsoft Lumia 435 8Gb';
UPDATE MobileShop.Product SET productName = 'Samsung Galaxy Edge' WHERE productName = 'Samsung Galaxy S6 Edge, 64Gb';
UPDATE MobileShop.Product SET productName = 'Samsung Galaxy S6' WHERE productName = 'Samsung Galaxy S6-32Gb';
UPDATE MobileShop.Product SET productName = 'Samsung Galaxy Ace 3' WHERE productName = 'Samsung Galaxy Ace 3, 4Gb';
UPDATE MobileShop.Product SET productName = 'Sony Xperia C4' WHERE productName = 'Sony Xperia C4, 16Gb';
UPDATE MobileShop.Product SET productName = 'Sony Xperia Z3 Plus' WHERE productName = 'Sony Xperia Z3 Plus, 32Gb';

DELIMITER //

CREATE TRIGGER bu_check BEFORE UPDATE ON MobileShop.Account
FOR EACH ROW
BEGIN

    SET NEW.accountStatus=
        CASE WHEN NEW.attempts > 15 THEN 'locked'
            WHEN NEW.attempts > 10 THEN 'stg2'
            WHEN NEW.attempts > 5 and NEW.attempts <= 10 THEN 'stg1'
            ELSE 'unlocked'
        END;
END;
//


CREATE TRIGGER ai_update_stock AFTER INSERT ON MobileShop.OrderProduct
FOR EACH ROW
BEGIN
    UPDATE MobileShop.Product set stock=(stock-NEW.quantity) WHERE productID=NEW.productID;
END;
//


DELIMITER ;

DROP function IF EXISTS calculate_time;

DELIMITER //

CREATE FUNCTION calculate_time(storedValue timestamp)
    RETURNS float
    LANGUAGE SQL
BEGIN
    RETURN (SELECT NOW()-storedValue);
END;
//

CREATE FUNCTION get_total_rating(nProductId INT)
RETURNS float
BEGIN
    DECLARE ratings int;
    DECLARE ratingCount int;
    
    SELECT SUM(rating) INTO Ratings FROM Rating WHERE productId = nProductId;
    SELECT COUNT(*) INTO RatingCount FROM Rating WHERE productId = nProductId;
    
    RETURN ratings/ratingCount;
END;
//
    


DELIMITER ;

DROP PROCEDURE IF EXISTS find_all_users;

DROP PROCEDURE IF EXISTS get_user_by_id;

DROP PROCEDURE IF EXISTS get_user_by_name;

DROP PROCEDURE IF EXISTS register_new_user;

DROP PROCEDURE IF EXISTS login;

DROP PROCEDURE IF EXISTS delete_user;

DROP PROCEDURE IF EXISTS edit_user_details;

DROP PROCEDURE IF EXISTS update_user_wallet;

DROP PROCEDURE IF EXISTS reset_user_wallet;

DROP PROCEDURE IF EXISTS password_recovery_by_id;

DROP PROCEDURE IF EXISTS register_new_password_recovery;

DROP PROCEDURE IF EXISTS password_reset_by_email;

DROP PROCEDURE IF EXISTS delete_password_reset_by_email;

DROP PROCEDURE IF EXISTS get_supplier_by_id;

DROP PROCEDURE IF EXISTS get_all_suppliers;

DROP PROCEDURE IF EXISTS delete_supplier_by_id;

DROP PROCEDURE IF EXISTS insert_supplier;

DROP PROCEDURE IF EXISTS update_supplier;

DROP PROCEDURE IF EXISTS find_all_comments;

DROP PROCEDURE IF EXISTS find_all_comments_by_product_id;

DROP PROCEDURE IF EXISTS find_all_comments_by_user_id;

DROP PROCEDURE IF EXISTS add_comment;

DROP PROCEDURE IF EXISTS delete_all_user_comment;

DROP PROCEDURE IF EXISTS delete_all_user_comment_on_product;

DROP PROCEDURE IF EXISTS edit_rating;

DROP PROCEDURE IF EXISTS add_rating;

DROP PROCEDURE IF EXISTS find_total_rating;

DROP PROCEDURE IF EXISTS count_brands;

DROP PROCEDURE IF EXISTS get_all_products;

DROP PROCEDURE IF EXISTS get_all_products_by_order_id;

DROP PROCEDURE IF EXISTS get_product;

DROP PROCEDURE IF EXISTS get_product_like_name;

DROP PROCEDURE IF EXISTS register_product;

DROP PROCEDURE IF EXISTS delete_product_by_id;

DROP PROCEDURE IF EXISTS delete_product_by_name;

DROP PROCEDURE IF EXISTS update_product;

DROP PROCEDURE IF EXISTS get_all_orders_product;

DROP PROCEDURE IF EXISTS get_all_orders_product_by_user_id;

DROP PROCEDURE IF EXISTS get_order_product_by_id;

DROP PROCEDURE IF EXISTS insert_order_product;

DROP PROCEDURE IF EXISTS delete_order_product_by_id;

DROP PROCEDURE IF EXISTS update_order_product;

DROP PROCEDURE IF EXISTS get_all_orders;

DROP PROCEDURE IF EXISTS get_all_orders_by_user_id;

DROP PROCEDURE IF EXISTS get_order_by_id;

DROP PROCEDURE IF EXISTS insert_order;

DROP PROCEDURE IF EXISTS delete_order_by_id;

DROP PROCEDURE IF EXISTS update_order;

DROP PROCEDURE IF EXISTS get_last_order_id;

DROP PROCEDURE IF EXISTS update_account;

DROP PROCEDURE IF EXISTS update_account_if_empty;

DROP PROCEDURE IF EXISTS get_status;

DROP PROCEDURE IF EXISTS reset;

DROP PROCEDURE IF EXISTS get_latch_info;

DROP PROCEDURE IF EXISTS set_latch_info;

DROP PROCEDURE IF EXISTS setLatchInfo;

DROP PROCEDURE IF EXISTS update_user_accountid_latch;



DELIMITER //

CREATE PROCEDURE update_user_accountid_latch(IN nEmail varchar(50),IN nAccountId varchar(100))
BEGIN
UPDATE User SET accountId = nAccountId WHERE email = nEmail;
END;
//

CREATE PROCEDURE get_latch_info()
BEGIN
SELECT * FROM Latch;
END;
//

CREATE PROCEDURE set_latch_info(IN APP_ID Varchar(100), IN SECRET Varchar(100))
BEGIN
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
END;
START TRANSACTION;
    DELETE FROM Latch WHERE 1=1;
    INSERT INTO Latch VALUES (APP_ID, SECRET);
    COMMIT;
END;
//

CREATE PROCEDURE find_all_users()
BEGIN
SELECT * FROM User;
END;
//

CREATE PROCEDURE get_user_by_id(IN nUserID INT(5))
BEGIN
SELECT * FROM User WHERE userID = nUserID;
END;
//

CREATE PROCEDURE get_user_by_name
(
IN newFName VARCHAR(100), 
IN newLName VARCHAR(100)
)
BEGIN
SELECT * FROM User WHERE fName like newFName or lName like newLName;
END;
//

CREATE PROCEDURE login
(
IN nEmail VARCHAR(100),
IN nPass VARCHAR(100)
)
BEGIN
SELECT * FROM User WHERE email = nEmail and pass = nPass;
END;
//


CREATE PROCEDURE register_new_user 
(
IN nUserID INT(5),
IN newFName VARCHAR(100),
IN newLName VARCHAR(100),
IN nEmail VARCHAR(100),
IN nPhone VARCHAR(100),
IN nPass VARCHAR(100),
IN nIsAdmin BOOLEAN,
IN nWalletCredits INT(100)
)
BEGIN
INSERT INTO User(
   userID,
   fName,
   lName,
   email,
   phone,
   pass,
   isAdmin,
   walletCredits)
VALUES (
   nUserID,
   newFName,
   newLName,
   nEmail,
   nPhone,
   nPass,
   nIsAdmin,
   nWalletCredits);
END;
//

CREATE PROCEDURE delete_user(IN nUserId INT(5))
BEGIN
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
END;
START TRANSACTION;
    DELETE FROM OrderProduct WHERE orderID IN(SELECT orderID FROM Orders WHERE userID = nUserId);
    DELETE FROM Orders WHERE userID = nUserId;
    DELETE FROM Address WHERE userID = nUserId;
    DELETE FROM Rating WHERE userId = userId;
    DELETE FROM User WHERE userID = nUserId;
    COMMIT;
END;
//

CREATE PROCEDURE edit_user_details
(
IN nUserID INT(5),
IN newFName VARCHAR(100),
IN newLName VARCHAR(100),
IN nEmail VARCHAR(100),
IN nPhone VARCHAR(100),
IN nPass VARCHAR(100),
IN nIsAdmin BOOLEAN,
IN nWalletCredits INT(100)
)
BEGIN
UPDATE User SET fName = newFName, lName = newLName, email = nEmail, phone = nPhone, pass = nPass, isAdmin = nIsAdmin,
walletCredits = nWalletCredits WHERE userID = nUserID;
END;
//


CREATE PROCEDURE update_user_wallet
(
IN nWalletCredits INT(100),
IN nUserID INT(5)
)
BEGIN
UPDATE User SET walletCredits = nWalletCredits WHERE userID = nUserID;
END;
//

CREATE PROCEDURE reset_user_wallet
(
IN nUserID INT(5)
)
BEGIN
UPDATE User SET walletCredits = 0 WHERE userID = nUserID;
END;
//

CREATE PROCEDURE password_recovery_by_id(IN nId VARCHAR(100))
BEGIN
SELECT * FROM PassRecovery WHERE id = nId;
END;
//

CREATE PROCEDURE register_new_password_recovery(IN nEmail VARCHAR(100), IN nId VARCHAR(100))
BEGIN
INSERT INTO PassRecovery VALUES (nEmail,NOW(),nId);
END;
//

CREATE PROCEDURE password_reset_by_email(IN nPass VARCHAR(100), IN nEmail VARCHAR(100))
BEGIN
UPDATE User SET pass = nPass WHERE email = nEmail;
END;
//

CREATE PROCEDURE delete_password_reset_by_email(IN nEmail VARCHAR(100))
BEGIN
DELETE FROM PasswordRecovery WHERE email = nEmail;
END;
//



CREATE PROCEDURE get_all_suppliers()
BEGIN
SELECT * FROM Supplier;
END;
//

CREATE PROCEDURE get_supplier_by_id(IN nId INT(5))
BEGIN
SELECT * FROM Supplier WHERE supplierID = nId;
END;
//

CREATE PROCEDURE insert_supplier 
(
IN nSupplierName VARCHAR(100),
IN nSupplierAddress VARCHAR(100),
IN nSupplierCounty VARCHAR(100),
IN nSupplierCountry VARCHAR(100),
IN nSupplierEmail VARCHAR(100),
IN nSupplierPhone VARCHAR(100)
)
BEGIN
INSERT INTO Supplier
(
   supplierName,
   address,
   county,
   country,
   email,
   phone
)
VALUES (
   nSupplierName,
   nSupplierAddress,
   nSupplierCounty,
   nSupplierCountry,
   nSupplierEmail,
   nSupplierPhone
   );
END;
//

CREATE PROCEDURE delete_supplier_by_id(IN nId INT(5))
BEGIN
DELETE FROM Supplier WHERE supplierID = nId;
END;
//

CREATE PROCEDURE update_supplier 
(
IN nSupplierId INT(5),
IN nSupplierName VARCHAR(100),
IN nSupplierAddress VARCHAR(100),
IN nSupplierCounty VARCHAR(100),
IN nSupplierCountry VARCHAR(100),
IN nSupplierEmail VARCHAR(100),
IN nSupplierPhone VARCHAR(100)
)
BEGIN
UPDATE Supplier SET supplierName = nSupplierName, address = nSupplierAddress, county = nSupplierCounty,
country = nSupplierCountry, email = nSupplierEmail, phone = nSupplierPhone WHERE supplierID = nSupplierId;
END;
//



CREATE PROCEDURE find_all_comments()
BEGIN
SELECT * FROM Rating;
END;
//

CREATE PROCEDURE find_all_comments_by_product_id(IN nProductId INT(5))
BEGIN
SELECT * FROM Rating WHERE productId = nProductId;
END;
//

CREATE PROCEDURE find_all_comments_by_user_id(IN nUserId INT(5))
BEGIN
SELECT * FROM Rating WHERE userId = nUserId;
END;
//

CREATE PROCEDURE add_comment 
(
IN nUserId INT(5),
IN nProductId INT(5),
IN nComment VARCHAR(1000)
)
BEGIN
INSERT INTO Rating
(
   userId,
   productId,
   comment
)
VALUES (
   nUserId,
   nProductId,
   nComment
   );
END;
//

CREATE PROCEDURE delete_all_user_comment(IN nUserId INT(5))
BEGIN
DELETE FROM Rating WHERE userID = nUserId;
END;
//

CREATE PROCEDURE delete_all_user_comment_on_product(IN nUserId INT(5), IN nProductId INT(5))
BEGIN
DELETE FROM Rating WHERE userID = nUserId and productId = nProductId;
END;
//

CREATE PROCEDURE edit_rating(IN nUserId INT(5), IN nProductId INT(5), IN nComment VARCHAR(1000), IN nRating INT(1))
BEGIN
UPDATE Rating SET comment = nComment, rating = nRating  WHERE userID = nUserId and productId = nProductId;
END;
//

CREATE PROCEDURE add_rating 
(
IN nUserId INT(5),
IN nProductId INT(5),
IN nRating INT(1),
IN nComment VARCHAR(1000)
)
BEGIN
INSERT INTO Rating
(
   userId,
   productId,
   rating,
   comment
)
VALUES (
   nUserId,
   nProductId,
   nRating,
   nComment
   );
END;
//

CREATE PROCEDURE find_total_rating(IN nProductId INT(5))
BEGIN
SELECT * FROM Rating WHERE productId = nProductId;
END;
//



CREATE PROCEDURE count_brands()
BEGIN
SELECT count(distinct(brand)) FROM Product;
END;
//

CREATE PROCEDURE get_all_products()
BEGIN
SELECT * FROM Product;
END;
//

CREATE PROCEDURE get_all_products_by_order_id(IN nOrderId INT(5))
BEGIN
SELECT Product.* FROM Orders, OrderProduct, Product WHERE Orders.orderID = OrderProduct.orderID 
AND OrderProduct.productID = Product.productID and Orders.orderID = nOrderId;
END;
//

CREATE PROCEDURE get_product(IN nProductId INT(5))
BEGIN
SELECT * FROM Product WHERE productID = nProductId;
END;
//

CREATE PROCEDURE get_product_like_name(IN nProductName VARCHAR(100))
BEGIN
SELECT * FROM Product WHERE productName like nProductName;
END;
//

CREATE PROCEDURE register_product
(
IN nProductName VARCHAR(100),
IN nPrice FLOAT(6,2),
IN nDescription VARCHAR(10000),
IN nStock INT(3),
IN nBrand VARCHAR(20),
IN nPictureLink VARCHAR(100)
)
BEGIN
INSERT INTO Product 
(
productName,
price,
description,
stock,
brand,
pictureLink
) 
VALUES 
(
nProductName,
nPrice,
nDescription,
nStock,
nBrand,
nPictureLink
);
END;
//


CREATE PROCEDURE delete_product_by_id(IN nProductId INT(5))
BEGIN
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
BEGIN
UPDATE Product SET price = 0, description = 'This item is no longer supplied', stock = 0 WHERE productId = nProductId;
END;
DELETE FROM Product WHERE productID = nProductId; 
END;
//

CREATE PROCEDURE delete_product_by_name(IN nProductName VARCHAR(100))
BEGIN
DELETE FROM Product WHERE productName = nProductName;
END;
//

CREATE PROCEDURE update_product(
IN nProductName VARCHAR(100),
IN nPrice FLOAT(6,2),
IN nDescription VARCHAR(10000),
IN nStock INT(3),
IN nBrand VARCHAR(20),
IN nPictureLink VARCHAR(100),
IN nProductId INT(5)
)
BEGIN
UPDATE Product SET productName = nProductName, price = nPrice, description = nDescription,
 stock = nStock, brand = nBrand, pictureLink = nPictureLink WHERE productID = nProductId;
END;
//


CREATE PROCEDURE get_all_orders_product()
BEGIN
SELECT * FROM OrderProduct;
END;
//

CREATE PROCEDURE get_all_orders_product_by_user_id(IN nUserId INT(5))
BEGIN
SELECT orderproduct.* FROM Product, orders, orderproduct WHERE
product.productID = orderproduct.productID AND orderproduct.orderID = orders.orderID
AND orders.userID = nUserId;
END;
//

CREATE PROCEDURE get_order_product_by_id(IN nOrderId INT(5), IN nProductId INT(5))
BEGIN
SELECT * FROM OrderProduct WHERE orderID = nOrderId AND productID = nProductId;
END;
//

CREATE PROCEDURE delete_order_product_by_id(IN nOrderId INT(5))
BEGIN
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
    ROLLBACK;
END;
START TRANSACTION;
DELETE FROM OrderProduct WHERE orderID = nOrderId;
DELETE FROM Orders WHERE orderID = nOrderId;
COMMIT;
END;
//



CREATE PROCEDURE update_order_product
(
IN nOrderId INT(5), 
IN nProductId INT(5), 
IN nQty INT(3), 
IN oOrderId INT(5), 
IN oProductId INT(5) 
)
BEGIN
UPDATE OrderProduct SET orderID = nOrderId, productID = nProductId , 
quantity = nQty WHERE orderID = oOrderId AND productID = oProductId;
END;
//



CREATE PROCEDURE get_all_orders()
BEGIN
SELECT * FROM Orders;
END;
//

CREATE PROCEDURE get_all_orders_by_user_id(IN nUserId INT(5))
BEGIN
SELECT * FROM Orders WHERE userID = nUserId;
END;
//

CREATE PROCEDURE get_order_by_id(IN nOrderId INT(5))
BEGIN
SELECT * FROM Orders WHERE orderID = nOrderId;
END;
//

CREATE PROCEDURE insert_order
(
IN nOrderId INT(5),
IN nDate DATE,
IN nUserId INT(5),
IN nAddressID INT(5),
IN nTotalPrice FLOAT(7,2)
)
BEGIN
INSERT INTO Orders VALUES(nOrderId,nDate,nUserId,nAddressID,nTotalPrice);
END;
//

CREATE PROCEDURE insert_order_product(
IN nOrderID INT(5),
IN nProductID INT(5),
IN nQuantity INT(3)
)
BEGIN
    INSERT INTO OrderProduct VALUES(nOrderID, nProductID, nQuantity);
END;
//

CREATE PROCEDURE delete_order_by_id( IN nOrderId INT(5))
BEGIN
DELETE FROM Orders WHERE orderID = nOrderId;
END;
//

CREATE PROCEDURE update_order
(
IN nDate DATE,
IN nUserId INT(5),
IN nOrderId INT(5)
)
BEGIN
UPDATE Orders SET dateOrdered = nDate, userID = nUserId WHERE orderID = nOrderId;
END;
//

CREATE PROCEDURE get_last_order_id()
BEGIN
SELECT max(orderID) FROM Orders;
END;
//

CREATE PROCEDURE update_account(IN nEmail VARCHAR(100))
BEGIN
DECLARE result int;
DECLARE CONTINUE HANDLER FOR SQLSTATE '40000'
BEGIN
INSERT INTO Account (email, attempts, time, accountStatus) values (nEmail, 1, NOW(), 'unlocked');
END;
if(SELECT IFNULL( (SELECT attempts FROM Account WHERE email = 'test@test.com') ,0) = 0) then
SIGNAL SQLSTATE '40000'
SET MESSAGE_TEXT = 'account info not found';
ELSE
update account SET attempts = (attempts+1), time=NOW()  WHERE email = nEmail;
END IF;
END;
//

CREATE PROCEDURE get_status(IN nEmail VARCHAR(100))
BEGIN
SELECT * FROM Account WHERE email = nEmail;
END;
//

CREATE PROCEDURE reset(IN nEmail VARCHAR(100))
BEGIN
DELETE FROM Account WHERE email = nEmail;
END;
// 


Create PROCEDURE get_all_address()
BEGIN
SELECT * FROM Address;
END;
//

CREATE PROCEDURE get_all_address_by_user_id(IN nUserId INT(5))
BEGIN
SELECT * FROM Address WHERE userID = nUserId;
END;
//

CREATE PROCEDURE get_address_by_address_id(IN nAddressId INT(5))
BEGIN
SELECT * FROM Address WHERE address = nAddress;
END;
//

CREATE PROCEDURE edit_user_address
(
IN nAddressId INT(5),
IN nAddress VARCHAR(100),
IN nCounty VARCHAR(100),
IN nCountry VARCHAR(100)
)
BEGIN
UPDATE Address SET address = nAddress, county = nCounty, country = nCountry WHERE addressID = nAddressId;
END;
//

CREATE PROCEDURE register_address
(
IN nAddressId INT(5),
IN nUserId INT(5),
IN nAddress VARCHAR(100),
IN nCounty VARCHAR(100),
IN nCountry VARCHAR(100)
)
BEGIN
INSERT INTO Address
(
addressID,
userID,
address,
county,
country
)
values
(
nAddressId,
nUserId,
nAddress,
nCounty,
nCountry
);
END
//

CREATE PROCEDURE delete_all_user_address(IN nUserId INT(5))
BEGIN
DELETE FROM Address WHERE userID = nUserId;
END;
//

CREATE PROCEDURE delete_address_by_address_id(IN nAddressId INT(5))
BEGIN
DELETE FROM Address WHERE addressID = nAddressId;
END;
//

CREATE PROCEDURE get_last_address(IN nUserId int(5))
BEGIN
SELECT MAX(addressID) FROM Address WHERE userID = nUserId;
END;
//

CREATE PROCEDURE get_time_difference(IN nEmail VARCHAR(100))
BEGIN
DECLARE tim datetime;
SELECT Account.time INTO tim FROM Account WHERE email=nEmail;
SELECT calculate_time(tim);
END;
//
